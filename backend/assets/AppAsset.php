<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/migx.css',
    ];
    public $js = [
        'js/dynamicFormLiveAdd.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'pendalf89\filemanager\assets\FilemanagerAsset',
        'pendalf89\filemanager\assets\FileInputAsset',
    ];
}
