<?php

namespace backend\controllers;

use yii\helpers\Url;

/**
* This is the class for controller "AkciaController".
*/
class AkciaController extends \backend\controllers\base\AkciaController
{
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!empty($_POST['Akcia']['products'])) {
            $_POST['Akcia']['products'] = implode(',', $_POST['Akcia']['products']);
        }

        if ($model->load($_POST) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
}
