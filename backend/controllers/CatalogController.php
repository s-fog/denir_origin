<?php

namespace backend\controllers;

use common\models\Catalog;

/**
* This is the class for controller "CatalogController".
*/
class CatalogController extends \backend\controllers\base\CatalogController
{
    public function actionCreate()
    {
        $model = new Catalog;

        try {
            if ($model->load($_POST) && $model->save()) {
                if ($_POST['mode'] == 'justSave') {
                    return $this->redirect(['update', 'id' => $model->id]);
                } else {
                    return $this->redirect(['index', 'id' => $model->id]);
                }
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing Catalog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load($_POST) && $model->save()) {
            if ($_POST['mode'] == 'justSave') {
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                return $this->redirect(['index', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
}
