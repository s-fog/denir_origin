<?php

namespace backend\controllers;

use common\models\CatalogHasCtag;
use common\models\Ctag;
use common\models\ProductHasCtag;

/**
* This is the class for controller "CtagController".
*/
class CtagController extends \backend\controllers\base\CtagController
{
    public function actionCatalogchange() {
        $values = $_POST['values'];
        $arrayValues = ($values != 'null') ? explode(',', $values) : [];
        $catalogTags = Ctag::catalogTags($_POST['catalog_id']);

        if (count($catalogTags) > count($arrayValues)) { //Удаляем элемент
            foreach($arrayValues as $key => $value) {
                if ($value * 1 <= 0) {//Если значение строка
                    $ctag = Ctag::find()->where(['name' => $value])->one();
                    $arrayValues[$key] = $ctag->id;
                }
            }

            foreach($catalogTags as $ctag) {
                if (!in_array($ctag, $arrayValues)) {
                    $catalogHasCtag = CatalogHasCtag::find()->where([
                        'catalog_id' => $_POST['catalog_id'],
                        'ctag_id' => $ctag
                    ])->one();
                    $catalogHasCtag->delete();
                    echo 'success delete ctag_id='.$ctag;
                }
            }
        } else {//Добавляем элемент
            foreach($arrayValues as $value) {
                if ($value * 1 > 0) {//Если значение число
                    $t = CatalogHasCtag::find()->where(['catalog_id' => $_POST['catalog_id'], 'ctag_id' => $value])->one();

                    if ($t == NULL) {
                        $this->catalogHasCtagCreate($value);
                    } else {
                        echo 'already here ctag_id='.$value;
                    }
                } else {//Если значение строка
                    if (!empty($value)) {
                        $ctag = Ctag::find()->where(['name' => $value])->one();

                        if (empty($ctag->id)) {
                            $ctag = new Ctag;
                            $ctag->name = $value;
                            $ctag->save();
                        }

                        $this->catalogHasCtagCreate($ctag->id);
                    }
                }
            }
        }
    }

    public function actionProductchange() {
        $values = $_POST['values'];
        $arrayValues = ($values != 'null') ? explode(',', $values) : [];
        $productTags = Ctag::productTags($_POST['product_id']);

        if (count($productTags) > count($arrayValues)) { //Удаляем элемент
            foreach($arrayValues as $key => $value) {
                if ($value * 1 <= 0) {//Если значение строка
                    $ctag = Ctag::find()->where(['name' => $value])->one();
                    $arrayValues[$key] = $ctag->id;
                }
            }

            foreach($productTags as $ctag) {
                if (!in_array($ctag, $arrayValues)) {
                    $productHasCtag = ProductHasCtag::find()->where([
                        'product_id' => $_POST['product_id'],
                        'ctag_id' => $ctag
                    ])->one();
                    $productHasCtag->delete();
                    echo 'success delete ctag_id='.$ctag;
                }
            }
        } else {//Добавляем элемент
            foreach($arrayValues as $value) {
                if ($value * 1 > 0) {//Если значение число
                    $t = ProductHasCtag::find()->where(['product_id' => $_POST['product_id'], 'ctag_id' => $value])->one();

                    if ($t == NULL) {
                        $this->productHasCtagCreate($value);
                    } else {
                        echo 'already here ctag_id='.$value;
                    }
                } else {//Если значение строка
                    if (!empty($value)) {
                        $ctag = Ctag::find()->where(['name' => $value])->one();

                        if (empty($ctag->id)) {
                            $ctag = new Ctag;
                            $ctag->name = $value;
                            $ctag->save();
                        }

                        $this->productHasCtagCreate($ctag->id);
                    }
                }
            }
        }
    }

    private function catalogHasCtagCreate($ctag_id) {
        $chc = CatalogHasCtag::find()->where(['ctag_id' => $ctag_id, 'catalog_id' => $_POST['catalog_id']])->one();

        if (empty($chc)) {
            $catalogHasCtag = new CatalogHasCtag;
            $catalogHasCtag->catalog_id = $_POST['catalog_id'];
            $catalogHasCtag->ctag_id = $ctag_id;

            if($catalogHasCtag->save()) {
                echo 'success';
            } else {
                echo 'error';
            }
        } else {
            echo 'CatalogHasCtag already here';
        }
    }

    private function productHasCtagCreate($ctag_id) {
        $phc = ProductHasCtag::find()->where(['ctag_id' => $ctag_id, 'product_id' => $_POST['product_id']])->one();
        if (empty($chc)) {
            $productHasCtag = new ProductHasCtag;
            $productHasCtag->product_id = $_POST['product_id'];
            $productHasCtag->ctag_id = $ctag_id;

            if($productHasCtag->save()) {
                echo 'success';
            } else {
                echo 'error';
            }
        } else {
            echo 'ProductHasCtag already here';
        }
    }
}
