<?php

namespace backend\controllers;

use common\models\Main;
use common\models\Model;
use common\models\Pslider;
use Exception;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
* This is the class for controller "MainController".
*/
class MainController extends \backend\controllers\base\MainController
{
    public function actionIndex()
    {
        return;
    }

    /**
     * Displays a single Main model.
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return;
    }

    /**
     * Creates a new Main model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        return;
    }

    /**
     * Updates an existing Main model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $modelMain = $this->findModel($id);
        $modelsPslider = $modelMain->pslider;

        if (!empty($_POST['Main']['portfolioslider'])) {
            $r = [];

            foreach($_POST['Main']['portfolioslider'] as $value) {
                $r[] = $value;
            }

            $_POST['Main']['portfolioslider'] = implode(',', $r);
        }

        if ($modelMain->load($_POST)) {

            $oldIDs = ArrayHelper::map($modelsPslider, 'id', 'id');
            $modelsPslider = Model::createMultiple(Pslider::classname(), $modelsPslider);
            Model::loadMultiple($modelsPslider, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsPslider, 'id', 'id')));
            //var_dump($modelsPslider);die();

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsPslider),
                    ActiveForm::validate($modelMain)
                );
            }

            // validate all models
            $valid = $modelMain->validate();
            $valid = Model::validateMultiple($modelsPslider) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $modelMain->save(false)) {
                        if (! empty($deletedIDs)) {
                            Pslider::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($modelsPslider as $modelPslider) {
                            $modelPslider->main_id = $modelMain->id;
                            if (! ($flag = $modelPslider->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['update', 'id' => $modelMain->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'model' => $modelMain,
            'modelsPslider' => (empty($modelsPslider)) ? [new Pslider] : $modelsPslider
        ]);
    }

    /**
     * Deletes an existing Main model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        return;
    }

    /**
     * Finds the Main model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Main the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Main::findOne($id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }
}
