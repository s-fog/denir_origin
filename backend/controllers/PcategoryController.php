<?php

namespace backend\controllers;

use common\models\Model;
use common\models\Pcategory;
use common\models\Pslider;
use Exception;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\widgets\ActiveForm;
use zxbodya\yii2\galleryManager\GalleryManagerAction;

/**
* This is the class for controller "PcategoryController".
*/
class PcategoryController extends \backend\controllers\base\PcategoryController
{
    public function actionCreate()
    {
        $modelPcategory = new Pcategory;
        $modelsPslider = [new Pslider];

        if(!empty($_POST['Pcategory']['portfolioslider'])) {
            $r = [];

            foreach($_POST['Pcategory']['portfolioslider'] as $value) {
                $r[] = $value;
            }

            $_POST['Pcategory']['portfolioslider'] = implode(',', $r);
        }

        if ($modelPcategory->load($_POST)) {

            $modelsPslider = Model::createMultiple(Pslider::classname());
            Model::loadMultiple($modelsPslider, Yii::$app->request->post());

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsPslider),
                    ActiveForm::validate($modelPcategory)
                );
            }

            // validate all models
            $valid = $modelPcategory->validate();
            $valid = Model::validateMultiple($modelsPslider) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $modelPcategory->save(false)) {
                        foreach ($modelsPslider as $modelPslider) {
                            $modelPslider->pcategory_id = $modelPcategory->id;
                            if (! ($flag = $modelPslider->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        if ($_POST['mode'] == 'justSave') {
                            return $this->redirect(['update', 'id' => $modelPcategory->id]);
                        } else {
                            return $this->redirect(['index', 'id' => $modelPcategory->id]);
                        }
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
            'model' => $modelPcategory,
            'modelsPslider' => (empty($modelsPslider)) ? [new Pslider] : $modelsPslider
        ]);
    }

    public function actionUpdate($id)
    {
        $modelPcategory = $this->findModel($id);
        $modelsPslider = $modelPcategory->pslider;

        if (!empty($_POST['Pcategory']['portfolioslider'])) {
            $r = [];

            foreach($_POST['Pcategory']['portfolioslider'] as $value) {
                $r[] = $value;
            }

            $_POST['Pcategory']['portfolioslider'] = implode(',', $r);
        }

        if ($modelPcategory->load($_POST)) {

            $oldIDs = ArrayHelper::map($modelsPslider, 'id', 'id');
            $modelsPslider = Model::createMultiple(Pslider::classname(), $modelsPslider);
            Model::loadMultiple($modelsPslider, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsPslider, 'id', 'id')));
            //var_dump($modelsPslider);die();

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsPslider),
                    ActiveForm::validate($modelPcategory)
                );
            }

            // validate all models
            $valid = $modelPcategory->validate();
            $valid = Model::validateMultiple($modelsPslider) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $modelPcategory->save(false)) {
                        if (! empty($deletedIDs)) {
                            Pslider::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($modelsPslider as $modelPslider) {
                            $modelPslider->pcategory_id = $modelPcategory->id;
                            if (! ($flag = $modelPslider->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();

                        if ($_POST['mode'] == 'justSave') {
                            return $this->redirect(['update', 'id' => $modelPcategory->id]);
                        } else {
                            return $this->redirect(['index', 'id' => $modelPcategory->id]);
                        }
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'model' => $modelPcategory,
            'modelsPslider' => (empty($modelsPslider)) ? [new Pslider] : $modelsPslider
        ]);
    }

    public function actions()
    {
        return [
            'galleryApi' => [
                'class' => GalleryManagerAction::className(),
                // mappings between type names and model classes (should be the same as in behaviour)
                'types' => [
                    'pcategory' => Pcategory::className()
                ]
            ],
        ];
    }
}
