<?php

namespace backend\controllers;

use common\models\Portfolio;
use yii\helpers\Url;
use zxbodya\yii2\galleryManager\GalleryManagerAction;

/**
* This is the class for controller "PortfolioController".
*/
class PortfolioController extends \backend\controllers\base\PortfolioController
{
    public function actionCreate()
    {
        $model = new Portfolio;

        if ($_POST) {
            $_POST['Portfolio']['creationdate'] = strtotime($_POST['Portfolio']['creationdate']);
        }

        try {
            if ($model->load($_POST) && $model->save()) {
                if ($_POST['mode'] == 'justSave') {
                    return $this->redirect(['update', 'id' => $model->id]);
                } else {
                    return $this->redirect(['index', 'id' => $model->id]);
                }
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($_POST) {
            $_POST['Portfolio']['creationdate'] = strtotime($_POST['Portfolio']['creationdate']);
        }

        if ($model->load($_POST) && $model->save()) {
            if ($_POST['mode'] == 'justSave') {
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                return $this->redirect(['index', 'id' => $model->id]);
            }
        } else {
            $model->creationdate = date('d.m.Y', $model->creationdate);

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actions()
    {
        return [
            'galleryApi' => [
                'class' => GalleryManagerAction::className(),
                // mappings between type names and model classes (should be the same as in behaviour)
                'types' => [
                    'portfolio' => Portfolio::className()
                ]
            ],
        ];
    }
}
