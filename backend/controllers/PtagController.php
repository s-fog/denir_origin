<?php

namespace backend\controllers;

use common\models\PcategoryHasPtag;
use common\models\PortfolioHasPtag;
use common\models\Ptag;

/**
* This is the class for controller "PtagController".
*/
class PtagController extends \backend\controllers\base\PtagController
{
    public function actionPcategorychange() {
        $ptags = Ptag::tags();
        $values = $_POST['values'];
        $arrayValues = ($values != 'null') ? explode(',', $values) : [];
        $pcategoryTags = Ptag::pcategoryTags($_POST['category_id']);

        if (count($pcategoryTags) > count($arrayValues)) { //Удаляем элемент
            foreach($arrayValues as $key => $value) {
                if ($value * 1 <= 0) {//Если значение строка
                    $ptag = Ptag::find()->where(['name' => $value])->one();
                    $arrayValues[$key] = $ptag->id;
                }
            }

            foreach($pcategoryTags as $ptag) {
                if (!in_array($ptag, $arrayValues)) {
                    $pcategoryHasPtag = PcategoryHasPtag::find()->where([
                        'pcategory_id' => $_POST['category_id'],
                        'ptag_id' => $ptag
                    ])->one();
                    $pcategoryHasPtag->delete();
                    echo 'success delete ptag_id='.$ptag;
                }
            }
        } else {//Добавляем элемент
            foreach($arrayValues as $value) {
                if ($value * 1 > 0) {//Если значение число
                    $t = PcategoryHasPtag::find()->where(['pcategory_id' => $_POST['category_id'], 'ptag_id' => $value])->one();

                    if ($t == NULL) {
                        $this->pcategoryHasPtagCreate($value);
                    } else {
                        echo 'already here ptag_id='.$value;
                    }
                } else {//Если значение строка
                    if (!empty($value)) {
                        $ptag = Ptag::find()->where(['name' => $value])->one();

                        if (empty($ptag->id)) {
                            $ptag = new Ptag;
                            $ptag->name = $value;
                            $ptag->save();
                        }

                        $this->pcategoryHasPtagCreate($ptag->id);
                    }
                }
            }
        }
    }

    private function pcategoryHasPtagCreate($ptag_id) {
        $php = PcategoryHasPtag::find()->where(['ptag_id' => $ptag_id, 'pcategory_id' => $_POST['category_id']])->one();

        if (empty($php)) {
            $pcategoryHasPtag = new PcategoryHasPtag;
            $pcategoryHasPtag->pcategory_id = $_POST['category_id'];
            $pcategoryHasPtag->ptag_id = $ptag_id;

            if($pcategoryHasPtag->save()) {
                echo 'success';
            } else {
                echo 'error';
            }
        } else {
            echo 'PcategoryHasPtag already here';
        }
    }


    public function actionPortfoliochange() {
        $values = $_POST['values'];
        $arrayValues = ($values != 'null') ? explode(',', $values) : [];
        $portfolioTags = Ptag::portfolioTags($_POST['portfolio_id']);

        if (count($portfolioTags) > count($arrayValues)) { //Удаляем элемент
            foreach($arrayValues as $key => $value) {
                if ($value * 1 <= 0) {//Если значение строка
                    $ptag = Ptag::find()->where(['name' => $value])->one();
                    $arrayValues[$key] = $ptag->id;
                }
            }

            foreach($portfolioTags as $ptag) {
                if (!in_array($ptag, $arrayValues)) {
                    $portfolioHasPtag = PortfolioHasPtag::find()->where([
                        'portfolio_id' => $_POST['portfolio_id'],
                        'ptag_id' => $ptag
                    ])->one();
                    $portfolioHasPtag->delete();
                    echo 'success delete ptag_id='.$ptag;
                }
            }
        } else {//Добавляем элемент
            foreach($arrayValues as $value) {
                if ($value * 1 > 0) {//Если значение число
                    $t = PortfolioHasPtag::find()->where(['portfolio_id' => $_POST['portfolio_id'], 'ptag_id' => $value])->one();

                    if ($t == NULL) {
                        $this->portfolioHasPtagCreate($value);
                    } else {
                        echo 'already here ptag_id='.$value;
                    }
                } else {//Если значение строка
                    if (!empty($value)) {
                        $ptag = Ptag::find()->where(['name' => $value])->one();

                        if (empty($ptag->id)) {
                            $ptag = new Ptag;
                            $ptag->name = $value;
                            $ptag->save();
                        }

                        $this->portfolioHasPtagCreate($ptag->id);
                    }
                }
            }
        }
    }

    private function portfolioHasPtagCreate($ptag_id) {
        $php = PortfolioHasPtag::find()->where(['ptag_id' => $ptag_id, 'portfolio_id' => $_POST['portfolio_id']])->one();

        if (empty($php)) {
            $portfolioHasPtag = new PortfolioHasPtag;
            $portfolioHasPtag->portfolio_id = $_POST['portfolio_id'];
            $portfolioHasPtag->ptag_id = $ptag_id;

            if($portfolioHasPtag->save()) {
                echo 'success';
            } else {
                echo 'error';
            }
        } else {
            echo 'PortfolioHasPtag already here';
        }

    }
}
