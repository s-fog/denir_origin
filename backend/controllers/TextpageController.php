<?php

namespace backend\controllers;

use Yii;
use common\models\Textpage;
use zxbodya\yii2\galleryManager\GalleryManagerAction;

/**
* This is the class for controller "TextpageController".
*/
class TextpageController extends \backend\controllers\base\TextpageController
{
    public function actionSlidermigx()
    {
        $this->layout = 'ajax';
        return $this->render('slidermigx');
    }

    public function actionMigxsave()
    {
        $request = Yii::$app->request;
        $model = Textpage::findOne(['id' => $request->post('id')]);
        $beforeArray = json_decode($model->slider, true);
        $requestArray = [];
        $newArray = [];

        foreach($request->post() as $key => $value) {
            if ($key != 'id') {
                $requestArray[$key] = $value;
            }
        }

        $beforeArray[] = $requestArray;

        foreach($beforeArray as $i => $arr) {
            $n = [];

            foreach($arr as $key => $value) {
                $n[] = '"' . $key . '":"' . $value . '"';
            }
            $newArray[$i] = $i . ':{' . implode(',', $n) . '}';
        }

        echo '{'.implode(',', $newArray).'}';
    }


    public function actions()
    {
        return [
            'galleryApi' => [
                'class' => GalleryManagerAction::className(),
                // mappings between type names and model classes (should be the same as in behaviour)
                'types' => [
                    'textpage' => Textpage::className()
                ]
            ],
        ];
    }
}
