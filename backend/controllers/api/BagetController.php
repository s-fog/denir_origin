<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "BagetController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class BagetController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Baget';
}
