<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "CatalogController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class CatalogController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Catalog';
}
