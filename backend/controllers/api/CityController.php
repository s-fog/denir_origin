<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "CityController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class CityController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\City';
}
