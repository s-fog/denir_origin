<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "CtagController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class CtagController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Ctag';
}
