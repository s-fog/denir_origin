<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "MainController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class MainController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Main';
}
