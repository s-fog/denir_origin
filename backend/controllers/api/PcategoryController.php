<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "PcategoryController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class PcategoryController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Pcategory';
}
