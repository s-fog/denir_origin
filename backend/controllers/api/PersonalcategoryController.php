<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "PersonalcategoryController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class PersonalcategoryController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Personalcategory';
}
