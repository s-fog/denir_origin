<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "PortfolioController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class PortfolioController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Portfolio';
}
