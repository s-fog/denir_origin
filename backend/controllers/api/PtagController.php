<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "PtagController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class PtagController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Ptag';
}
