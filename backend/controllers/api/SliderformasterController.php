<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "SliderformasterController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class SliderformasterController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Sliderformaster';
}
