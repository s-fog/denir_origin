<?php

use kartik\widgets\Select2;
use pendalf89\filemanager\widgets\FileInput;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var common\models\Akcia $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="akcia-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Akcia',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-danger'
    ]
    );
    ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>

        <p>
            

<!-- attribute pagetitle -->
			<?= $form->field($model, 'pagetitle')->textInput(['maxlength' => true]) ?>

<!-- attribute alias -->
            <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

<!-- attribute text1 -->
			<?= $form->field($model, 'text1')->textarea(['rows' => 6]) ?>

<!-- attribute text2 -->
			<?= $form->field($model, 'text2')->textarea(['rows' => 6]) ?>

            <?php
                if (!empty($model->products)) {
                    $model->products = explode(',', $model->products);
                }
            ?>

            <?=$form->field($model, 'products')->widget(Select2::classname(), [
                'options' => ['placeholder' => '', 'multiple' => true],
                'pluginOptions' => [
                    'tags' => true,
                    'tokenSeparators' => [',', ''],
                    'maximumInputLength' => 20
                ],
            ]);?>

            <?= $form->field($model, 'image')->widget(FileInput::classname(), [
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'options' => ['class' => 'form-control'],
                // Widget template
                'template' => '<div class="url-manager-image img"><img src="'.$model->image.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                // Optional, if set, only this image can be selected by user
                'thumb' => 'original',
                // Optional, if set, in container will be inserted selected image
                'imageContainer' => '.img',
                // Default to FileInput::DATA_URL. This data will be inserted in input field
                'pasteData' => FileInput::DATA_URL,
                // JavaScript function, which will be called before insert file data to input.
                // Argument data contains file data.
                // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                'callbackBeforeInsert' => 'function(e, data) {
                        console.log( data );
                    }',
            ]); ?>

<!-- attribute seodescription -->
			<?= $form->field($model, 'seodescription')->textarea(['rows' => 6]) ?>

<!-- attribute seotitle -->
			<?= $form->field($model, 'seotitle')->textInput(['maxlength' => true]) ?>

<!-- attribute seokeywords -->
			<?= $form->field($model, 'seokeywords')->textInput(['maxlength' => true]) ?>

<!-- attribute seoh1 -->
			<?= $form->field($model, 'seoh1')->textInput(['maxlength' => true]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    Tabs::widget(
                 [
                    'encodeLabels' => false,
                    'items' => [ 
                        [
    'label'   => Yii::t('models', 'Akcia'),
    'content' => $this->blocks['main'],
    'active'  => true,
],
                    ]
                 ]
    );
    ?>
        <hr/>

        <?php echo $form->errorSummary($model); ?>

        <?= Html::submitButton(
        '<span class="glyphicon glyphicon-check"></span> ' .
        ($model->isNewRecord ? 'Create' : 'Сохранить'),
        [
        'id' => 'save-' . $model->formName(),
        'class' => 'btn btn-success'
        ]
        );
        ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>

