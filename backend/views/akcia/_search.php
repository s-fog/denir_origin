<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\AkciaSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="akcia-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'pagetitle') ?>

		<?= $form->field($model, 'alias') ?>

		<?= $form->field($model, 'text1') ?>

		<?= $form->field($model, 'text2') ?>

		<?php // echo $form->field($model, 'products') ?>

		<?php // echo $form->field($model, 'seotitle') ?>

		<?php // echo $form->field($model, 'seokeywords') ?>

		<?php // echo $form->field($model, 'seoh1') ?>

		<?php // echo $form->field($model, 'seodescription') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
