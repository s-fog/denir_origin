<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Akcia $model
*/

$this->title = $model->pagetitle;
?>
<div class="giiant-crud akcia-update">

    <hr />

    <?php echo $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>
