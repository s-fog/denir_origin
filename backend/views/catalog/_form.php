<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var common\models\Catalog $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="catalog-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Catalog',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-danger'
    ]
    );
    ?>

    <?=Tabs::widget([
        'items' => [
            [
                'label'     =>  'Основное',
                'content'   =>  $this->render('_main', ['form' => $form, 'model' => $model]),
                'active'    =>  true
            ],
            [
                'label'     => 'SEO',
                'content'   =>  $this->render('_seo', ['form' => $form, 'model' => $model])
            ],
            [
                'label'     => 'SEO текст',
                'content'   =>  $this->render('_seotext', ['form' => $form, 'model' => $model])
            ]
        ]
    ]);?>
        <hr/>

        <?php echo $form->errorSummary($model); ?>
    
        <?= Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> ' .
            ($model->isNewRecord ? 'Создать' : 'Сохранить'),
            [
                'id' => 'save-' . $model->formName(),
                'class' => 'btn btn-success',
                'name' => 'mode',
                'value' => 'justSave'
            ]
        );
        ?>
        <?=Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> ' .
            ($model->isNewRecord ? 'Создать и выйти' : 'Сохранить и выйти'),
            [
                'class' => 'btn btn-success',
                'name' => 'mode',
                'value' => 'saveAndExit'
            ]
        );?>

        <?php ActiveForm::end(); ?>

    </div>

</div>

