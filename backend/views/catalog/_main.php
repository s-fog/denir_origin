<?php
use common\models\Catalog;
use common\models\Ctag;
use kartik\select2\Select2;
use mihaildev\ckeditor\CKEditor;

?>
<br>
<?=$form->field($model, 'name')->textInput(['maxlength' => true]) ?>
<?php if(!$model->isNewRecord && $model->parent_id == 0) { ?>
<?= $form->field($model, 'type')
    ->radioList([
        'Картина' => 'Картина',
        'Репродукция' => 'Репродукция',
        'Модульная картина' => 'Модульная картина',
        'Образ' => 'Образ'
    ]) ?>
<?php }?>
<div class="form-group">
    <label class="control-label col-sm-3" for="pcategory-parent_id">Теги</label>
    <div class="col-sm-6">
        <?php if($model->isNewRecord) { ?>
            Теги станут доступны после сохранения
        <?php } else { ?>
            <?=Select2::widget([
                'name' => 'tags',
                'value' => Ctag::catalogTags($model->id), // initial value
                'data' => Ctag::tags(),
                'maintainOrder' => true,
                'options' => ['placeholder' => 'Выберите теги ...', 'multiple' => true],
                'pluginOptions' => [
                    'tags' => true,
                    'maximumInputLength' => 100,
                ],
                'pluginEvents' => [
                    "change" => "function(event) { 
                        var values = $(this).val();
                        var request = 'values=' + values + '&catalog_id=' + ".$model->id.";
                                
                        $.post('/admin/index.php?r=ctag/catalogchange', request, function(response) {
                            console.log(response);
                        });
                     }"
                ]
            ])?>
        <?php } ?>
    </div>
</div>
<?=$form->field($model, 'parent_id')->widget(Select2::classname(), [
    'data' => Catalog::getCatalog(),
    'options' => ['placeholder' => 'Выберите родителя ...']
]);?>

<?php
    if ($model->parent_id == 0) {
        echo $form->field($model, 'cost')->widget(CKEditor::className(), ['editorOptions' =>['preset' => 'standard' ]]);
        echo $form->field($model, 'delivery')->widget(CKEditor::className(), ['editorOptions' =>['preset' => 'standard' ]]);
        echo $form->field($model, 'canvas')->widget(CKEditor::className(), ['editorOptions' =>['preset' => 'standard' ]]);
    }
?>
