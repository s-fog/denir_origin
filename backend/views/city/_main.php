<?php
use common\models\Pcategory;
use common\models\Portfolio;
use kartik\widgets\Select2;
use mihaildev\ckeditor\CKEditor;
use pendalf89\filemanager\widgets\FileInput;
use zxbodya\yii2\galleryManager\GalleryManager;

?>
    <br>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <!-- attribute period -->
<?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <!-- attribute period -->
<?= $form->field($model, 'period')->textInput(['maxlength' => true]) ?>

    <!-- attribute price -->
<?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <!-- attribute header -->
<?= $form->field($model, 'header')->textInput(['maxlength' => true]) ?>

    <!-- attribute html -->
<?= $form->field($model, 'html')->widget(CKEditor::className(), ['editorOptions' =>['preset' => 'standard' ]]); ?>