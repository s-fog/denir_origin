<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\CitySearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="city-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'name') ?>

		<?= $form->field($model, 'period') ?>

		<?= $form->field($model, 'price') ?>

		<?= $form->field($model, 'html') ?>

		<?php // echo $form->field($model, 'header') ?>

		<?php // echo $form->field($model, 'seotitle') ?>

		<?php // echo $form->field($model, 'seokeywords') ?>

		<?php // echo $form->field($model, 'seodescription') ?>

		<?php // echo $form->field($model, 'seoh1') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
