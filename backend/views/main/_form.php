<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var common\models\Main $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="main-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Main',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-danger'
    ]
    );
    ?>

    <?=Tabs::widget([
        'items' => [
            [
                'label'     =>  'Основное',
                'content'   =>  $this->render('_main', ['form' => $form, 'model' => $model, 'modelsPslider' => $modelsPslider]),
                'active'    =>  true
            ],
            [
                'label'     => 'SEO',
                'content'   =>  $this->render('_seo', ['form' => $form, 'model' => $model])
            ],
            [
                'label'     => 'SEO текст',
                'content'   =>  $this->render('_seotext', ['form' => $form, 'model' => $model])
            ],
            [
                'label'     => 'Настройки',
                'content'   =>  $this->render('_settings', ['form' => $form, 'model' => $model])
            ]
        ]
    ]);?>
        <hr/>

        <?php echo $form->errorSummary($model); ?>

        <?= Html::submitButton(
        '<span class="glyphicon glyphicon-check"></span> ' .
        ($model->isNewRecord ? 'Создать' : 'Сохранить'),
        [
        'id' => 'save-' . $model->formName(),
        'class' => 'btn btn-success'
        ]
        );
        ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>

