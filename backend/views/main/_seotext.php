<?php
use mihaildev\ckeditor\CKEditor;
?>
<br>
<?=$form->field($model, 'seoheader') ?>
<?=$form->field($model, 'seotext')->widget(CKEditor::className(), ['editorOptions' =>['preset' => 'standard' ]]); ?>
