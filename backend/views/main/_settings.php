<?php
use common\models\Catalog;
use common\models\Ctag;
use common\models\Portfolio;
use kartik\select2\Select2;
use pendalf89\filemanager\widgets\FileInput;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\Html;

?>
<br>

<?= $form->field($model, 'phone') ?>
<?= $form->field($model, 'address') ?>
<?= $form->field($model, 'work_time')->textarea() ?>

