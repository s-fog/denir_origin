<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Main $model
*/

$this->title = 'Главная страница';
?>
<div class="giiant-crud main-update">

    <?php echo $this->render('_form', [
        'model' => $model,
        'modelsPslider' => $modelsPslider
    ]); ?>

</div>
