<?php
use common\models\Pcategory;
use common\models\Portfolio;
use kartik\checkbox\CheckboxX;
use kartik\widgets\Select2;
use pendalf89\filemanager\widgets\FileInput;
?>
    <br>
            <?=$form->field($model, 'name')->textInput(['maxlength' => true]) ?>

			<?=$form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

            <?=$form->field($model, 'type')->widget(Select2::classname(), [
                'data' => [
                    'Портрет' => 'Портрет',
                    'Модульная' => 'Модульная',
                    'Шарж' => 'Шарж',
                    'Картина' => 'Картина',
                    'Роспись стен' => 'Роспись стен',
                    'Багеты' => 'Багеты'
                ],
                'options' => ['placeholder' => 'Выберите тип ...']
            ]);?>

<!-- attribute parent_id -->
            <?=$form->field($model, 'parent_id')->widget(Select2::classname(), [
                'data' => Pcategory::getPcategories(),
                'options' => ['placeholder' => 'Выберите родителя ...']
            ]);?>

            

            <?= $form->field($model, 'inmenu')->widget(CheckboxX::classname(), [
                'pluginOptions' => [
                    'threeState'=>false
                ]
            ])?>

            <?= $form->field($model, 'description')->textarea() ?>

            <div class="form-group">
                <label class="control-label col-sm-3" for="pcategory-parent_id">Теги</label>
                <div class="col-sm-6">
                    <?php if($model->isNewRecord) { ?>
                        Теги станут доступны после сохранения
                    <?php } else { ?>
                        <?=Select2::widget([
                            'name' => 'tags',
                            'value' => \common\models\Ptag::pcategoryTags($model->id), // initial value
                            'data' => \common\models\Ptag::tags(),
                            'maintainOrder' => true,
                            'options' => ['placeholder' => 'Выберите теги ...', 'multiple' => true],
                            'pluginOptions' => [
                                'tags' => true,
                                'maximumInputLength' => 100,
                                'class' => 'gfgfgfg'
                            ],
                            'pluginEvents' => [
                                "change" => "function(event) { 
                                var values = $(this).val();
                                var request = 'values=' + values + '&category_id=' + ".$model->id.";
                                
                                $.post('/admin/index.php?r=ptag/pcategorychange', request, function(response) {
                                    console.log(response);
                                });
                            }"
                            ]
                        ])?>
                    <?php } ?>
                </div>
            </div>


            <?= $form->field($model, 'image')->widget(FileInput::classname(), [
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'options' => ['class' => 'form-control'],
                // Widget template
                'template' => '<div class="url-manager-image img"><img src="'.$model->image.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                // Optional, if set, only this image can be selected by user
                'thumb' => 'original',
                // Optional, if set, in container will be inserted selected image
                'imageContainer' => '.img',
                // Default to FileInput::DATA_URL. This data will be inserted in input field
                'pasteData' => FileInput::DATA_URL,
                // JavaScript function, which will be called before insert file data to input.
                // Argument data contains file data.
                // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                'callbackBeforeInsert' => 'function(e, data) {
                        console.log( data );
                    }',
            ]); ?>

            <?php
                if ($model->parent_id == 0) {
                    echo $form->field($model, 'price')->textInput(['maxlength' => true]);
                    echo $form->field($model, 'period')->textInput(['maxlength' => true]);
                }
            ?>
