<?php
use common\models\Portfolio;
use kartik\widgets\Select2;
use pendalf89\filemanager\widgets\FileInput;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\Html;
use zxbodya\yii2\galleryManager\GalleryManager;

?>
<br>
<?php if ($model->parent_id == 6) { ?>
    <div class="form-group field-review-type">
        <label class="control-label col-sm-3" for="review-type">Маленький слайдер(пропорции 450*330, минимум 450*330)</label>
        <div class="col-sm-6">
            <?php
                echo GalleryManager::widget(
                    [
                        'model' => $model,
                        'behaviorName' => 'galleryBehavior',
                        'apiRoute' => 'pcategory/galleryApi'
                    ]
                );
            ?>
        </div>
    </div>
<?php } ?>
<?php  $model->portfolioslider = explode(',', $model->portfolioslider); ?>
<?=$form->field($model, 'portfolioslider')->widget(Select2::classname(), [
    'data' => Portfolio::getPortfolios(),
    'maintainOrder' => true,
    'options' => [
        'placeholder' => 'Выберите портфолио ...',
        'multiple' => true
    ],
    'pluginOptions' => [
        'tags' => true,
        'tokenSeparators' => [',', ' ']
    ],
]);?>
<div class="panel panel-default">
    <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Слайдер</h4></div>
    <div class="panel-body">
        <?php
            DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'min' => 1, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $modelsPslider[0],
            'formId' => 'Pcategory',
            'formFields' => [
                'header',
                'text1',
                'price',
                'time',
                'text2',
                'image',
                'background',
            ],
        ]); ?>

        <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($modelsPslider as $i => $modelPslider): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Слайд</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                        // necessary for update action.
                        if (! $modelPslider->isNewRecord) {
                            echo Html::activeHiddenInput($modelPslider, "[{$i}]id");
                        }
                        ?>
                        <?= $form->field($modelPslider, "[{$i}]header")->textInput(['maxlength' => true]) ?>
                        <?= $form->field($modelPslider, "[{$i}]text1")->textInput(['maxlength' => true]) ?>
                        <?= $form->field($modelPslider, "[{$i}]price")->textInput(['maxlength' => true]) ?>
                        <?= $form->field($modelPslider, "[{$i}]time")->textInput(['maxlength' => true]) ?>
                        <?= $form->field($modelPslider, "[{$i}]text2")->textInput(['maxlength' => true]) ?>

                        <?= $form->field($modelPslider, "[{$i}]image")->widget(FileInput::classname(), [
                            'buttonTag' => 'button',
                            'buttonName' => 'Browse',
                            'buttonOptions' => ['class' => 'btn btn-default'],
                            'options' => ['class' => 'form-control'],
                            // Widget template
                            'template' => '<div class="url-manager-image imgI'.$i.'"><img src="'.$modelPslider->image.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                            // Optional, if set, only this image can be selected by user
                            'thumb' => 'original',
                            // Optional, if set, in container will be inserted selected image
                            'imageContainer' => '.imgI'.$i,
                            // Default to FileInput::DATA_URL. This data will be inserted in input field
                            'pasteData' => FileInput::DATA_URL,
                            // JavaScript function, which will be called before insert file data to input.
                            // Argument data contains file data.
                            // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                            'callbackBeforeInsert' => 'function(e, data) {
                                                console.log( data );
                                            }',
                        ]); ?>
                        <?= $form->field($modelPslider, "[{$i}]background")->widget(FileInput::classname(), [
                            'buttonTag' => 'button',
                            'buttonName' => 'Browse',
                            'buttonOptions' => ['class' => 'btn btn-default'],
                            'options' => ['class' => 'form-control'],
                            // Widget template
                            'template' => '<div class="url-manager-image imgB'.$i.'"><img src="'.$modelPslider->background.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                            // Optional, if set, only this image can be selected by user
                            'thumb' => 'original',
                            // Optional, if set, in container will be inserted selected image
                            'imageContainer' => '.imgB'.$i,
                            // Default to FileInput::DATA_URL. This data will be inserted in input field
                            'pasteData' => FileInput::DATA_URL,
                            // JavaScript function, which will be called before insert file data to input.
                            // Argument data contains file data.
                            // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                            'callbackBeforeInsert' => 'function(e, data) {
                                                console.log( data );
                                            }',
                        ]); ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php DynamicFormWidget::end(); ?>
    </div>
</div>