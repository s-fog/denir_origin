<?php

use common\models\Personal;
use kartik\date\DatePicker;
use kartik\widgets\Select2;
use mihaildev\ckeditor\CKEditor;
use pendalf89\filemanager\widgets\FileInput;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;
use zxbodya\yii2\galleryManager\GalleryManager;

/**
* @var yii\web\View $this
* @var common\models\Portfolio $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="portfolio-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Portfolio',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-danger'
    ]
    );
    ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>

        <p>


            <?=$form->field($model, 'type')->widget(Select2::classname(), [
                'data' => [
                    'Портрет' => 'Портрет',
                    'Модульная' => 'Модульная',
                    'Шарж' => 'Шарж',
                    'Картина' => 'Картина',
                    'Роспись стен' => 'Роспись стен',
                    'Багеты' => 'Багеты'
                ],
                'options' => ['placeholder' => 'Выберите тип ...']
            ]);?>
            <?php if (!$model->isNewRecord) { ?>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="pcategory-parent_id">Теги</label>
                    <div class="col-sm-6">
                        <?php
                        echo Select2::widget([
                            'name' => 'tags',
                            'value' => \common\models\Ptag::portfolioTags($model->id), // initial value
                            'data' => \common\models\Ptag::tags(),
                            'maintainOrder' => true,
                            'options' => ['placeholder' => 'Выберите теги ...', 'multiple' => true],
                            'pluginOptions' => [
                                'tags' => true,
                                'maximumInputLength' => 100,
                                'class' => 'gfgfgfg'
                            ],
                            'pluginEvents' => [
                                "change" => "function(event) { 
                                        var values = $(this).val();
                                        var request = 'values=' + values + '&portfolio_id=' + ".$model->id.";
                                        
                                        $.post('/admin/index.php?r=ptag/portfoliochange', request, function(response) {
                                            console.log(response);
                                        });
                                    }",
                                /*"select2:opening" => "function() { console.log('opening');}",
                                "select2:open" => "function() { console.log('open'); }",
                                "select2:closing" => "function() { console.log('closing'); }",
                                "select2:close" => "function() { console.log('close');}",
                                "select2:selecting" => "function() { console.log('selecting'); }",
                                "select2:select" => "function() { console.log('select'); }",
                                "select2:unselecting" => "function() { console.log('unselecting'); }",
                                "select2:unselect" => "function() { console.log('unselect'); }"*/
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            <?php } else {?>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="pcategory-parent_id">Теги</label>
                    <div class="col-sm-6">
                        Теги станут доступны после сохранения портфолио
                    </div>
                </div>
            <?php } ?>
<!-- attribute borderimage -->
            <?= $form->field($model, 'borderimage')->widget(FileInput::classname(), [
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'options' => ['class' => 'form-control'],
                // Widget template
                'template' => '<div class="url-manager-image img1"><img src="'.$model->borderimage.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                // Optional, if set, only this image can be selected by user
                'thumb' => 'original',
                // Optional, if set, in container will be inserted selected image
                'imageContainer' => '.img1',
                // Default to FileInput::DATA_URL. This data will be inserted in input field
                'pasteData' => FileInput::DATA_URL,
                // JavaScript function, which will be called before insert file data to input.
                // Argument data contains file data.
                // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                'callbackBeforeInsert' => 'function(e, data) {
                    console.log( data );
                }',
            ]); ?>
        <?//= $form->field($model, 'withborder')->checkbox()->label('Изображение с рамкой?'); ?>

<!-- attribute bigimage -->
            <?= $form->field($model, 'bigimage')->widget(FileInput::classname(), [
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'options' => ['class' => 'form-control'],
                // Widget template
                'template' => '<div class="url-manager-image img2"><img src="'.$model->bigimage.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                // Optional, if set, only this image can be selected by user
                'thumb' => 'original',
                // Optional, if set, in container will be inserted selected image
                'imageContainer' => '.img2',
                // Default to FileInput::DATA_URL. This data will be inserted in input field
                'pasteData' => FileInput::DATA_URL,
                // JavaScript function, which will be called before insert file data to input.
                // Argument data contains file data.
                // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                'callbackBeforeInsert' => 'function(e, data) {
                    console.log( data );
                }',
            ]); ?>

            <?= $form->field($model, 'previewimage')->widget(FileInput::classname(), [
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'options' => ['class' => 'form-control'],
                // Widget template
                'template' => '<div class="url-manager-image img243"><img src="'.$model->previewimage.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                // Optional, if set, only this image can be selected by user
                'thumb' => 'original',
                // Optional, if set, in container will be inserted selected image
                'imageContainer' => '.img243',
                // Default to FileInput::DATA_URL. This data will be inserted in input field
                'pasteData' => FileInput::DATA_URL,
                // JavaScript function, which will be called before insert file data to input.
                // Argument data contains file data.
                // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                'callbackBeforeInsert' => 'function(e, data) {
                        console.log( data );
                    }',
            ]); ?>

<!-- attribute borderbg -->
            <?= $form->field($model, 'borderbg')->widget(FileInput::classname(), [
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'options' => ['class' => 'form-control'],
                // Widget template
                'template' => '<div class="url-manager-image img3"><img src="'.$model->borderbg.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                // Optional, if set, only this image can be selected by user
                'thumb' => 'original',
                // Optional, if set, in container will be inserted selected image
                'imageContainer' => '.img3',
                // Default to FileInput::DATA_URL. This data will be inserted in input field
                'pasteData' => FileInput::DATA_URL,
                // JavaScript function, which will be called before insert file data to input.
                // Argument data contains file data.
                // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                'callbackBeforeInsert' => 'function(e, data) {
                    console.log( data );
                }',
            ]); ?>

            <?= $form->field($model, 'bordersliderbg')->widget(FileInput::classname(), [
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'options' => ['class' => 'form-control'],
                // Widget template
                'template' => '<div class="url-manager-image img35"><img src="'.$model->bordersliderbg.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                // Optional, if set, only this image can be selected by user
                'thumb' => 'original',
                // Optional, if set, in container will be inserted selected image
                'imageContainer' => '.img35',
                // Default to FileInput::DATA_URL. This data will be inserted in input field
                'pasteData' => FileInput::DATA_URL,
                // JavaScript function, which will be called before insert file data to input.
                // Argument data contains file data.
                // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                'callbackBeforeInsert' => 'function(e, data) {
                    console.log( data );
                }',
            ]); ?>

            <!-- attribute interierimage -->
            <?= $form->field($model, 'interierimage')->widget(FileInput::classname(), [
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'options' => ['class' => 'form-control'],
                // Widget template
                'template' => '<div class="url-manager-image img4"><img src="'.$model->interierimage.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                // Optional, if set, only this image can be selected by user
                'thumb' => 'original',
                // Optional, if set, in container will be inserted selected image
                'imageContainer' => '.img4',
                // Default to FileInput::DATA_URL. This data will be inserted in input field
                'pasteData' => FileInput::DATA_URL,
                // JavaScript function, which will be called before insert file data to input.
                // Argument data contains file data.
                // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                'callbackBeforeInsert' => 'function(e, data) {
                    console.log( data );
                }',
            ]); ?>

            <?= $form->field($model, 'interierimage2')->widget(FileInput::classname(), [
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'options' => ['class' => 'form-control'],
                // Widget template
                'template' => '<div class="url-manager-image img4"><img src="'.$model->interierimage2.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                // Optional, if set, only this image can be selected by user
                'thumb' => 'original',
                // Optional, if set, in container will be inserted selected image
                'imageContainer' => '.img4',
                // Default to FileInput::DATA_URL. This data will be inserted in input field
                'pasteData' => FileInput::DATA_URL,
                // JavaScript function, which will be called before insert file data to input.
                // Argument data contains file data.
                // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                'callbackBeforeInsert' => 'function(e, data) {
                    console.log( data );
                }',
            ]); ?>

            <?= $form->field($model, 'interierimage3')->widget(FileInput::classname(), [
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'options' => ['class' => 'form-control'],
                // Widget template
                'template' => '<div class="url-manager-image img4"><img src="'.$model->interierimage3.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                // Optional, if set, only this image can be selected by user
                'thumb' => 'original',
                // Optional, if set, in container will be inserted selected image
                'imageContainer' => '.img4',
                // Default to FileInput::DATA_URL. This data will be inserted in input field
                'pasteData' => FileInput::DATA_URL,
                // JavaScript function, which will be called before insert file data to input.
                // Argument data contains file data.
                // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                'callbackBeforeInsert' => 'function(e, data) {
                    console.log( data );
                }',
            ]); ?>

            <?= $form->field($model, 'interierimage4')->widget(FileInput::classname(), [
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'options' => ['class' => 'form-control'],
                // Widget template
                'template' => '<div class="url-manager-image img4"><img src="'.$model->interierimage4.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                // Optional, if set, only this image can be selected by user
                'thumb' => 'original',
                // Optional, if set, in container will be inserted selected image
                'imageContainer' => '.img4',
                // Default to FileInput::DATA_URL. This data will be inserted in input field
                'pasteData' => FileInput::DATA_URL,
                // JavaScript function, which will be called before insert file data to input.
                // Argument data contains file data.
                // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                'callbackBeforeInsert' => 'function(e, data) {
                    console.log( data );
                }',
            ]); ?>

            <?= $form->field($model, 'interierimage5')->widget(FileInput::classname(), [
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'options' => ['class' => 'form-control'],
                // Widget template
                'template' => '<div class="url-manager-image img4"><img src="'.$model->interierimage5.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                // Optional, if set, only this image can be selected by user
                'thumb' => 'original',
                // Optional, if set, in container will be inserted selected image
                'imageContainer' => '.img4',
                // Default to FileInput::DATA_URL. This data will be inserted in input field
                'pasteData' => FileInput::DATA_URL,
                // JavaScript function, which will be called before insert file data to input.
                // Argument data contains file data.
                // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                'callbackBeforeInsert' => 'function(e, data) {
                    console.log( data );
                }',
            ]); ?>

            <?= $form->field($model, 'interierimage6')->widget(FileInput::classname(), [
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'options' => ['class' => 'form-control'],
                // Widget template
                'template' => '<div class="url-manager-image img4"><img src="'.$model->interierimage6.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                // Optional, if set, only this image can be selected by user
                'thumb' => 'original',
                // Optional, if set, in container will be inserted selected image
                'imageContainer' => '.img4',
                // Default to FileInput::DATA_URL. This data will be inserted in input field
                'pasteData' => FileInput::DATA_URL,
                // JavaScript function, which will be called before insert file data to input.
                // Argument data contains file data.
                // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                'callbackBeforeInsert' => 'function(e, data) {
                    console.log( data );
                }',
            ]); ?>


            <div class="form-group field-review-type">
                <label class="control-label col-sm-3" for="review-type">Исходные изображения(пропорции 50x50)</label>
                <div class="col-sm-6">
                    <?php
                    if ($model->isNewRecord) {
                        echo 'Нельзя начать загружать изображения для несохраненного портфолио';
                    } else {
                        echo GalleryManager::widget(
                            [
                                'model' => $model,
                                'behaviorName' => 'galleryBehavior',
                                'apiRoute' => 'portfolio/galleryApi'
                            ]
                        );
                    }
                    ?>
                </div>
            </div>

<!-- attribute creationdate -->
            <?=$form->field($model, 'creationdate')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату создания ...'],
                'pluginOptions' => [
                    'format' => 'dd.mm.yyyy',
                    'autoclose'=>true,
                    'todayHighlight' => false
                ]
            ]);?>

<!-- attribute personal_id -->
            <?=$form->field($model, 'personal_id')->widget(Select2::classname(), [
                'data' => Personal::getDesigners(),
                'options' => ['placeholder' => 'Выберите художника ...']
            ]);?>

<!-- attribute dop -->

        <?= $form->field($model, 'dop')->textarea() ?>

<!-- attribute material -->
			<?= $form->field($model, 'material')->textInput(['maxlength' => true]) ?>

<!-- attribute size -->
			<?= $form->field($model, 'size')->textInput(['maxlength' => true]) ?>

<!-- attribute creationperiod -->
			<?= $form->field($model, 'creationperiod')->textInput(['maxlength' => true]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    Tabs::widget(
                 [
                    'encodeLabels' => false,
                    'items' => [ 
                        [
    'label'   => Yii::t('models', 'Portfolio'),
    'content' => $this->blocks['main'],
    'active'  => true,
],
                    ]
                 ]
    );
    ?>
        <hr/>

        <?php echo $form->errorSummary($model); ?>

        <?= Html::submitButton(
        '<span class="glyphicon glyphicon-check"></span> ' .
        ($model->isNewRecord ? 'Создать' : 'Сохранить'),
        [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-success',
            'name' => 'mode',
            'value' => 'justSave'
        ]
        );
        ?>
       <?=Html::submitButton(
           '<span class="glyphicon glyphicon-check"></span> ' .
           ($model->isNewRecord ? 'Создать и выйти' : 'Сохранить и выйти'),
           [
               'class' => 'btn btn-success',
               'name' => 'mode',
               'value' => 'saveAndExit'
           ]
       );?>

        <?php ActiveForm::end(); ?>

    </div>

</div>

