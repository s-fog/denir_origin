<?php
use common\models\Ctag;
use kartik\select2\Select2;
use pendalf89\filemanager\widgets\FileInput;

?>
<br>
<?=$form->field($model, 'name')->textInput(['maxlength' => true]) ?>
<?=$form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
<?=$form->field($model, 'artikul')->textInput(['maxlength' => true]) ?>
<div class="form-group">
    <label class="control-label col-sm-3">Теги</label>
    <div class="col-sm-6">
        <?php if($model->isNewRecord) { ?>
            Теги станут доступны после сохранения
        <?php } else { ?>
            <?=Select2::widget([
                'name' => 'tags',
                'value' => Ctag::productTags($model->id), // initial value
                'data' => Ctag::tags(),
                'maintainOrder' => true,
                'options' => ['placeholder' => 'Выберите теги ...', 'multiple' => true],
                'pluginOptions' => [
                    'tags' => true,
                    'maximumInputLength' => 100,
                ],
                'pluginEvents' => [
                    "change" => "function(event) { 
                        var values = $(this).val();
                        var request = 'values=' + values + '&product_id=' + ".$model->id.";
                                
                        $.post('/admin/index.php?r=ctag/productchange', request, function(response) {
                            console.log(response);
                        });
                     }"
                ]
            ])?>
        <?php } ?>
    </div>
</div>
<?= $form->field($model, 'type')
    ->radioList([
        'Картина' => 'Картина',
        'Репродукция' => 'Репродукция',
        'Модульная картина' => 'Модульная картина',
        'Модульная картина маслом' => 'Модульная картина маслом',
        'Образ' => 'Образ'
    ]) ?>

<?= $form->field($model, 'image')->widget(FileInput::classname(), [
    'buttonTag' => 'button',
    'buttonName' => 'Browse',
    'buttonOptions' => ['class' => 'btn btn-default'],
    'options' => ['class' => 'form-control'],
    // Widget template
    'template' => '<div class="url-manager-image img"><img src="'.$model->image.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
    // Optional, if set, only this image can be selected by user
    'thumb' => 'original',
    // Optional, if set, in container will be inserted selected image
    'imageContainer' => '.img',
    // Default to FileInput::DATA_URL. This data will be inserted in input field
    'pasteData' => FileInput::DATA_URL,
    // JavaScript function, which will be called before insert file data to input.
    // Argument data contains file data.
    // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
    'callbackBeforeInsert' => 'function(e, data) {
                        console.log( data );
                    }',
]); ?>

<?= $form->field($model, 'previewimage')->widget(FileInput::classname(), [
    'buttonTag' => 'button',
    'buttonName' => 'Browse',
    'buttonOptions' => ['class' => 'btn btn-default'],
    'options' => ['class' => 'form-control'],
    // Widget template
    'template' => '<div class="url-manager-image img2"><img src="'.$model->previewimage.'" alt=""></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
    // Optional, if set, only this image can be selected by user
    'thumb' => 'original',
    // Optional, if set, in container will be inserted selected image
    'imageContainer' => '.img2',
    // Default to FileInput::DATA_URL. This data will be inserted in input field
    'pasteData' => FileInput::DATA_URL,
    // JavaScript function, which will be called before insert file data to input.
    // Argument data contains file data.
    // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
    'callbackBeforeInsert' => 'function(e, data) {
                        console.log( data );
                    }',
]); ?>
