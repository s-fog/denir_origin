<?php

use common\models\Pcategory;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;
use zxbodya\yii2\galleryManager\GalleryManager;

/**
* @var yii\web\View $this
* @var common\models\Review $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="review-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Review',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-danger'
    ]
    );
    ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>

        <p>


<!-- attribute name -->
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<!-- attribute city -->
			<?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

<!-- attribute portfolio_id -->
            <?=$form->field($model, 'portfolio_id')->widget(Select2::classname(), [
                'data' => \common\models\Portfolio::getPortfolios(),
                'options' => ['placeholder' => 'Выберите портфолио ...']
            ]);?>


            <?=$form->field($model, 'pcategory_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Pcategory::find()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Выберите категорию ...']
            ]);?>

<!-- attribute text -->
			<?= $form->field($model, 'introtext')->textarea(['rows' => 6]) ?>

			<?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

<!-- attribute type -->
			<?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>
        </p>

        <div class="form-group field-review-type">
            <label class="control-label col-sm-3" for="review-type">Изображения(1000x600)</label>
            <div class="col-sm-6">
                <?php
                if ($model->isNewRecord) {
                    echo 'Нельзя начать загружать изображения для несохраненного отзыва';
                } else {
                    echo GalleryManager::widget(
                        [
                            'model' => $model,
                            'behaviorName' => 'galleryBehavior',
                            'apiRoute' => 'review/galleryApi'
                        ]
                    );
                }
                ?>
            </div>
        </div>

        <?php $this->endBlock(); ?>

        <?=
    Tabs::widget(
                 [
                    'encodeLabels' => false,
                    'items' => [ 
                        [
    'label'   => Yii::t('models', 'Review'),
    'content' => $this->blocks['main'],
    'active'  => true,
],
                    ]
                 ]
    );
    ?>
        <hr/>

        <?php echo $form->errorSummary($model); ?>


        <?= Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> ' .
            ($model->isNewRecord ? 'Создать' : 'Сохранить'),
            [
                'id' => 'save-' . $model->formName(),
                'class' => 'btn btn-success',
                'name' => 'mode',
                'value' => 'justSave'
            ]
        );
        ?>
        <?=Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> ' .
            ($model->isNewRecord ? 'Создать и выйти' : 'Сохранить и выйти'),
            [
                'class' => 'btn btn-success',
                'name' => 'mode',
                'value' => 'saveAndExit'
            ]
        );?>

        <?php ActiveForm::end(); ?>

    </div>

</div>

