<?php
use common\models\Pcategory;
use common\models\Portfolio;
use kartik\widgets\Select2;
use mihaildev\ckeditor\CKEditor;
use pendalf89\filemanager\widgets\FileInput;
use zxbodya\yii2\galleryManager\GalleryManager;

?>
    <br>
<?= $form->field($model, 'pagetitle')->textInput(['maxlength' => true]) ?>

<!-- attribute alias -->
<?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'text1')->widget(CKEditor::className(), ['editorOptions' =>['preset' => 'standard' ]]); ?>

</p>

<?php if ($model->id == 4) { ?>
    <div class="form-group field-review-type">
        <label class="control-label col-sm-3" for="review-type">Изображения</label>
        <div class="col-sm-6">
            <?php
            if ($model->isNewRecord) {
                echo 'Нельзя начать загружать изображения для несохраненного отзыва';
            } else {
                echo GalleryManager::widget(
                    [
                        'model' => $model,
                        'behaviorName' => 'galleryBehavior',
                        'apiRoute' => 'textpage/galleryApi'
                    ]
                );
            }
            ?>
        </div>
    </div>
<?php } ?>
            