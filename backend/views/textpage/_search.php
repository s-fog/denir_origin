<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\TextpageSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="textpage-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'pagetitle') ?>

		<?= $form->field($model, 'menutitle') ?>

		<?= $form->field($model, 'alias') ?>

		<?= $form->field($model, 'text1') ?>

		<?php // echo $form->field($model, 'text2') ?>

		<?php // echo $form->field($model, 'text3') ?>

		<?php // echo $form->field($model, 'seo_title') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
