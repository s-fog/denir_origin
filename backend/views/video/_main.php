<?php
use common\models\Ctag;
use kartik\select2\Select2;
use mihaildev\ckeditor\CKEditor;
use pendalf89\filemanager\widgets\FileInput;

?>
<br>
<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
<?=$form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <!-- attribute undername -->
<?= $form->field($model, 'undername')->textInput(['maxlength' => true]) ?>

    <!-- attribute video -->
<?= $form->field($model, 'video')->textInput(['maxlength' => true]) ?>

    <!-- attribute introtext -->
<?= $form->field($model, 'introtext')->widget(CKEditor::className(), ['editorOptions' =>['preset' => 'standard' ]]); ?>

    <!-- attribute text -->
<?= $form->field($model, 'text')->widget(CKEditor::className(), ['editorOptions' =>['preset' => 'standard' ]]); ?>
