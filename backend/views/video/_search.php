<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\VideoSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="video-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'name') ?>

		<?= $form->field($model, 'video') ?>

		<?= $form->field($model, 'undername') ?>

		<?= $form->field($model, 'introtext') ?>

		<?php // echo $form->field($model, 'text') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

		<?php // echo $form->field($model, 'seotitle') ?>

		<?php // echo $form->field($model, 'alias') ?>

		<?php // echo $form->field($model, 'seoh1') ?>

		<?php // echo $form->field($model, 'seokeywords') ?>

		<?php // echo $form->field($model, 'seoheader') ?>

		<?php // echo $form->field($model, 'seodescription') ?>

		<?php // echo $form->field($model, 'seotext') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
