<aside class="main-sidebar">

    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Главная страница', 'url' => ['/main/update', 'id' => 1]],
                    ['label' => 'Текстовые страницы', 'url' => ['/textpage']],
                    ['label' => 'Отзывы', 'url' => ['/review']],
                    ['label' => 'Видео', 'url' => ['/video']],
                    ['label' => 'Города', 'url' => ['/city']],
                    ['label' => 'Акция', 'url' => ['/akcia/update', 'id' => 1]],
                    [
                        'label' => 'Каталог',
                        'options' => ['class' => 'header'],
                        'url' => '#',
                        'items' => [
                            ['label' => 'Категории', 'url' => ['/catalog']],
                            ['label' => 'Продукт', 'url' => ['/product']],
                            ['label' => 'Багеты', 'url' => ['/baget']],
                        ]
                    ],
                    [
                        'label' => 'Портфолио',
                        'options' => ['class' => 'header'],
                        'url' => '#',
                        'items' => [
                            ['label' => 'Категории', 'url' => ['/pcategory']],
                            ['label' => 'Портфолио', 'url' => ['/portfolio']]
                        ]
                    ],
                    [
                        'label' => 'Теги',
                        'options' => ['class' => 'header'],
                        'url' => '#',
                        'items' => [
                            ['label' => 'Теги для портфолио', 'url' => ['/ptag']],
                            ['label' => 'Теги для каталога', 'url' => ['/ctag']],
                        ]
                    ],
                    [
                        'label' => 'Слайдеры',
                        'options' => ['class' => 'header'],
                        'url' => '#',
                        'items' => [
                            ['label' => 'Слайдер на мастерской', 'url' => ['/sliderformaster']]
                        ]
                    ],
                    [
                        'label' => 'Персонал',
                        'options' => ['class' => 'header'],
                        'url' => '#',
                        'items' => [
                            ['label' => 'Персонал', 'url' => ['/personal']],
                            ['label' => 'Категории персонала', 'url' => ['/personalcategory']],
                        ]
                    ],
                    ['label' => 'Фото студии', 'icon' => 'file-code-o', 'url' => ['/gallery/gallery/update', 'id' => 1]],
                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                ],
            ]
        ) ?>

    </section>

</aside>
