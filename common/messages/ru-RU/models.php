<?php

return [
    'Textpage' => 'Текстовая страница',
    'Textpages' => 'Текстовые страницы',
    'Personals' => 'Персонал',
    'Personal' => 'Сотрудник',
    'Personalcategories' => 'Категории персонала',
    'Personalcategory' => 'Категория персонала',
    'Sliderformasters' => 'Слайдер для мастерской',
    'Sliderformaster' => 'Слайд',
    'Reviews' => 'Отзывы',
    'Review' => 'Отзыв',
    'Videos' => 'Видео',
    'Video' => 'Видео',
    'Catalogs' => 'Категории',
    'Catalog' => 'Категория',
    'Products' => 'Продукты',
    'Product' => 'Продукт',
    'Bagets' => 'Багеты',
    'Baget' => 'Багет',
    'Pcategories' => 'Категории',
    'Pcategory' => 'Категория',
    'Portfolios' => 'Портфолио',
    'Portfolio' => 'Портфолио',
    'Ptags' => 'Теги для портфолио',
    'Ptag' => 'Тег для портфолио',
    'Ctags' => 'Теги для каталога',
    'Ctag' => 'Тег для каталога',
];

?>