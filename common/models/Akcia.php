<?php

namespace common\models;

use Yii;
use \common\models\base\Akcia as BaseAkcia;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "akcia".
 */
class Akcia extends BaseAkcia
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => SluggableBehavior::className(),
                    'attribute' => 'pagetitle',
                    'slugAttribute' => 'alias',
                    'immutable' => true
                ],
            ]
        );
    }

    public function rules()
    {
        return [
            [['pagetitle'], 'required'],
            [['text1', 'text2', 'seodescription', 'image'], 'string'],
            [['products'], 'safe'],
            [['pagetitle', 'alias', 'seotitle', 'seokeywords', 'seoh1'], 'string', 'max' => 255]
        ];
    }
}
