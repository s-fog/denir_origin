<?php

namespace common\models;

use Yii;
use \common\models\base\Baget as BaseBaget;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "baget".
 */
class Baget extends BaseBaget
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'image' => 'Изображение(305x300)',
            'artikul' => 'Артикул',
            'width' => 'Ширина',
            'price' => 'Цена',
        ];
    }
}
