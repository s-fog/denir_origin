<?php

namespace common\models;

use Yii;
use \common\models\base\Catalog as BaseCatalog;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "catalog".
 */
class Catalog extends BaseCatalog
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => SluggableBehavior::className(),
                    'attribute' => 'name',
                    'slugAttribute' => 'alias',
                ]
            ]
        );
    }

    public function rules()
    {
        return [
            [['name', 'parent_id'], 'required'],
            [['seodescription', 'seotext', 'cost', 'delivery', 'canvas'], 'string'],
            [['parent_id'], 'integer'],
            [['name', 'type'], 'string', 'max' => 100],
            [['seotitle', 'alias', 'seoh1', 'seokeywords', 'seoheader'], 'string', 'max' => 255]
        ];
    }

    public static function getCatalog() {
        $catalog = Catalog::find()->all();
        $result = [];
        $result[0] = 'Нет родителя';

        foreach($catalog as $p) {
            $result[$p['id']] = $p['name'];
        }

        return $result;
    }

    public static function getMenu() {
        $catalog = Catalog::find()->where(['parent_id' => 0])->asArray()->all();
        $menu = [];

        foreach($catalog as $page) {
            $menu[] = ['label' => $page['name'], 'url' => ['catalog/index', 'alias' => $page['alias']]];
        }

        return $menu;
    }

    public function getCtagsids()
    {
        return $this->hasMany(CatalogHasCtag::className(), ['catalog_id' => 'id']);
    }

    public function getCtags()
    {
        return $this->hasMany(Ctag::className(), ['id' => 'ctag_id'])
            ->via('ctagsids');
    }

    public static function getProducts($id, $limit, $page = 1, $mode = 'limit', $search = '') {
        $catalog = Catalog::find()->where(['id' => $id])->one();
        $catalogCtags = [];
        $type = '';

        if ($catalog->parent_id == 0) {
            $catalogs = Catalog::find()->where(['parent_id' => $id])->all();
            $type = $catalog->type;
            $ids = [];

            foreach($catalogs as $cat) {
                $ids[] = $cat->id;
            }

            $cc = CatalogHasCtag::find()->where(['catalog_id' => $ids])->asArray()->all();//Получаем id тегов этой категории
        } else {
            $parent = Catalog::findOne($catalog->parent_id);
            $type = $parent->type;
            $cc = CatalogHasCtag::find()->where(['catalog_id' => $id])->asArray()->all();//Получаем id тегов этой категории
        }

        foreach($cc as $c) {
            $catalogCtags[] = $c['ctag_id'];
        }

        $catalogCtags = array_unique($catalogCtags);

        if (!empty($_GET['ctag_id'])) {
            $catalogCtags = $_GET['ctag_id'];
        }

        if (!empty($search)) {
            $andWhere = ['like', 'product.name', $search];
        } else {
            $andWhere = [];
        }

        if ($id == 3 || $catalog->parent_id == 3) {// id категории
            if (isset($_GET['type']) && !empty($_GET['type'])) {
                $andWhereType = ['type' => $_GET['type']];
            } else {
                $andWhereType = ['type' => 'Модульная картина'];;
            }
        } else {
            $andWhereType = ['type' => $type];
        }

        if (!empty($_GET['sort'])) {
            $sort = explode('_', $_GET['sort']);
            $orderBy = [$sort[0], $sort[1]];
        } else {
            $orderBy = ['product.created_at' => SORT_DESC];
        }

        if ($mode == 'all') {
            $products = Product::find()
                ->innerJoin('product_has_ctag phc', 'phc.product_id = product.id')
                ->where([
                    'phc.ctag_id' => $catalogCtags
                ])
                ->andWhere($andWhere)
                ->andWhere($andWhereType)
                ->orderBy($orderBy)
                ->groupBy('id')
                ->all();
        } else {
            $products = Product::find()
                ->innerJoin('product_has_ctag phc', 'phc.product_id = product.id')
                ->where([
                    'phc.ctag_id' => $catalogCtags
                ])
                ->andWhere($andWhere)
                ->andWhere($andWhereType)
                ->offset(($page - 1) * $limit)
                ->limit($limit)
                ->orderBy($orderBy)
                ->groupBy('id')
                ->all();
        }

        return $products;
    }
}
