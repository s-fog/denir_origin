<?php

namespace common\models;

use Yii;
use \common\models\base\CatalogHasCtag as BaseCatalogHasCtag;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "catalog_has_ctag".
 */
class CatalogHasCtag extends BaseCatalogHasCtag
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }
}
