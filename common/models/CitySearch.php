<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\City;

/**
* CitySearch represents the model behind the search form about `common\models\City`.
*/
class CitySearch extends City
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id'], 'integer'],
            [['name', 'period', 'price', 'html', 'header', 'seotitle', 'seokeywords', 'seodescription', 'seoh1'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = City::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'period', $this->period])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'html', $this->html])
            ->andFilterWhere(['like', 'header', $this->header])
            ->andFilterWhere(['like', 'seotitle', $this->seotitle])
            ->andFilterWhere(['like', 'seokeywords', $this->seokeywords])
            ->andFilterWhere(['like', 'seodescription', $this->seodescription])
            ->andFilterWhere(['like', 'seoh1', $this->seoh1]);

return $dataProvider;
}
}