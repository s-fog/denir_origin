<?php

namespace common\models;

use Yii;
use \common\models\base\Ctag as BaseCtag;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ctag".
 */
class Ctag extends BaseCtag
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }

    public static function tags() {
        $tags = Ctag::find()->asArray()->all();
        $result = [];

        foreach($tags as $tag) {
            $result[$tag['id']] = $tag['name'];
        }

        return $result;
    }

    public static function catalogTags($catalog_id) {
        $query = new Query;
        $query->select('ctag.id, ctag.name')
            ->from('ctag')
            ->join('INNER JOIN', 'catalog_has_ctag chc', 'chc.catalog_id = '.$catalog_id)->where('ctag.id = chc.ctag_id')
            ->orderBy('ctag.id')
            ->all();
        $rows = $query->all();
        $command = $query->createCommand();
        $tags = $command->queryAll();
        $result = [];

        foreach($tags as $tag) {
            $result[] = $tag['id'];
        }

        return $result;
    }

    public static function productTags($product_id) {
        $query = new Query;
        $query->select('ctag.id, ctag.name')
            ->from('ctag')
            ->join('INNER JOIN', 'product_has_ctag phc', 'phc.product_id = '.$product_id)->where('ctag.id = phc.ctag_id')
            ->orderBy('ctag.id')
            ->all();
        $rows = $query->all();
        $command = $query->createCommand();
        $tags = $command->queryAll();
        $result = [];

        foreach($tags as $tag) {
            $result[] = $tag['id'];
        }

        return $result;
    }
}
