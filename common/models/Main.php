<?php

namespace common\models;

use Yii;
use \common\models\base\Main as BaseMain;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "main".
 */
class Main extends BaseMain
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return [
            [['seodescription', 'seotext', 'phone', 'address', 'work_time'], 'string'],
            [['name'], 'string', 'max' => 100],
            [['portfolioslider'], 'safe'],
            [['seotitle', 'seokeywords', 'seoheader', 'seoh1'], 'string', 'max' => 255]
        ];
    }

    public function getPslider() {
        return $this->hasMany(Pslider::className(), ['main_id' => 'id']);
    }
}
