<?php

namespace common\models;

use Yii;
use \common\models\base\Pcategory as BasePcategory;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\galleryManager\GalleryBehavior;

/**
 * This is the model class for table "pcategory".
 */
class Pcategory extends BasePcategory
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => SluggableBehavior::className(),
                    'attribute' => 'name',
                    'slugAttribute' => 'alias',
                ],
                'galleryBehavior' => [
                    'class' => GalleryBehavior::className(),
                    'type' => 'pcategory',
                    'extension' => 'png',
                    'directory' => Yii::getAlias('@www') . '/uploads/pcategory',
                    'url' => Yii::getAlias('@www') . '/uploads/pcategory',
                    'timeHash' => false,
                    'versions' => [
                        'small' => function ($img) {
                            /** @var \Imagine\Image\ImageInterface $img */
                            return $img
                                ->copy()
                                ->thumbnail(new \Imagine\Image\Box(450, 330));
                        },
                        'medium' => function ($img) {
                            /** @var Imagine\Image\ImageInterface $img */
                            $dstSize = $img->getSize();
                            $maxWidth = 560;
                            if ($dstSize->getWidth() > $maxWidth) {
                                $dstSize = $dstSize->widen($maxWidth);
                            }
                            return $img
                                ->copy()
                                ->resize($dstSize);
                        },
                    ]
                ]
            ]
        );
    }

    public function rules()
    {
        return [
            [['name', 'image', 'parent_id', 'type', 'inmenu'], 'required'],
            [['parent_id', 'inmenu'], 'integer'],
            [['portfolioslider'], 'safe'],
            [['description', 'seodescription', 'seotext'], 'string'],
            [['name', 'type', 'price', 'period'], 'string', 'max' => 100],
            [['alias', 'image', 'smallimage', 'seotitle', 'seokeywords', 'seoheader', 'seoh1'], 'string', 'max' => 255]
        ];
    }

    public static function getPcategories() {
        $pcategories = Pcategory::find()->all();
        $result = [];
        $result[0] = 'Нет родителя';

        foreach($pcategories as $p) {
            $result[$p['id']] = $p['name'];
        }

        return $result;
    }

    public static function getMenu() {
        $pcategories = Pcategory::find()
            ->where(['parent_id' => 0])
            ->andWhere(['inmenu' => 1])
            ->asArray()
            ->all();
        $menu = [];

        foreach($pcategories as $page) {
            $menu[] = ['label' => $page['name'], 'url' => ['pcategory/index', 'alias' => $page['alias']]];
        }

        return $menu;
    }

    public function getPslider() {
        return $this->hasMany(Pslider::className(), ['pcategory_id' => 'id']);
    }

    public static function getCategories($id) {
        return Pcategory::find()->where(['parent_id' => $id])->all();
    }

    public static function getPortfolios($id, $limit = 999999, $offset = 0) {
        if ($id == 0) {
            $pcategories = Pcategory::find()->where(['parent_id' => 0])->all();

            $pp = PcategoryHasPtag::find()
                ->where(['pcategory_id' => ArrayHelper::map($pcategories, 'id', 'id')])
                ->asArray()
                ->all();//Получаем id тегов этой категории

            $pcategoryPtags = ArrayHelper::map($pp, 'id', 'id');

            $portfolios = Portfolio::find()
                ->orderBy(['created_at' => SORT_DESC])
                ->limit($limit)
                ->offset($offset)
                ->groupBy('id')
                ->all();

        } else {
            $pcategory = Pcategory::findOne($id);
            $pcategoryPtags = [];

            /*if ($pcategory->parent_id == 0) {
                $pcategories = Pcategory::find()->where(['parent_id' => $id])->all();
                if ($pcategories) {
                    $ids = [];

                    foreach($pcategories as $pcategory) {
                        $ids[] = $pcategory->id;
                    }

                    $pp = PcategoryHasPtag::find()->where(['pcategory_id' => $ids])->asArray()->all();//Получаем id тегов этой категории
                } else {
                    $pp = PcategoryHasPtag::find()->where(['pcategory_id' => $id])->asArray()->all();//Получаем id тегов этой категории
                }
            } else {
                $pp = PcategoryHasPtag::find()->where(['pcategory_id' => $id])->asArray()->all();//Получаем id тегов этой категории
            }*/
            $pp = PcategoryHasPtag::find()->where(['pcategory_id' => $id])->asArray()->all();//Получаем id тегов этой категории

            foreach($pp as $p) {
                $pcategoryPtags[] = $p['ptag_id'];
            }

            if (!empty($_GET['ptag_id'])) {
                $pcategoryPtags = [$_GET['ptag_id']];
            }

            $portfolios = Portfolio::find()
                ->innerJoin('portfolio_has_ptag pht', 'pht.portfolio_id = portfolio.id')
                ->where(['pht.ptag_id' => $pcategoryPtags, 'portfolio.type' => $pcategory->type])
                ->limit($limit)
                ->offset($offset)
                ->orderBy(['created_at' => SORT_DESC])
                ->groupBy('id')
                ->all();
        }

        return $portfolios;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Урл',
            'image' => 'Изображение(ширина 357x201)',
            'smallimage' => 'Маленькое изображение(пропорции 59*47)',
            'parent_id' => 'Родитель',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'description' => 'Описание',
            'type' => 'Тип',
            'portfolioslider' => 'Слайдер по портфолио(Если заполнено то будет отображаться этот слайдер вместо нижнего)',
            'seotitle' => 'Seo title',
            'seokeywords' => 'Seo keywords',
            'seodescription' => 'Seo description',
            'seotext' => 'Текст',
            'seoheader' => 'Заголовок',
            'seoh1' => 'Seo h1',
            'price' => 'Цена от',
            'period' => 'Срок изготовления от',
            'inmenu' => 'Показывать в меню?',
        ];
    }

    public function getPtagsids()
    {
        return $this->hasMany(PcategoryHasPtag::className(), ['pcategory_id' => 'id']);
    }

    public function getPtags()
    {
        return $this->hasMany(Ptag::className(), ['id' => 'ptag_id'])
            ->via('ptagsids');
    }
}
