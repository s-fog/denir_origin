<?php

namespace common\models;

use Yii;
use \common\models\base\PcategoryHasPtag as BasePcategoryHasPtag;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "pcategory_has_ptag".
 */
class PcategoryHasPtag extends BasePcategoryHasPtag
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }
}
