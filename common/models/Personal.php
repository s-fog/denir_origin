<?php

namespace common\models;

use Yii;
use \common\models\base\Personal as BasePersonal;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "personal".
 */
class Personal extends BasePersonal
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }

    public function getPersonalcategory()
    {
        return $this->hasOne(Personalcategory::className(), ['id' => 'personalcategory_id']);
    }
    
    public static function getDesigners() {
        $personal = Personal::find()->where(['personalcategory_id' => 2])->asArray()->all();
        $designers = [];

        foreach($personal as $person) {
            $designers[$person['id']] = $person['name'];
        }

        return $designers;
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'function' => 'Должность',
            'image' => 'Фото(250*199)',
            'text' => 'Текст',
            'personalcategory_id' => 'Категория',
        ];
    }
}
