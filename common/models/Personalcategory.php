<?php

namespace common\models;

use Yii;
use \common\models\base\Personalcategory as BasePersonalcategory;
use \common\models\Personal;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "personalcategory".
 */
class Personalcategory extends BasePersonalcategory
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }

    public static function allPersonal() {
        $result = [];
        $categories = Personalcategory::find()
            ->orderBy([new \yii\db\Expression("FIELD(id, 2, 1)")])
            ->all();

        foreach ($categories as $key=>$category) {
            $personal = Personal::find()->where(['personalcategory_id' => $category->id])->all();
            $result[$key]['category'] = $category->name;
            $result[$key]['personal'] = $personal;
        }

        return $result;
    }
}
