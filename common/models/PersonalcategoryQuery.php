<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Personalcategory]].
 *
 * @see Personalcategory
 */
class PersonalcategoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Personalcategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Personalcategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
