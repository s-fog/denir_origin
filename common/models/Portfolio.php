<?php

namespace common\models;

use Yii;
use \common\models\base\Portfolio as BasePortfolio;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\galleryManager\GalleryBehavior;

/**
 * This is the model class for table "portfolio".
 */
class Portfolio extends BasePortfolio
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'galleryBehavior' => [
                    'class' => GalleryBehavior::className(),
                    'type' => 'portfolio',
                    'extension' => 'png',
                    'directory' => Yii::getAlias('@www') . '/uploads/portfolio',
                    'url' => Yii::getAlias('@www') . '/uploads/portfolio',
                    'timeHash' => false,
                    'versions' => [
                        'small' => function ($img) {
                            /** @var \Imagine\Image\ImageInterface $img */
                            return $img
                                ->copy()
                                ->thumbnail(new \Imagine\Image\Box(55, 55));
                        },
                        'medium' => function ($img) {
                            /** @var Imagine\Image\ImageInterface $img */
                            $dstSize = $img->getSize();
                            $maxWidth = 560;
                            if ($dstSize->getWidth() > $maxWidth) {
                                $dstSize = $dstSize->widen($maxWidth);
                            }
                            return $img
                                ->copy()
                                ->resize($dstSize);
                        },
                    ]
                ]
            ]
        );
    }

    public function rules()
    {
        return [
            [['bigimage', 'borderbg', 'type', 'previewimage'], 'required'],
            [['personal_id', 'withborder'], 'integer'],
            [['creationdate', 'submit'], 'safe'],
            [['dop'], 'string'],
            [['borderimage', 'bigimage', 'borderbg', 'bordersliderbg', 'interierimage', 'interierimage2', 'interierimage3', 'interierimage4', 'interierimage5', 'interierimage6', 'material', 'size', 'creationperiod', 'previewimage'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 100],
            [['interierimage'], 'required', 'when' => function($model) {
                $flag = false;

                if (!empty($model->interierimage2)) {
                    $flag = true;
                }
                if (!empty($model->interierimage3)) {
                    $flag = true;
                }
                if (!empty($model->interierimage4)) {
                    $flag = true;
                }
                if (!empty($model->interierimage5)) {
                    $flag = true;
                }
                if (!empty($model->interierimage6)) {
                    $flag = true;
                }

                return $flag;
            },'whenClient' => "function(attribute, value) {
                var flag = true;
                
                if ($('#portfolio-interierimage2').val().length == 0) {
                    flag = false;
                }
                if ($('#portfolio-interierimage3').val().length == 0) {
                    flag = false;
                }
                if ($('#portfolio-interierimage4').val().length == 0) {
                    flag = false;
                }
                if ($('#portfolio-interierimage5').val().length == 0) {
                    flag = false;
                }
                if ($('#portfolio-interierimage6').val().length == 0) {
                    flag = false;
                }
                
                return flag;
            }", 'message' => 'Сначала заполните это'
            ],
        ];
    }

    public static function getPortfolios() {
        $portfolio = Portfolio::find()->asArray()->all();
        $result = [0 => 'Нет работы'];

        foreach($portfolio as $p) {
            $id = ''.$p['id'].'';
            $result[$id] = 'Заказ№'.$id;
        }

        return $result;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'borderimage' => 'Изображение в рамке(максимальный размер 540x320)',
            'bigimage' => 'Большое изображение(размер любой)',
            'borderbg' => 'Изображение на фоне(749x450)',
            'bordersliderbg' => 'Изображение на фоне в слайдере(840x480)',
            'interierimage' => 'Изображение интерьера (749x450)',
            'interierimage2' => 'Изображение интерьера2 (749x450)',
            'interierimage3' => 'Изображение интерьера3 (749x450)',
            'interierimage4' => 'Изображение интерьера4 (749x450)',
            'interierimage5' => 'Изображение интерьера5 (749x450)',
            'interierimage6' => 'Изображение интерьера6 (749x450)',
            'creationdate' => 'Дата создания',
            'personal_id' => 'Художник',
            'material' => 'Материал',
            'size' => 'Размер',
            'creationperiod' => 'Срок создания',
            'dop' => 'Дополнение',
            'type' => 'Type',
            'previewimage' => 'Превью для каталога(310x270)',
            'withborder' => 'Изображение с рамкой?',
        ];
    }
}
