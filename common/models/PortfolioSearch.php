<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Portfolio;

/**
* PortfolioSearch represents the model behind the search form about `common\models\Portfolio`.
*/
class PortfolioSearch extends Portfolio
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'created_at', 'updated_at', 'creationdate', 'personal_id'], 'integer'],
            [['borderimage', 'bigimage', 'borderbg', 'interierimage', 'material', 'size', 'creationperiod', 'dop'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Portfolio::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'creationdate' => $this->creationdate,
            'personal_id' => $this->personal_id,
        ]);

        $query->andFilterWhere(['like', 'borderimage', $this->borderimage])
            ->andFilterWhere(['like', 'bigimage', $this->bigimage])
            ->andFilterWhere(['like', 'borderbg', $this->borderbg])
            ->andFilterWhere(['like', 'interierimage', $this->interierimage])
            ->andFilterWhere(['like', 'material', $this->material])
            ->andFilterWhere(['like', 'size', $this->size])
            ->andFilterWhere(['like', 'creationperiod', $this->creationperiod])
            ->andFilterWhere(['like', 'dop', $this->dop]);

return $dataProvider;
}
}