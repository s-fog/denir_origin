<?php

namespace common\models;

use Yii;
use \common\models\base\Product as BaseProduct;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product".
 */
class Product extends BaseProduct
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => SluggableBehavior::className(),
                    'attribute' => 'name',
                    'slugAttribute' => 'alias',
                ]
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }

    public function getCtagsids()
    {
        return $this->hasMany(ProductHasCtag::className(), ['product_id' => 'id']);
    }

    public function getCtags()
    {
        return $this->hasMany(Ctag::className(), ['id' => 'ctag_id'])
            ->via('ctagsids');
    }

    public static function getType() {
        return [
            'Картина' => 'Картина',
            'Репродукция' => 'Репродукция',
            'Модульная картина' => 'Модульная картина',
            'Модульная картина маслом' => 'Модульная картина маслом',
            'Образ' => 'Образ',
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'alias' => 'Алиас',
            'seotitle' => 'Seo title',
            'seoh1' => 'Seo h1',
            'seokeywords' => 'Seo keywords',
            'seoheader' => 'Заголовок',
            'image' => 'Изображение',
            'previewimage' => 'Превью-изображение для каталога (225x230)',
            'artikul' => 'Артикул',
            'seodescription' => 'Seo description',
            'seotext' => 'Текст',
            'type' => 'Тип картины',
        ];
    }
}
