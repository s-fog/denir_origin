<?php

namespace common\models;

use Yii;
use \common\models\base\ProductHasCtag as BaseProductHasCtag;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product_has_ctag".
 */
class ProductHasCtag extends BaseProductHasCtag
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }
}
