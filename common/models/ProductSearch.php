<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Product;

/**
* ProductSearch represents the model behind the search form about `common\models\Product`.
*/
class ProductSearch extends Product
{
/**
* @inheritdoc
*/
public function rules()
{
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'alias', 'type', 'seotitle', 'seoh1', 'seokeywords', 'seoheader', 'seodescription', 'seotext'], 'safe'],
        ];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Product::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'seotitle', $this->seotitle])
            ->andFilterWhere(['like', 'seoh1', $this->seoh1])
            ->andFilterWhere(['like', 'seokeywords', $this->seokeywords])
            ->andFilterWhere(['like', 'seoheader', $this->seoheader])
            ->andFilterWhere(['like', 'seodescription', $this->seodescription])
            ->andFilterWhere(['type' => $this->type])
            ->andFilterWhere(['like', 'seotext', $this->seotext]);

return $dataProvider;
}
}