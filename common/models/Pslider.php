<?php

namespace common\models;

use Yii;
use \common\models\base\Pslider as BasePslider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "pslider".
 */
class Pslider extends BasePslider
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pcategory_id' => 'id категории портфолио',
            'portfolio_id' => 'id портфолио',
            'main_id' => 'id главной',
            'header' => 'Заголовок',
            'text1' => 'Текст1',
            'price' => 'Цена',
            'text2' => 'Текст2',
            'background' => 'Фон(840x480)',
            'image' => 'Изображение(840x480)',
            'time' => 'Время',
        ];
    }
}
