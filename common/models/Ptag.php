<?php

namespace common\models;

use Yii;
use \common\models\base\Ptag as BasePtag;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ptag".
 */
class Ptag extends BasePtag
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }

    public static function tags() {
        $tags = Ptag::find()->asArray()->all();
        $result = [];

        foreach($tags as $tag) {
            $result[$tag['id']] = $tag['name'];
        }

        return $result;
    }

    public static function pcategoryTags($pcategory_id) {
        $query = new Query;
        $query->select('ptag.id, ptag.name')
            ->from('ptag')
            ->join('INNER JOIN', 'pcategory_has_ptag cht', 'cht.pcategory_id = '.$pcategory_id)->where('ptag.id = cht.ptag_id')
            ->orderBy('ptag.id')
            ->all();
        $rows = $query->all();
        $command = $query->createCommand();
        $tags = $command->queryAll();
        $result = [];

        foreach($tags as $tag) {
            $result[] = $tag['id'];
        }

        return $result;
    }

    public static function portfolioTags($portfolio_id) {
        $query = new Query;
        $query->select('ptag.id, ptag.name')
            ->from('ptag')
            ->join('INNER JOIN', 'portfolio_has_ptag pht', 'pht.portfolio_id = '.$portfolio_id)->where('ptag.id = pht.ptag_id')
            ->orderBy('ptag.id')
            ->all();
        $rows = $query->all();
        $command = $query->createCommand();
        $tags = $command->queryAll();
        $result = [];

        foreach($tags as $tag) {
            $result[] = $tag['id'];
        }

        return $result;
    }
}
