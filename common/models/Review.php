<?php

namespace common\models;

use Yii;
use \common\models\base\Review as BaseReview;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\galleryManager\GalleryBehavior;

/**
 * This is the model class for table "review".
 */
class Review extends BaseReview
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'galleryBehavior' => [
                    'class' => GalleryBehavior::className(),
                    'type' => 'review',
                    'extension' => 'png',
                    'directory' => Yii::getAlias('@www') . '/uploads/reviews',
                    'url' => Yii::getAlias('@www') . '/uploads/reviews',
                    'timeHash' => false,
                    'versions' => [
                        'small' => function ($img) {
                            /** @var \Imagine\Image\ImageInterface $img */
                            return $img
                                ->copy()
                                ->thumbnail(new \Imagine\Image\Box(200, 200));
                        },
                        'medium' => function ($img) {
                            /** @var Imagine\Image\ImageInterface $img */
                            $dstSize = $img->getSize();
                            $maxWidth = 560;
                            if ($dstSize->getWidth() > $maxWidth) {
                                $dstSize = $dstSize->widen($maxWidth);
                            }
                            return $img
                                ->copy()
                                ->resize($dstSize);
                        },
                    ]
                ]
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'city' => 'Город',
            'type' => 'Тип',
            'text' => 'Текст',
            'portfolio_id' => 'Портфолио',
            'pcategory_id' => 'Категория в которой будет отображаться отзыв(Если выбрано Портфолио, то это поле игнорируется)',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата модификации',
        ];
    }

    public static function firstReviews($pcategory_id) {
        $limit = 4;
        $portfolios = Pcategory::getPortfolios($pcategory_id);
        $potfoliosId = ArrayHelper::map($portfolios, 'id', 'id');
        $reviews = Review::find()
            ->where(['portfolio_id' => $potfoliosId])
            ->orWhere("pcategory_id = {$pcategory_id} AND portfolio_id = 0")
            ->limit($limit)
            ->orderBy(['created_at'=>SORT_DESC])
            ->all();
        return $reviews;
    }

    public static function allReviews($pcategory_id) {
        $portfolios = Pcategory::getPortfolios($pcategory_id);
        $potfoliosId = ArrayHelper::map($portfolios, 'id', 'id');
        $reviews = Review::find()
            ->where(['portfolio_id' => $potfoliosId])
            ->orWhere("pcategory_id = {$pcategory_id} AND portfolio_id = 0")
            ->orderBy(['created_at'=>SORT_DESC])
            ->all();
        return $reviews;
    }
}
