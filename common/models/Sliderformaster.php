<?php

namespace common\models;

use Yii;
use \common\models\base\Sliderformaster as BaseSliderformaster;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sliderformaster".
 */
class Sliderformaster extends BaseSliderformaster
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Изображение(ширина от 960)',
            'header' => 'Заголовок',
            'text' => 'Текст',
        ];
    }
}
