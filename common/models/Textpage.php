<?php

namespace common\models;

use Yii;
use \common\models\base\Textpage as BaseTextpage;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\galleryManager\GalleryBehavior;

/**
 * This is the model class for table "textpage".
 */
class Textpage extends BaseTextpage
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'galleryBehavior' => [
                    'class' => GalleryBehavior::className(),
                    'type' => 'textpage',
                    'extension' => 'png',
                    'directory' => Yii::getAlias('@www') . '/uploads/textpage',
                    'url' => Yii::getAlias('@www') . '/uploads/textpage',
                    'timeHash' => false,
                    'versions' => [
                        'small' => function ($img) {
                            /** @var \Imagine\Image\ImageInterface $img */
                            return $img
                                ->copy()
                                ->thumbnail(new \Imagine\Image\Box(200, 200));
                        },
                        'medium' => function ($img) {
                            /** @var Imagine\Image\ImageInterface $img */
                            $dstSize = $img->getSize();
                            $maxWidth = 241;
                            if ($dstSize->getWidth() > $maxWidth) {
                                $dstSize = $dstSize->widen($maxWidth);
                            }
                            return $img
                                ->copy()
                                ->resize($dstSize);
                        },
                    ]
                ],
                [
                    'class' => SluggableBehavior::className(),
                    'attribute' => 'pagetitle',
                    'slugAttribute' => 'alias',
                    'immutable' => true
                ],
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }

    public static function getMenu() {
        $textpages = Textpage::find()
            ->orderBy([new \yii\db\Expression("FIELD(id, 1, 3, 2, 4, 6, 5)")])
            ->asArray()
            ->all();

        foreach($textpages as $page) {
            $menu[] = ['label' => $page['pagetitle'], 'url' => ['textpage/index', 'alias' => $page['alias']]];
        }

        return $menu;
    }
}
