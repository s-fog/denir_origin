<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Textpage]].
 *
 * @see Textpage
 */
class TextpageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Textpage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Textpage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
