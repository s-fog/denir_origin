<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "portfolio_has_ptag".
 *
 * @property integer $id
 * @property integer $portfolio_id
 * @property integer $ptag_id
 * @property string $aliasModel
 */
abstract class PortfolioHasPtag extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'portfolio_has_ptag';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['portfolio_id', 'ptag_id'], 'required'],
            [['portfolio_id', 'ptag_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'portfolio_id' => 'Portfolio ID',
            'ptag_id' => 'Ptag ID',
        ];
    }




}
