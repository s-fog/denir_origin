<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'resources/js/vendor/owl-carousel/owl-carousel/owl.carousel.css',
        'resources/js/vendor/fancybox/dist/jquery.fancybox.min.css',
        'resources/css/flipclock.css',
        'resources/css/styles.css',
    ];
    public $js = [
        'resources/js/vendor.js',
        'resources/js/flex-grid-add-elements.js',
        'resources/js/flipclock.js',
        'resources/js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
