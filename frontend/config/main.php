<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<alias>' => 'textpage/index',
                'portfolios/<alias>' => 'pcategory/index',
                'catalog/<alias>' => 'catalog/index',
                'catalog/item/<alias>' => 'catalog/view',
                'catalog/<parentAlias>/<alias>' => 'catalog/inner',
                'videoobzory/<alias>' => 'video/index',
                'city/<alias>' => 'city/index',
            ],
            'suffix' => '',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@frontend/views/mail',
            'htmlLayout' => 'main',
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => ['denir@yandex.ru' => 'Denir']
            ],
            'useFileTransport' => true,
        ],
    ],
    'params' => $params,
];
