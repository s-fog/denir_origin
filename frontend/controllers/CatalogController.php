<?php

namespace frontend\controllers;

use common\models\Baget;
use common\models\Catalog;
use common\models\Pcategory;
use common\models\Product;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class CatalogController extends \yii\web\Controller
{
    public function actions()
    {
        if (isset($_GET['type'])) {
            if ($_GET['type'] == 'Модульная картина') {
                $this->redirect(['catalog/index', 'alias' => Catalog::findOne(3)->alias]);
            }
        }
    }

    public function actionIndex($alias)
    {
        $this->layout = 'catalog';
        $model = Catalog::find()->where(['alias' => $alias])->one();
        return $this->logic($model);
    }

    public function actionInner($parentAlias, $alias)
    {
        $this->layout = 'catalog';
        $parent = Catalog::find()->where(['alias' => $parentAlias])->one();
        $model = Catalog::find()->where(['alias' => $alias, 'parent_id' => $parent->id])->one();
        return $this->logic($model);
    }

    public function actionView($alias)
    {
        $this->layout = 'catalog';
        $model = Product::find()->where(['alias' => $alias])->one();
        $topCategory = Catalog::find()->where(['type' => $model->type])->asArray()->one();

        return $this->render('view', [
            'model' => $model,
            'topCategory' => $topCategory,
            'cost' => $topCategory['cost'],
            'delivery' => $topCategory['delivery'],
            'canvas' => $topCategory['canvas']
        ]);
    }

    private function logic($model) {
        if (isset($_GET['search']) && !empty($_GET['search'])) {
            $product = Product::find()->where(['artikul' => $_GET['search']])->one();

            if (!empty($product)) {
                return $this->redirect(Url::to(['catalog/view', 'alias' => $product->alias]));
            }
        } else {
            $_GET['search'] = '';
        }

        if ($model->id == 5) {
            if (!empty($_GET['sort'])) {
                $sort = explode('__', $_GET['sort']);
                $orderBy = [$sort[0] => $sort[1]*1];
            } else {
                $orderBy = ['price' => SORT_ASC];
            }

            $bAndWhere = '';

            if (isset($_GET['widthFrom'])) {
                $bAndWhere = "width >={$_GET['widthFrom']} 
                AND width <={$_GET['widthTo']} 
                AND price >={$_GET['priceFrom']} 
                AND price <={$_GET['priceTo']} ";
            }

            $page = (!empty($_GET['page']))? $_GET['page'] : 1;
            $limit = 8;
            $bagetsOrigin = Baget::find()
                ->andWhere($bAndWhere)
                ->orderBy($orderBy)
                ->all();
            $bagetsCount = count($bagetsOrigin);
            $pages = [];

            if (isset($_GET['search']) && !empty($_GET['search'])) {
                $bagets = Baget::find()
                    ->where(['artikul' => $_GET['search']])
                    ->andWhere($bAndWhere)
                    ->orderBy($orderBy)
                    ->all();

                if (empty($bagets)) {
                    $bagets = Baget::find()
                        ->where(['like', 'name', $_GET['search']])
                        ->andWhere($bAndWhere)
                        ->orderBy($orderBy)
                        ->offset(($page - 1) * $limit)
                        ->limit($limit)
                        ->all();
                }
            }

            if (empty($bagets)) {
                $bagets = Baget::find()
                    ->andWhere($bAndWhere)
                    ->orderBy($orderBy)
                    ->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->all();
            }

            for ($i = 1; $i <= ceil($bagetsCount / $limit); $i++) {
                $pages[] = $i;
            }

            return $this->render('index-baget', [
                'model' => $model,
                'bagets' => $bagets,
                'pages' => $pages
            ]);
        } else {
            $categories = Catalog::find()->where(['parent_id' => $model->id])->all();

            if ($model->parent_id == 0) {
                $id = $model->id;
            } else {
                $id = $model->parent_id;
            }

            $outerCategories = Catalog::find()->where(['parent_id' => $id])->all();
            //////////////////////////////////////////////////////////////////////////////
            $page = (!empty($_GET['page']))? $_GET['page'] : 1;
            $limit = 10;
            $productsOrigin = Catalog::getProducts($model->id, $limit, $page, 'all', $_GET['search']);
            $products = Catalog::getProducts($model->id, $limit, $page, 'limit', $_GET['search']);
            $productsCount = count($productsOrigin);
            $pages = [];

            for ($i = 1; $i <= ceil($productsCount / $limit); $i++) {
                $pages[] = $i;
            }
            //////////////////////////////////////////////////////////////////////////////
            return $this->render('index', [
                'model' => $model,
                'categories' => $categories,
                'outerCategories' => $outerCategories,
                'products' => $products,
                'pages' => $pages
            ]);
        }
    }
}
