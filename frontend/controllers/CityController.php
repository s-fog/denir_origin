<?php

namespace frontend\controllers;

use common\models\City;
use Yii;
use yii\db\Query;

class CityController extends \yii\web\Controller
{
    public function actionIndex($alias)
    {
        $this->layout = 'textpage';
        $model = City::find()->where(['alias' => $alias])->one();

        return $this->render('@frontend/views/textpage/city',[
            'model' => $model
        ]);
    }
}
