<?php

namespace frontend\controllers;

use frontend\models\CallbackForm;
use frontend\models\PhotoForm;
use frontend\models\ProschetForm;
use frontend\models\RaschetForm;
use frontend\models\RequestForm;
use frontend\models\RequestForm2;
use Yii;

class MailController extends \yii\web\Controller
{
    public function actionIndex() {
        if (isset($_POST['CallbackForm'])) {
            if (!empty($_POST['CallbackForm']) && isset($_POST['CallbackForm']['type']) && !strlen($_POST['CallbackForm']['BC'])) {
                $form = new CallbackForm();
                $post = $_POST['CallbackForm'];
                $form->send($post, []);
            }
        } else if (isset($_POST['PhotoForm'])) {
            if (!empty($_POST['PhotoForm']) && isset($_POST['PhotoForm']['type']) && !strlen($_POST['PhotoForm']['BC'])) {
                $form = new PhotoForm();
                $post = $_POST['PhotoForm'];
                $form->send($post, (!empty($_FILES['PhotoForm'])? $_FILES['PhotoForm']: []));
            }
        } else if (isset($_POST['ProschetForm'])) {
            if (!empty($_POST['ProschetForm']) && isset($_POST['ProschetForm']['type']) && !strlen($_POST['ProschetForm']['BC'])) {
                $form = new ProschetForm();
                $post = $_POST['ProschetForm'];
                $form->send($post, []);
            }
        } else if (isset($_POST['RequestForm'])) {
            if (!empty($_POST['RequestForm']) && isset($_POST['RequestForm']['type']) && !strlen($_POST['RequestForm']['BC'])) {
                $form = new RequestForm();
                $post = $_POST['RequestForm'];
                $form->send($post, (!empty($_FILES['RequestForm'])? $_FILES['RequestForm']: []));
            }
        } else if (isset($_POST['RequestForm2'])) {
            if (!empty($_POST['RequestForm2']) && isset($_POST['RequestForm2']['type']) && !strlen($_POST['RequestForm2']['BC'])) {
                $form = new RequestForm2();
                $post = $_POST['RequestForm2'];
                $form->send($post, (!empty($_FILES['RequestForm2'])? $_FILES['RequestForm2']: []));
            }
        } else if (isset($_POST['RaschetForm'])) {
            if (!empty($_POST['RaschetForm']) && isset($_POST['RaschetForm']['type']) && !strlen($_POST['RaschetForm']['BC'])) {
                $form = new RaschetForm();
                $post = $_POST['RaschetForm'];
                $form->send($post, (!empty($_FILES['RaschetForm'])? $_FILES['RaschetForm']: []));
            }
        } else {
            if (!empty($_POST) && isset($_POST['type']) && !strlen($_POST['BC'])) {
                $form = new ProschetForm();
                $post = $_POST;
                $form->send($post, $_FILES);
            }
        }

    }
}
