<?php

namespace frontend\controllers;

use common\models\Pcategory;
use common\models\Portfolio;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class PcategoryController extends \yii\web\Controller
{
    public function actionIndex($alias)
    {
        $this->layout = 'portfolio';
        $model = Pcategory::find()->where(['alias' => $alias])->one();

        if ($model === null) {
            throw new NotFoundHttpException;
        }

        $portfolioslider = [];
        if (!empty($model->portfolioslider)) {
            $portfolioslider = Portfolio::find()->where(['id' => explode(',', $model->portfolioslider)])->all();
        }

        if ($model->parent_id == 0 && $model->id != 6) {
            return $this->render('index', [
                'model' => $model,
                'portfolioslider' => $portfolioslider
            ]);
        } else if ($model->parent_id == 6) {
            return $this->render('present', [
                'model' => $model
            ]);
        } else if ($model->id == 6) {
            $model = Pcategory::findOne(11);
            return $this->redirect(Url::to(['pcategory/index', 'alias' => $model->alias]));

            return $this->render('present', [
                'model' => $model
            ]);
        } else {
            return $this->render('inner', [
                'model' => $model,
                'portfolioslider' => $portfolioslider
            ]);
        }
    }

    public function actionLoadmore()
    {
        $portfolios = Pcategory::getPortfolios($_POST['id'], 12, $_POST['already']);
        $allPortfoliosCount = count(Pcategory::getPortfolios($_POST['id']));
        $availablePortfoliosCount = count($portfolios) + $_POST['already'];
        $result = [];

        foreach($portfolios as $portfolio) {
            $item = str_replace('"', "'", trim($this->renderPartial('@frontend/views/pcategory/_item', ['portfolio' => $portfolio])));
            $item = str_replace(array("\r", "\n", "\r\n", "\t"), "", $item);
            $result[] = $item;
        }

        return '{"items": "'.implode('', $result).'", 
        "availablePortfoliosCount": "'.$availablePortfoliosCount.'",
        "allPortfoliosCount": "'.$allPortfoliosCount.'"}';
    }
}
