<?php

namespace frontend\controllers;


use common\models\Portfolio;
use common\models\Review;

class PortfolioController extends \yii\web\Controller
{
    public function actionGetportfolio()
    {
        $portfolio = Portfolio::find()->where(['id' => $_POST['id']])->one();
        $this->layout = false;
        $review = Review::find()->where(['portfolio_id' => $portfolio->id])->one();

        return $this->render('item', [
            'portfolio' => $portfolio,
            'review' => $review
        ]);
    }
}
