<?php

namespace frontend\controllers;

use common\models\Pcategory;
use common\models\Review;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class ReviewController extends \yii\web\Controller
{
    public function actionIndex()
    {
        //return $this->render('index');
    }

    public function actionLoadmore() {
        $request = Yii::$app->request;
        $limit = 4;
        $query = new Query;
        $portfolios = Pcategory::getPortfolios($request->post('pcategory_id'));
        $potfoliosId = ArrayHelper::map($portfolios, 'id', 'id');
        $allReviews = $query->select('id')
            ->from('review')
            ->where(['portfolio_id' => $potfoliosId])
            ->orWhere("pcategory_id = {$request->post('pcategory_id')} AND portfolio_id = 0")
            ->all();
        $reviews = Review::find()
            ->orderBy(['created_at'=>SORT_DESC])
            ->offset($request->post('already'))
            ->where(['portfolio_id' => $potfoliosId])
            ->orWhere("pcategory_id = {$request->post('pcategory_id')} AND portfolio_id = 0")
            ->limit($limit)
            ->all();
        $availableReviewsCount = count($reviews) + $request->post('already');
        $allReviewsCount = count($allReviews);
        $result = [];

        foreach($reviews as $review) {
            $item = str_replace('"', "'", trim($this->renderPartial('@frontend/views/review/_item', ['review' => $review])));
            $item = str_replace(array("\r", "\n", "\r\n", "\t"), "", $item);
            $result[] = $item;
        }

        return '{"items": "'.implode('', $result).'", 
        "availableReviewsCount": "'.$availableReviewsCount.'",
        "allReviewsCount": "'.$allReviewsCount.'"}';
    }
}
