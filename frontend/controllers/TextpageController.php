<?php

namespace frontend\controllers;

use common\models\Akcia;
use common\models\City;
use common\models\Review;
use common\models\Textpage;
use common\models\Sliderformaster;
use common\models\Personal;
use common\models\Personalcategory;
use common\models\Video;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class TextpageController extends \yii\web\Controller
{
    public function actionIndex($alias)
    {
        $this->layout = 'textpage';
        $model = Textpage::find()->where(['alias' => $alias])->one();

        if ($modelAckia = Akcia::find()->where(['alias' => $alias])->one()) {
            return $this->render('akcia', [
                'model' => $modelAckia,
            ]);
        }

        if ($model === null) {
            throw new NotFoundHttpException;
        }

        if ($model->id == 1) {
            $slider = Sliderformaster::find()->all();

            return $this->render('masterskaya', [
                'model' => $model,
                'slider' => $slider,
            ]);
        } else if ($model->id == 2) {
            $citiesOrigin = City::find()->orderBy('name')->all();
            $cities = [];

            foreach($citiesOrigin as $city) {
                $firstLetter = ucfirst(substr($city->name, 0, 2));
                $cities[$firstLetter][] = $city;
            }

            return $this->render('dostavka', [
                'model' => $model,
                'cities' => $cities
            ]);
        } else if ($model->id == 3) {
            $query = Review::find();
            $countQuery = clone $query;
            $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 12]);
            $pages->pageSizeParam = false;

            $reviews = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

            return $this->render('reviews', [
                'model' => $model,
                'reviews' => $reviews,
                'pages' => $pages,
            ]);
        } else if ($model->id == 4) {
            return $this->render('serts', [
                'model' => $model
            ]);
        } else if ($model->id == 5) {
            $slider = Sliderformaster::find()->all();
            $artists = Personal::find()->where(['personalcategory_id' => '2'])->all();

            return $this->render('contacts', [
                'model' => $model,
                'slider' => $slider,
                'artists' => $artists
            ]);
        } else if ($model->id == 6) {
            $videos = Video::find()->all();

            return $this->render('video', [
                'model' => $model,
                'videos' => $videos,
            ]);
        } else {
            return $this->render('index', [
                'model' => $model
            ]);
        }
    }
}
