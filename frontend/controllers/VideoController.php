<?php

namespace frontend\controllers;

use common\models\Review;
use common\models\Video;
use Yii;
use yii\db\Query;

class VideoController extends \yii\web\Controller
{
    public function actionIndex($alias)
    {
        $this->layout = 'textpage';
        $model = Video::find()->where(['alias' => $alias])->one();

        return $this->render('index',[
            'model' => $model
        ]);
    }
}
