<?php

namespace frontend\models;

use Yii;

class PhotoForm extends Forms
{
    public $zakazN;
    
    public function rules()
    {
        return [
            [['phone'], 'required'],
            [['phone'], 'string', 'length' => [6]],
            [['file'], 'file', 'extensions' => 'jpg, gif, png, jpeg', 'maxFiles' => 10],
        ];
    }

}