<?php

namespace frontend\models;

use Yii;

class ProschetForm extends Forms
{
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['name'], 'string', 'length' => [2]],
            [['phone'], 'string', 'length' => [6]],
        ];
    }

}