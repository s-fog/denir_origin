<?php

namespace frontend\models;

use Yii;

class RaschetForm extends Forms
{
    public $product;
    public $email;

    public function rules()
    {
        return [
            [['phone'], 'required'],
            [['phone'], 'string', 'length' => [6]],
            [['file'], 'file', 'extensions' => 'jpg, jpeg, gif, png', 'maxFiles' => 10],
        ];
    }

}