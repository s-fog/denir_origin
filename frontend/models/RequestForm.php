<?php

namespace frontend\models;

use Yii;

class RequestForm extends Forms
{
    public function rules()
    {
        return [
            [['phone'], 'required'],
            [['phone'], 'string', 'length' => [6]],
            [['file'], 'file', 'extensions' => 'jpg, gif, png, jpeg', 'maxFiles' => 10],
        ];
    }

}