<?php foreach($models as $image) { ?>
    <a href="<?=$image->src?>" data-fancybox="photos" class="works__item">
        <div class="works__itemImage">
            <img src="<?=$image->src?>" alt="" width="310" height="270">
            <span></span>
        </div>
    </a>
<?php } ?>
<?php
if (count($models) % 4 == 3) {
    echo '<a class="works__item hide"></a>';
} else if(count($models) % 4 == 2) {
    echo '<a class="works__item hide"></a>';
    echo '<a class="works__item hide"></a>';
} else if(count($models) % 4 == 1) {
    echo '<a class="works__item hide"></a>';
    echo '<a class="works__item hide"></a>';
    echo '<a class="works__item hide"></a>';
}
?>