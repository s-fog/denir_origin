    <div class="advantages <?=$class?>">
        <div class="container">
            <div class="advantages__inner">
                <div class="advantages__item">
                    <div class="advantages__image">
                        <div style="background-image: url(/resources/img/advIcon1.png);"></div>
                        <span>01</span>
                    </div>
                    <div class="advantages__info">
                        <div class="advantages__header">Изготовление макета</div>
                        <div class="advantages__text">Компания Denir в течение 1 дня изготовит макет вашей будущей картины. </div>
                    </div>
                </div>
                <div class="advantages__item">
                    <div class="advantages__image">
                        <div style="background-image: url(/resources/img/advIcon4.png);"></div>
                        <span>04</span>
                    </div>
                    <div class="advantages__info">
                        <div class="advantages__header">Картины в интерьер</div>
                        <div class="advantages__text">Профессиональные дизайнеры всегда готовы подобрать картину к вашему интерьеру.</div>
                    </div>
                </div>
                <div class="advantages__item">
                    <div class="advantages__image">
                        <div style="background-image: url(/resources/img/advIcon2.png);"></div>
                        <span>02</span>
                    </div>
                    <div class="advantages__info">
                        <div class="advantages__header">Подарок вовремя</div>
                        <div class="advantages__text">В течение 1 дня мы изготовим картину для вашего подарка.</div>
                    </div>
                </div>
                <div class="advantages__item">
                    <div class="advantages__image">
                        <div style="background-image: url(/resources/img/advIcon5.png);"></div>
                        <span>05</span>
                    </div>
                    <div class="advantages__info">
                        <div class="advantages__header">Заказы круглосуточно</div>
                        <div class="advantages__text">24 часа 7 дней в неделю вы можете оформить заказ на нашем сайте.</div>
                    </div>
                </div>
                <div class="advantages__item">
                    <div class="advantages__image">
                        <div style="background-image: url(/resources/img/advIcon3.png);"></div>
                        <span>03</span>
                    </div>
                    <div class="advantages__info">
                        <div class="advantages__header">Срочный портрет маслом</div>
                        <div class="advantages__text">По желанию клиента мы готовы выполнить портрет маслом за 2 дня. </div>
                    </div>
                </div>
                <div class="advantages__item">
                    <div class="advantages__image">
                        <div style="background-image: url(/resources/img/advIcon6.png);"></div>
                        <span>06</span>
                    </div>
                    <div class="advantages__info">
                        <div class="advantages__header">Удобная доставка</div>
                        <div class="advantages__text">Доставка в города Украины
                            осуществляется в течении суток
                            службой доставок «Нова Пошта». </div>
                    </div>
                </div>
            </div>
        </div>
    </div>