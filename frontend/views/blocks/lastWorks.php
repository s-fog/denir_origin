<?php
use common\models\Pcategory;
use common\models\Portfolio;

?>
<div class="works" data-id="<?=(!empty($model->id) && isset($model->id)) ? $model->id : 0;?>">
    <div class="container">
        <div class="works__header">Последние работы</div>
        <div class="works__inner">
            <?php
            $limit = 12;

            if (!empty($model->id) && isset($model->id)) {
                $portfolios = Pcategory::getPortfolios($model->id, $limit);
                $allPortfoliosCount = Pcategory::getPortfolios($model->id);
            } else {
                $portfolios = Pcategory::getPortfolios(0, $limit);
                $allPortfoliosCount = Pcategory::getPortfolios(0);
            }
            
            foreach($portfolios as $portfolio) {  ?>
                <?=$this->render('@frontend/views/pcategory/_item', ['portfolio' => $portfolio])?>
            <?php }
            if (count($portfolios) % 4 == 1) {
                echo '<div class="works__item hide"></div>';
                echo '<div class="works__item hide"></div>';
                echo '<div class="works__item hide"></div>';
            } else if (count($portfolios) % 4 == 2) {
                echo '<div class="works__item hide"></div>';
                echo '<div class="works__item hide"></div>';
            } else if (count($portfolios) % 4 == 3) {
                echo '<div class="works__item hide"></div>';
            }
            ?>
        </div>
        <?php
            if (count($portfolios) < count($allPortfoliosCount)) {
                echo '<div class="works__more"><span>Больше готовых работ</span></div>';
            }
        ?>
    </div>
</div>
