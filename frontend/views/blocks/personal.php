<?php

use common\models\Personalcategory;

$allPersonal = Personalcategory::allPersonal();

?>

<div class="personal">
    <div class="container">
    <?php foreach($allPersonal as $category): ?>
        <div class="personal__category">
            <div class="personal__header"><span><?=$category['category']?></span></div>
            <div class="personal__inner">
                <?php foreach($category['personal'] as $personal): ?>
                    <div class="personal__item">
                        <div class="personal__image">
                            <img src="<?=$personal->image?>" alt="">
                            <?php if ($personal->text): ?>
                                <div class="personal__infoTrigger">i</div>
                                <div class="personal__info">
                                    <div class="personal__infoInner"><?=$personal->text?></div>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="personal__name"><?=$personal->name?></div>
                        <div class="personal__function"><?=$personal->function?></div>
                    </div>
                <?php endforeach; ?>
                <?php
                    $ost = count($category['personal']) % 4;

                    if ($ost == 2) {
                        echo '<div class="personal__item"></div>';
                        echo '<div class="personal__item"></div>';
                    } else if ($ost == 3) {
                        echo '<div class="personal__item"></div>';
                    }
                ?>
            </div>
        </div>
    <?php endforeach; ?>
    </div>
</div>