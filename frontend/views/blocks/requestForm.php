<?php
use frontend\models\RequestForm;
use frontend\models\RequestForm2;
use yii\widgets\ActiveForm;

if (!isset($id)) {
    $requestForm = new RequestForm();
    $form = ActiveForm::begin([
        'options' => [
            'class' => 'requestForm sendForm',
            'id' => 'requestForm'
        ],
    ]);?>
    <div class="container">
        <div class="requestForm__inner">
            <div class="requestForm__header">Сделать расчет</div>
            <div class="requestForm__text">Отправляйте фото и уже через 4 часа Вы получите возможные варианты будущей работы!</div>
            <div class="requestForm__fieldset">
                <?=$form->field($requestForm, 'file[]', [
                    'template' => "{input}\n{label}\n{hint}\n\n{error}"
                ])->fileInput(['multiple' => true])?>
                <?=$form->field($requestForm, 'phone')
                    ->textInput([
                        'class' => 'input',
                        'placeholder' => 'Телефон'
                    ])->label(false)?>
                <?=$form->field($requestForm, 'wishes')
                    ->textInput([
                        'class' => 'input',
                        'placeholder' => 'Пожелания'
                    ])->label(false)?>
            </div>
            <br>
            <button type="submit" class="button3">Отправить заявку</button>
            <div class="loading"></div>
        </div>
    </div>
    <?=$form->field($requestForm, 'type')
        ->hiddenInput([
            'value' => 'Загрузить фото с сайта denir'
        ])->label(false)?>

    <?=$form->field($requestForm, 'url')
        ->hiddenInput([
            'value' => $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']
        ])->label(false)?>

    <?=$form->field($requestForm, 'BC')
        ->hiddenInput([
            'value' => ''
        ])->label(false)?>

    <?php ActiveForm::end();?>   
<?php } else if ($id == 'requestForm2') {
    $requestForm = new RequestForm2();
    $form = ActiveForm::begin([
        'options' => [
            'class' => 'requestForm sendForm',
            'id' => $id
        ],
    ]);?>
    <div class="container">
        <div class="requestForm__inner">
            <div class="requestForm__header">Сделать расчет</div>
            <div class="requestForm__text">Отправляйте фото и уже через 4 часа Вы получите возможные варианты будущей работы!</div>
            <div class="requestForm__fieldset">
                <?=$form->field($requestForm, 'file[]', [
                    'template' => "{input}\n{label}\n{hint}\n\n{error}"
                ])->fileInput(['multiple' => true])?>
                <?=$form->field($requestForm, 'phone')
                    ->textInput([
                        'class' => 'input',
                        'placeholder' => 'Телефон'
                    ])->label(false)?>
                <?=$form->field($requestForm, 'wishes')
                    ->textInput([
                        'class' => 'input',
                        'placeholder' => 'Пожелания'
                    ])->label(false)?>
            </div>
            <br>
            <button type="submit" class="button3">Отправить заявку</button>
            <div class="loading"></div>
        </div>
    </div>
    <?=$form->field($requestForm, 'type')
        ->hiddenInput([
            'value' => 'Загрузить фото с сайта denir'
        ])->label(false)?>

    <?=$form->field($requestForm, 'url')
        ->hiddenInput([
            'value' => $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']
        ])->label(false)?>

    <?=$form->field($requestForm, 'BC')
        ->hiddenInput([
            'value' => ''
        ])->label(false)?>

    <?php ActiveForm::end();?>
    
<?php }