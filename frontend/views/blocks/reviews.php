<?php

use common\models\Review;

$limit = 4;

if (!isset($pcategory_id) && empty($pcategory_id)) {
    $pcategory_id = 0;
}

if (!isset($class)) {
    $class = '';
}

if (empty($reviews))
    $reviews = Review::firstReviews($pcategory_id);

$allReviews = Review::allReviews($pcategory_id);

?>
<div class="reviews<?=' '.$class?>" data-pcategoryid="<?=$pcategory_id?>">
    <div class="container">
        <div class="reviews__header">Отзывы клиентов</div>
        <div class="reviews__inner">
            <?php
                foreach($reviews as $review) {
                    echo $this->render('@frontend/views/review/_item', ['review' => $review]);
                }
            ?>
        </div>
    </div>
    <?=(count($reviews) < count($allReviews))? '<div class="reviews__more"><span>Больше отзывов</span></div>' : ''?>
</div>