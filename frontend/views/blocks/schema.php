<div class="schema">
    <div class="container">
        <div class="header"><span>Схема работы</span></div>
        <div class="schema__inner">
            <div class="schema__item">
                <div class="schema__itemLeft" style="background-image: url(/resources/img/pink1.png);"></div>
                <div class="schema__itemRight">
                    <div class="schema__itemHeader">ЗАЯВКА</div>
                    <div class="schema__itemText">Вы оставляете заявку или звоните нам по телефону: 8 (800) 505-251</div>
                </div>
            </div>
            <div class="schema__item">
                <div class="schema__itemLeft" style="background-image: url(/resources/img/pink2.png);"></div>
                <div class="schema__itemRight">
                    <div class="schema__itemHeader">Встреча</div>
                    <div class="schema__itemText">Наш дизайнер встретится
                        с вами и обсудит объект
                        в любое удобное для вас время.</div>
                </div>
            </div>
            <div class="schema__item">
                <div class="schema__itemLeft" style="background-image: url(/resources/img/pink3.png);"></div>
                <div class="schema__itemRight">
                    <div class="schema__itemHeader">Макет</div>
                    <div class="schema__itemText">Разработаем дизайн, составим смету,
                        согласуем и утвердим. Подпишем договор.</div>
                </div>
            </div>
            <div class="schema__item">
                <div class="schema__itemLeft" style="background-image: url(/resources/img/pink4.png);"></div>
                <div class="schema__itemRight">
                    <div class="schema__itemHeader">ФОтоотчет</div>
                    <div class="schema__itemText">Начинаем создавать чудо!
                        По плану дизайн-проекта каждый шаг контролируется. Формируем фотоотчет.</div>
                </div>
            </div>
            <div class="schema__item">
                <div class="schema__itemLeft" style="background-image: url(/resources/img/pink5.png);"></div>
                <div class="schema__itemRight">
                    <div class="schema__itemHeader">3D-лепнна</div>
                    <div class="schema__itemText">При необходимости сформируем 3D-лепнину. Получится объемная роспись. Невероятно привлекательно!</div>
                </div>
            </div>
            <div class="schema__item">
                <div class="schema__itemLeft" style="background-image: url(/resources/img/pink6.png);"></div>
                <div class="schema__itemRight">
                    <div class="schema__itemHeader">Сдача проекта</div>
                    <div class="schema__itemText">Вы становитесь счастливым клиентом нашей компании
                        и обладателем самой роскошной и самой красивой росписи.</div>
                </div>
            </div>
        </div>
    </div>
</div>