<?php
if (!isset($catalog)) $catalog = 0;
if (!isset($parent_id)) $parent_id = 0;
if (!isset($_GET['type'])) $_GET['type'] = 'Модульная картина';
?>
<form action="" class="search<?=(isset($class))? ' '.$class : ''?>">
    <?php if ($catalog == 3 || $parent_id == 3) { ?>
        <label class="radioWithImage">
            <input type="radio"
                   name="type"
                   value="Модульная картина"
                   <?=(!isset($_GET['type']) || $_GET['type'] == 'Модульная картина')? ' checked' : '';?>>
            <span class="radioWithImage__inner">
                <span class="radioWithImage__marker"></span>
                <span class="radioWithImage__text">Модульная картина</span>
            </span>
        </label>
        <label class="radioWithImage">
            <input type="radio"
                   name="type"
                   value="Модульная картина маслом"
                <?=($_GET['type'] == 'Модульная картина маслом')? ' checked' : '';?>>
            <span class="radioWithImage__inner">
                <span class="radioWithImage__marker"></span>
                <span class="radioWithImage__text">Модульная картина маслом</span>
            </span>
        </label>
    <?php } ?>
    <div class="search__inner">
        <input type="text" name="search" placeholder="Артикул или название" value="<?=$_GET['search']?>">
        <button type="submit"></button>
    </div>
    <input type="hidden" name="ctag_id" value="<?=(isset($_GET['ctag_id']))? $_GET['ctag_id'] : ''?>">
</form>