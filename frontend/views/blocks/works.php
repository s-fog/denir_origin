<div class="works">
    <div class="container">
        <div class="header">Фото студии</div>
        <div class="works__inner">
            <?= \sadovojav\gallery\widgets\Gallery::widget([
                'galleryId' => 1,
                'template' => '@frontend/views/blocks/_workItems'
            ]); ?>
        </div>
    </div>
</div>