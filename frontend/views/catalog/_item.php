<?php
use yii\helpers\Url;
$parent = \common\models\Catalog::findOne($category->parent_id);
$alias = Url::to(['catalog/inner', 'alias' => $category->alias, 'parentAlias' => $parent->alias]);
?>
<li>
    <a href="<?=$alias?>"><?=$category->name?></a>
    <ul>
        <?php foreach($category->ctags as $ctag) { ?>
            <?php
                $queryString = $_SERVER['QUERY_STRING'];
                $url = '';

                if (strpos($queryString, 'ctag_id=')) {
                    $url = preg_replace('#(.*ctag_id)=[0-9]+(.*)#siU', '$1='.$ctag->id.'$2', $queryString);
                } else {
                    if (empty($queryString)) {
                        $url = $queryString.'ctag_id='.$ctag->id;
                    } else {
                        $url = $queryString.'&ctag_id='.$ctag->id;
                    }
                }
            ?>
            <li><a href="<?=$alias?>?<?=$url?>"><?=$ctag->name?></a></li>
        <?php } ?>
    </ul>
</li>