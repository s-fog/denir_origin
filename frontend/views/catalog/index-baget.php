<?php
use common\models\Baget;
use common\models\Catalog;
use common\models\Ctag;
use common\models\Pcategory;
use yii\helpers\Url;

if (!empty($outerCategories)) {
    $this->params['categories'] = $outerCategories;
};
$this->params['pagetitle'] = $model->name;
$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;

if (!isset($_GET['sort'])) {
    $_GET['sort'] = '';
}
$widthMin = Baget::find()->min('width');
$widthMax = Baget::find()->max('width');
$priceMin = Baget::find()->min('price');
$priceMax = Baget::find()->max('price');
?>

    <div class="catalog">
        <form class="catalog__filter">
            <div class="container">
                <div class="catalog__filterInner">
                    <div class="catalog__filterItem">
                        <div class="catalog__filterText">Ширина:</div>
                        <div class="catalog__filterSlider filterWidth"></div>
                    </div>
                    <input type="hidden"
                           name="widthFrom"
                           value="<?=(isset($_GET['widthFrom']) && !empty($_GET['widthFrom']))? $_GET['widthFrom'] : $widthMin?>"
                           data-min="<?=$widthMin?>">
                    <input type="hidden"
                           name="widthTo"
                           value="<?=(isset($_GET['widthTo']) && !empty($_GET['widthTo']))? $_GET['widthTo'] : $widthMax?>"
                           data-max="<?=$widthMax?>">
                    <div class="catalog__filterItem">
                        <div class="catalog__filterText">Цена:</div>
                        <div class="catalog__filterSlider filterPrice"></div>
                    </div>
                    <input type="hidden"
                           name="priceFrom"
                           value="<?=(isset($_GET['priceFrom']) && !empty($_GET['priceFrom']))? $_GET['priceFrom'] : $priceMin?>"
                           data-min="<?=$priceMin?>">
                    <input type="hidden"
                           name="priceTo"
                           value="<?=(isset($_GET['priceTo']) && !empty($_GET['priceTo']))? $_GET['priceTo'] : $priceMax?>"
                           data-max="<?=$priceMax?>">
                    <div class="search__inner">
                        <input type="text" name="search" placeholder="Артикул или название" value="<?=$_GET['search']?>">
                        <button type="submit"></button>
                    </div>
                    <div class="sort">
                        <div class="sort__text">Cортировать:</div>
                        <div class="sort__item">
                            <select name="sort" class="sort__select <?=($_GET['sort'] == 'price__3')? ' reverse' : ''?>">
                                <option value="price__4"
                                    <?=($_GET['sort'] == 'price__4')? ' selected' : ''?>>По возрастанию цены</option>
                                <option value="price__3"
                                    <?=($_GET['sort'] == 'price__3')? ' selected' : ''?>>По убыванию цены</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="container">
            <div class="catalogBagets">
                <div class="catalogBagets__inner">
                    <?php foreach($bagets as $baget) { ?>
                        <div class="catalogBagets__item" data-fancybox="bagetPopup" data-src="#bagetPopup" data-type="inline">
                            <div class="catalogBagets__itemImage" style="background-image: url(<?=$baget->image?>);"></div>
                            <div class="catalogBagets__itemInfo">
                                <div class="catalogBagets__itemInfoInner">
                                    <div class="catalogBagets__itemName"><?=$baget->name?></div>
                                    <div class="catalogBagets__itemFeature">Артикул: <?=$baget->artikul?></div>
                                    <div class="catalogBagets__itemFeature">Ширина: <?=$baget->width?> см.</div>
                                    <div class="catalogBagets__itemFeature" style="margin-top: 5px;"><span class="catalogBagets__itemPrice"><?=$baget->price?></span> грн. за 1 метр </div>
                                </div>
                                <div class="catalogBagets__itemButton">Сделать расчет</div>
                            </div>
                        </div>
                    <?php }
                    if (count($bagets) % 4 == 1) {
                        echo '<div class="catalogBagets__item hide"></div>';
                        echo '<div class="catalogBagets__item hide"></div>';
                        echo '<div class="catalogBagets__item hide"></div>';
                    } else if (count($bagets) % 4 == 2) {
                        echo '<div class="catalogBagets__item hide"></div>';
                        echo '<div class="catalogBagets__item hide"></div>';
                    } else if (count($bagets) % 4 == 3) {
                        echo '<div class="catalogBagets__item hide"></div>';
                    }
                    ?>
                </div>
            </div>
            <ul class="pagination">
                <?php
                $currentPage = (isset($_GET['page']))? $_GET['page'] : 1;

                foreach($pages as $key => $page) {
                    $requestUri = $_SERVER['REQUEST_URI'];

                    if (strpos($requestUri, 'page=')) {
                        $url = preg_replace('#(.*page)=[0-9]+(.*)#siU', '$1='.$page.'$2', $requestUri);
                        $urlPrev = preg_replace('#(.*page)=[0-9]+(.*)#siU', '$1='.($currentPage-1).'$2', $requestUri);
                        $urlNext = preg_replace('#(.*page)=[0-9]+(.*)#siU', '$1='.($currentPage+1).'$2', $requestUri);
                    } else {
                        if (strpos($requestUri, '?')) {
                            $url = $requestUri.'&page='.$page;
                            $urlPrev = $requestUri.'&page='.($currentPage-1);
                            $urlNext = $requestUri.'&page='.($currentPage+1);
                        } else {
                            $url = $requestUri.'?page='.$page;
                            $urlPrev = $requestUri.'?page='.($currentPage-1);
                            $urlNext = $requestUri.'?page='.($currentPage+1);
                        }
                    }

                    if ($key == 0 && $currentPage - 1 != 0) {
                        echo '<li class="prev"><a href="'.$urlPrev.'"></a></li>';
                    }

                    if ($currentPage == $page) {
                        echo '<li><span>'.$page.'</span></li>';
                    } else {
                        echo '<li><a href="'.$url.'">'.$page.'</a></li>';
                    }

                    if (count($pages) == $key + 1 && $currentPage != count($pages)) {
                        echo '<li class="next"><a href="'.$urlNext.'"></a></li>';
                    }
                }
                ?>
            </ul>
        </div>
    </div>

<?= $this->render('@frontend/views/blocks/requestForm') ?>

<?= $this->render('@frontend/views/blocks/reviews') ?>

<?= $this->render('@frontend/views/blocks/advantages', ['class' => 'advantages_additional advantages_bottom']) ?>

<?= $this->render('@frontend/views/blocks/requestForm') ?>