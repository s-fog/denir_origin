<?php
use common\models\Catalog;
use common\models\Ctag;
use common\models\Pcategory;
use yii\helpers\Url;

if (!empty($outerCategories)) {
    $this->params['categories'] = $outerCategories;
};
$this->params['pagetitle'] = $model->name;
$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
if ($model->parent_id != 0) {
    $this->params['parent'] = Catalog::find()->where(['id' => $model->parent_id])->asArray()->one();
}
?>

    <div class="catalog">
        <div class="container">
            <?php if ($model->parent_id == 0  && empty($_GET['ctag_id'])) { ?>
                <?= $this->render('@frontend/views/blocks/search', [
                    'class' => 'module',
                    'catalog' => $model->id,
                    'parent_id' => $model->parent_id
                ]) ?>
            <?php } else { ?>
                <div class="catalog__open">
                    <div class="catalog__openInner">
                        <div data-fancybox="catalogOpen" data-src="#catalogOpen" data-type="inline" class="catalog__openTrigger">
                            <span class="catalog__openTriggerMarker"></span>
                            <span class="catalog__openTriggerText">Меню категорий образов</span>
                        </div>
                        <div class="catalog__openPicked">
                            <span class="catalog__openPickedLeft">Выбранная категория: </span>
                            <span class="catalog__openPickedRight">
                                <?=$model->name?>
                                <?=(!empty($_GET['ctag_id']))? '/ '.Ctag::findOne($_GET['ctag_id'])->name : ''?>
                            </span>
                        </div>
                        <?= $this->render('@frontend/views/blocks/search', ['class' => 'catalog__openSearch']) ?>
                    </div>
                </div>
            <?php } ?>
            <?php if (empty($_GET['ctag_id'])) { ?>
                <div class="catalogCategories">
                    <ul class="catalogCategories__inner">
                        <?php foreach($categories as $category) {
                            echo $this->render('@frontend/views/catalog/_item', ['category' => $category]);
                        }

                        if (count($categories) % 4 == 3) {
                            echo '<li class="hide"></li>';
                        } else if(count($categories) % 4 == 2) {
                            echo '<li class="hide"></li>';
                            echo '<li class="hide"></li>';
                        } else if(count($categories) % 4 == 1) {
                            echo '<li class="hide"></li>';
                            echo '<li class="hide"></li>';
                            echo '<li class="hide"></li>';
                        }
                        ?>
                    </ul>
                </div>
            <?php } ?>
            <div class="catalog__inner">
                <?php
                foreach($products as $product) { ?>
                    <?= $this->render('@frontend/views/product/_productItem', ['product' => $product]) ?>
                <?php }
                if (count($products) % 5 == 2) {
                    echo '<div class="catalog__item"></div>';
                    echo '<div class="catalog__item"></div>';
                    echo '<div class="catalog__item"></div>';
                } else if (count($products) % 5 == 3) {
                    echo '<div class="catalog__item"></div>';
                    echo '<div class="catalog__item"></div>';
                } else if (count($products) % 5 == 4) {
                    echo '<div class="catalog__item"></div>';
                }
                ?>
            </div>
            <ul class="pagination">
                <?php
                    $currentPage = (isset($_GET['page']))? $_GET['page'] : 1;

                    foreach($pages as $key => $page) {
                        $requestUri = $_SERVER['REQUEST_URI'];

                        if (strpos($requestUri, 'page=')) {
                            $url = preg_replace('#(.*page)=[0-9]+(.*)#siU', '$1='.$page.'$2', $requestUri);
                            $urlPrev = preg_replace('#(.*page)=[0-9]+(.*)#siU', '$1='.($currentPage-1).'$2', $requestUri);
                            $urlNext = preg_replace('#(.*page)=[0-9]+(.*)#siU', '$1='.($currentPage+1).'$2', $requestUri);
                        } else {
                            if (strpos($requestUri, '?')) {
                                $url = $requestUri.'&page='.$page;
                                $urlPrev = $requestUri.'&page='.($currentPage-1);
                                $urlNext = $requestUri.'&page='.($currentPage+1);
                            } else {
                                $url = $requestUri.'?page='.$page;
                                $urlPrev = $requestUri.'?page='.($currentPage-1);
                                $urlNext = $requestUri.'?page='.($currentPage+1);
                            }
                        }

                        if ($key == 0 && $currentPage - 1 != 0) {
                            echo '<li class="prev"><a href="'.$urlPrev.'"></a></li>';
                        }

                        if ($currentPage == $page) {
                            echo '<li><span>'.$page.'</span></li>';
                        } else {
                            echo '<li><a href="'.$url.'">'.$page.'</a></li>';
                        }

                        if (count($pages) == $key + 1 && $currentPage != count($pages)) {
                            echo '<li class="next"><a href="'.$urlNext.'"></a></li>';
                        }
                    }
                ?>
            </ul>
        </div>
    </div>

<?= $this->render('@frontend/views/blocks/requestForm') ?>

<?= $this->render('@frontend/views/blocks/reviews') ?>

<?= $this->render('@frontend/views/blocks/advantages', ['class' => 'advantages_additional advantages_bottom']) ?>

<?= $this->render('@frontend/views/blocks/requestForm') ?>