<?php
use common\models\Ctag;
use yii\helpers\Url;

$this->params['parent'] = $topCategory;
$this->params['pagetitle'] = $model->name;
$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
?>

    <div class="catalogItem">
        <div class="container">
            <div class="catalogItem__inner">
                <div class="catalogItem__left">
                    <div class="catalogItem__name"><?=$model->name?></div>
                    <div class="catalogItem__artikul">Артикул №<?=$model->artikul?></div>
                    <a href="<?=$model->image?>" data-fancybox class="catalogItem__image"><img src="<?=$model->image?>" alt=""></a>
                    <div class="catalogItem__tags">
                        <div class="catalogItem__tagsHeader">Теги:</div>
                        <ul class="catalogItem__tagsItems">
                            <?php foreach($model->ctags as $ctag) { ?>
                                <li class="catalogItem__tagsItem">
                                    <a href="<?=Url::to(['catalog/index', 'alias' => $topCategory['alias']])?>?ctag_id=<?=$ctag->id?>"
                                       class="catalogItem__tagsItem"><?=$ctag->name?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="catalogItem__hr"></div>
                    <div class="catalogItem__info">
                        <div class="catalogItem__infoItem"><a href="#">Узнать о стоимости и сроках доставки</a></div>
                        <div class="catalogItem__infoContent content"><?=$cost?></div>
                        <div class="catalogItem__infoItem"><a href="#">Посмотреть способы оплаты</a></div>
                        <div class="catalogItem__infoContent content"><?=$delivery?></div>
                        <div class="catalogItem__infoItem"><a href="#">Характеристики холста и чернил</a></div>
                        <div class="catalogItem__infoContent content"><?=$canvas?></div>
                    </div>
                </div>
                <div class="catalogItem__right">
                    <?php if(strpos($model->type, 'одульная')) { ?>
                        <form class="calc sendForm">
                            <div class="calc__item">
                                <div class="calc__header">Введите высоту и ширину Вашей картины: </div>
                                <div class="calc__fieldset">
                                    <label class="calc__inputWrapper">
                                        <span>Высота:</span>
                                        <input type="text" name="height" class="calc__input">
                                        <span>см.</span>
                                    </label>
                                    <label class="calc__inputWrapper">
                                        <span>Ширина:</span>
                                        <input type="text" name="width" class="calc__input">
                                        <span>см.</span>
                                    </label>
                                </div>
                            </div>
                            <div class="calc__item">
                                <div class="calc__header">Выберите материал: </div>
                                <div class="calc__fieldset">
                                    <label class="radioWithImage">
                                    <span class="radioWithImage__image">
                                        <img src="/resources/img/glanc.png" alt="">
                                    </span>
                                        <input type="radio" name="material" value="Глянцевое стекло" checked>
                                    <span class="radioWithImage__inner">
                                        <span class="radioWithImage__marker"></span>
                                        <span class="radioWithImage__text">Глянцевое стекло</span>
                                    </span>
                                    </label>
                                    <label class="radioWithImage">
                                    <span class="radioWithImage__image">
                                        <img src="/resources/img/natur.png" alt="">
                                    </span>
                                        <input type="radio" name="material" value="Натуральный холст">
                                    <span class="radioWithImage__inner">
                                        <span class="radioWithImage__marker"></span>
                                        <span class="radioWithImage__text">Натуральный холст</span>
                                    </span>
                                    </label>
                                </div>
                            </div>
                            <div class="calc__item">
                                <div class="calc__header">Покрыть картину объемным глянцевым гелем?</div>
                                <label class="radioWithImage">
                            <span class="radioWithImage__image">
                                <img src="/resources/img/glancGel.png" alt="">
                            </span>
                                    <input type="checkbox" name="glgel" value="да">
                            <span class="radioWithImage__inner">
                                <span class="radioWithImage__marker"></span>
                                <span class="radioWithImage__text">+ 50 % от рассчитанной стоимости картины</span>
                            </span>
                                </label>
                            </div>
                            <div class="calc__item">
                                <div class="calc__header">Дополнительные услуги:</div>
                                <label class="radio" style="margin-bottom: 7px;">
                                    <input type="checkbox" name="montazhPicture" value="Да">
                            <span class="radio__inner">
                                <span class="radio__marker"></span>
                                <span>Монтаж картины</span>
                            </span>
                                </label>
                                <label class="radio">
                                    <input type="checkbox" name="designerCall" value="Да">
                            <span class="radio__inner">
                                <span class="radio__marker"></span>
                                <span>Вызов дизайнера</span>
                            </span>
                                </label>
                            </div>
                            <div class="calc__item">
                                <div class="calc__header calc__submitHeader">Срок изготовления вашей картины: <span class="calc__days">2</span> дня</div>
                                <div class="form-group">
                                    <input type="text" name="name" placeholder="Имя" class="input">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="phone" placeholder="Телефон" class="input">
                                </div>
                                <button class="button1 calc__submit" type="submit">Сделать расчет</button>
                            </div>
                            <input type="hidden" name="type" value="Рассчет срока изготовления картины <?=$model->name?>">
                            <input type="hidden" name="url" value="<?=$_SERVER['HTTP_HOST']?><?=$_SERVER['REQUEST_URI']?>">
                            <input type="text" name="BC" class="BC">
                        </form>
                    <?php } else { ?>
                        <form class="calc sendForm">
                            <div class="calc__item">
                                <div class="calc__header">Введите высоту и ширину Вашей картины: </div>
                                <div class="calc__fieldset">
                                    <label class="calc__inputWrapper">
                                        <span>Высота:</span>
                                        <input type="text" name="height" class="calc__input">
                                        <span>см.</span>
                                    </label>
                                    <label class="calc__inputWrapper">
                                        <span>Ширина:</span>
                                        <input type="text" name="width" class="calc__input">
                                        <span>см.</span>
                                    </label>
                                </div>
                            </div>
                            <div class="calc__item">
                                <div class="calc__header">Выберите материал: </div>
                                <div class="calc__fieldset">
                                    <label class="radioWithImage">
                                    <span class="radioWithImage__image">
                                        <img src="/resources/img/natur.png" alt="">
                                    </span>
                                        <input type="radio" name="material" value="Натуральный холст" checked>
                                    <span class="radioWithImage__inner">
                                        <span class="radioWithImage__marker"></span>
                                        <span class="radioWithImage__text">Натуральный холст</span>
                                    </span>
                                    </label>
                                </div>
                            </div>
                            <div class="calc__item">
                                <div class="calc__header">Выберите Технику:</div>
                                <label class="radioWithImage">
                                    <span class="radioWithImage__image">
                                        <img src="/resources/img/masl.png" alt="">
                                    </span>
                                    <input type="radio" name="technique" value="Масляные краски" checked>
                                    <span class="radioWithImage__inner">
                                        <span class="radioWithImage__marker"></span>
                                        <span class="radioWithImage__text">Масляные краски</span>
                                    </span>
                                </label>
                                <label class="radioWithImage">
                                    <span class="radioWithImage__image">
                                        <img src="/resources/img/akril.png" alt="">
                                    </span>
                                    <input type="radio" name="technique" value="Акрил">
                                    <span class="radioWithImage__inner">
                                        <span class="radioWithImage__marker"></span>
                                        <span class="radioWithImage__text">Акрил</span>
                                    </span>
                                </label>
                            </div>
                            <div class="calc__item">
                                <div class="calc__header">Дополнительные услуги:</div>
                                <label class="radio" style="margin-bottom: 7px;">
                                    <input type="checkbox" name="montazhPicture" value="Да">
                            <span class="radio__inner">
                                <span class="radio__marker"></span>
                                <span>Монтаж картины</span>
                            </span>
                                </label>
                                <label class="radio">
                                    <input type="checkbox" name="designerCall" value="Да">
                            <span class="radio__inner">
                                <span class="radio__marker"></span>
                                <span>Вызов дизайнера</span>
                            </span>
                                </label>
                            </div>
                            <div class="calc__item">
                                <div class="calc__header calc__submitHeader">Срок изготовления вашей картины: <span class="calc__days">2</span> дня</div>
                                <div class="form-group">
                                    <input type="text" name="name" placeholder="Имя" class="input">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="phone" placeholder="Телефон" class="input">
                                </div>
                                <button class="button1 calc__submit" type="submit">Сделать расчет</button>
                            </div>
                            <input type="hidden" name="period" value="2 дня">
                            <input type="hidden" name="type" value="Рассчет срока изготовления картины <?=$model->name?>">
                            <input type="hidden" name="url" value="<?=$_SERVER['HTTP_HOST']?><?=$_SERVER['REQUEST_URI']?>">
                            <input type="text" name="BC" class="BC">
                        </form>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>



<?= $this->render('@frontend/views/blocks/requestForm') ?>

<?= $this->render('@frontend/views/blocks/reviews') ?>

<?= $this->render('@frontend/views/blocks/advantages', ['class' => 'advantages_additional advantages_bottom']) ?>

<?= $this->render('@frontend/views/blocks/requestForm') ?>