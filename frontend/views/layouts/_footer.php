<?php
use common\models\Akcia;
use common\models\Main;
use common\models\Textpage;
use frontend\models\CallbackForm;
use frontend\models\PhotoForm;
use frontend\models\ProschetForm;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Menu;

$main = Main::findOne(1);
?>
<div class="footer">
    <div class="container">
        <div class="footer__inner">
            <!--<div class="footer__item">
                <?=Menu::widget([
                    'options' => ['class' => 'menu3'],
                    'items' => \common\models\Catalog::getMenu(),
                    'linkTemplate' => '<a href="{url}" class="menu3__link"><span>{label}</span></a>'
                ]);?>
            </div>-->
            <div class="footer__item" style="position: relative;">
                <a href="/" class="footer__logo"></a>
            </div>
            <div class="footer__item">
                <?=Menu::widget([
                    'options' => ['class' => 'menu2'],
                    'items' => \common\models\Pcategory::getMenu(),
                    'linkTemplate' => '<a href="{url}" class="menu2__link">{label}</a>'
                ]);?>
            </div>
            <div class="footer__item">
                <?=Menu::widget([
                    'options' => ['class' => 'menu1 menu1_footer'],
                    'items' => \common\models\Textpage::getMenu(),
                    'linkTemplate' => '<a href="{url}" class="menu1__link">{label}</a>'
                ]);?>
            </div>
            <div class="footer__item">
                <div class="quickOrder">
                    <div class="quickOrder__header">Быстрый заказ:</div>
                    <div class="quickOrder__text">Отправляйте фото и уже через
                        4 часа вы получите возможные варианты будущей работы</div>
                    <a href="#photo" data-fancybox="forms" class="button2 button2_footer" style="margin-bottom: 5px;">Загрузить фото</a>
                    <a href="<?=Url::to(['textpage/index', 'alias' => Akcia::findOne(1)->alias])?>" class="button1 button1_padding button1_footer raschetFooter">Сделать расчет</a>
                </div>
            </div>
            <div class="footer__item">
                <a href="tel:+380800505251" class="phone phone_footer">
                    <span>0 800 505 251</span>
                    Звонок бесплатный
                </a>
                <br>
                <div class="time time_footer">
                    пн-сб 09:00–21:00
                    <span>вс - выходной</span>
                </div>
                <br>
                <a href="<?=Url::to(['textpage/index', 'alias' => Textpage::findOne(5)->alias])?>" class="address address_footer">
                    <span>г. Киев, переулок<br>Куреневский 15</span>
                </a>
            </div>
        </div>
    </div>
    <div class="footer__line"></div>
    <div class="footer__bottom">© 2012 -2017 «Denir». Все права на любые материалы, опубликованные на сайте, защищены в соответствии с Украинским<br>
        и международным законодательством об авторском праве и смежных правах.</div>
</div>
<div id="work" class="work">

</div>
<div id="product" class="productPopup">
    <img src="#" alt="" class="productPopup__image">
    <div class="productPopup__linkWrapper"><a href="#" class="productPopup__link">Узнать цену</a></div>
</div>
<div id="success" class="popup">
    <div class="success__image"></div>
    <p class="text-center" style="margin: 0 0 5px;font-size: 30px;">Спасибо за ваш заказ!</p>
    <p class="text-center" style="font-size: 18px;">Менеджеры компании свяжутся с вами<br>
        в самое ближайшее время.</p>
</div>
<?php
$callbackForm = new CallbackForm();
$form = ActiveForm::begin([
    'options' => [
        'class' => 'popup sendForm',
        'id' => 'callback'
    ],
]);?>
    <div class="popup__header">Заказать звонок</div>
    <?=$form->field($callbackForm, 'name')
        ->textInput([
            'class' => 'input',
            'placeholder' => 'Имя'
        ])->label(false)?>
    <?=$form->field($callbackForm, 'phone')
        ->textInput([
            'class' => 'input',
            'placeholder' => 'Телефон'
        ])->label(false)?>
    <?=$form->field($callbackForm, 'wishes')
        ->textarea([
            'class' => 'input',
            'placeholder' => 'Пожелания'
        ])->label(false)?>

    <button type="submit" class="button3">Заказать</button>
    <div class="loading">

    <?=$form->field($callbackForm, 'type')
        ->hiddenInput([
            'value' => 'Заказать звонок с сайта denir'
        ])->label(false)?>

    <?=$form->field($callbackForm, 'url')
        ->hiddenInput([
            'value' => $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']
        ])->label(false)?>

    <?=$form->field($callbackForm, 'BC')
        ->hiddenInput([
            'value' => ''
        ])->label(false)?>
<?php ActiveForm::end();?>
<?php
$photoForm = new PhotoForm();
$form = ActiveForm::begin([
    'options' => [
        'class' => 'popup sendForm',
        'id' => 'photo'
    ],
]);?>
<div class="popup__header">Загрузить фото</div>
<?=$form->field($photoForm, 'file[]', [
    'template' => "{input}\n{label}\n{hint}\n\n{error}"
])->fileInput(['multiple' => true, 'accept' => 'image/*'])?>
<?=$form->field($photoForm, 'name')
    ->textInput([
        'class' => 'input',
        'placeholder' => 'Имя'
    ])->label(false)?>
<?=$form->field($photoForm, 'phone')
    ->textInput([
        'class' => 'input',
        'placeholder' => 'Телефон'
    ])->label(false)?>
<?=$form->field($photoForm, 'wishes')
    ->textarea([
        'class' => 'input',
        'placeholder' => 'Пожелания'
    ])->label(false)?>

<button type="submit" class="button3">Отправить</button>
        <div class="loading"></div>

<?=$form->field($photoForm, 'type')
    ->hiddenInput([
        'value' => 'Загрузить фото с сайта denir'
    ])->label(false)?>

<?=$form->field($photoForm, 'zakazN')
    ->hiddenInput([
        'value' => ''
    ])->label(false)?>

<?=$form->field($photoForm, 'url')
    ->hiddenInput([
        'value' => $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']
    ])->label(false)?>

<?=$form->field($photoForm, 'BC')
    ->hiddenInput([
        'value' => ''
    ])->label(false)?>
<?php ActiveForm::end();?>

<?php
$proschetForm = new ProschetForm();
$form = ActiveForm::begin([
    'options' => [
        'class' => 'popup sendForm',
        'id' => 'raschet'
    ],
]);?>
<div class="popup__header">Сделать расчет</div>
<?=$form->field($proschetForm, 'name')
    ->textInput([
        'class' => 'input',
        'placeholder' => 'Имя'
    ])->label(false)?>
<?=$form->field($proschetForm, 'phone')
    ->textInput([
        'class' => 'input',
        'placeholder' => 'Телефон'
    ])->label(false)?>
<?=$form->field($proschetForm, 'wishes')
    ->textarea([
        'class' => 'input',
        'placeholder' => 'Пожелания'
    ])->label(false)?>
<?=$form->field($proschetForm, 'file[]', [
    'template' => "{input}\n{label}\n{hint}\n\n{error}"
])->fileInput(['multiple' => true])?>

<button type="submit" class="button3">Отправить</button>
        <div class="loading"></div>

<?=$form->field($proschetForm, 'type')
    ->hiddenInput([
        'value' => 'Сделать расчет с сайта denir'
    ])->label(false)?>

<?=$form->field($proschetForm, 'url')
    ->hiddenInput([
        'value' => $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']
    ])->label(false)?>

<?=$form->field($proschetForm, 'BC')
    ->hiddenInput([
        'value' => ''
    ])->label(false)?>
<?php ActiveForm::end();?>
<?php if (isset($categories)) { ?>
<div id="catalogOpen" class="catalogOpen">
    <div class="catalogCategories">
        <ul class="catalogCategories__inner">
            <?php foreach($categories as $category) {
                echo $this->render('@frontend/views/catalog/_item', ['category' => $category]);
            }

            if (count($categories) % 4 == 2) {
                echo '<li class="hide"></li>';
                echo '<li class="hide"></li>';
            } else if(count($categories) % 4 == 1) {
                echo '<li class="hide"></li>';
            }
            ?>
        </ul>
    </div>
</div>
<?php } ?>
<form class="bagetPopup" id="bagetPopup">
    <div class="header">Предварительный расчет стоимости</div>
    <div class="bagetPopup__inner">
        <div class="bagetPopup__left">
            <img src="" alt="">
        </div>
        <div class="bagetPopup__right">
            <div class="bagetPopup__rightInner">

            </div>
            <hr>
            <label class="bagetPopup__label">
                <span>Высота вашей картины:</span>
                <input type="text" name="bagetHeight">
                <span>см.</span>
            </label>
            <label class="bagetPopup__label">
                <span>Ширина вашей картины:</span>
                <input type="text" name="bagetWidth">
                <span>см.</span>
            </label>
            <hr>
            <div class="bagetPopup__bottom">
                <label class="bagetPopup__label">
                    <span>Итого:</span>
                    <input type="text" name="bagetResult">
                    <span>грн.</span>
                </label>
                <button type="submit" class="button1 bagetPopup__submit">Оформить заказ</button>
            </div>
        </div>
    </div>
</form>
