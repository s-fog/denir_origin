<?php
use common\models\Akcia;
use common\models\Main;
use common\models\Textpage;
use yii\helpers\Url;
use yii\widgets\Menu;

if (!isset($class)) {
    $class = '';
}

$main = Main::findOne(1);
?>
<div class="mainHeader <?=$class?>">
    <div class="container">
        <div class="mainHeader__inner">
            <a href="/" class="mainHeader__logo"></a>
            <div class="mainHeader__left">
                <?=Menu::widget([
                    'options' => ['class' => 'menu1'],
                    'items' => \common\models\Textpage::getMenu(),
                    'linkTemplate' => '<a href="{url}" class="menu1__link">{label}</a>'
                ]);?>
                <?=Menu::widget([
                    'options' => ['class' => 'menu2'],
                    'items' => \common\models\Pcategory::getMenu(),
                    'linkTemplate' => '<a href="{url}" class="menu2__link">{label}</a>'
                ]);?>
            </div>
            <div class="mainHeader__right">
                <a href="tel:+3<?=$main->phone?>" class="mainHeader__rightItem phone">
                    <span><?=$main->phone?></span>
                    Звонок бесплатный
                </a>
                <a href="<?=Url::to(['textpage/index', 'alias' => Textpage::findOne(5)->alias])?>" class="mainHeader__rightItem address" style="flex: 0 0 34%; width: 34%;">
                    <span><?=$main->address?></span>
                </a>
                <div class="mainHeader__rightItem time"><?=$main->work_time?></div>
                <div class="mainHeader__rightItem">
                    <a href="#callback" data-fancybox class="button1" style="width: 100%;">Заказать звонок</a>
                </div>
            </div>
        </div>
        <div style="height: 41px; margin-top: 15px;"></div>
        <!--<div class="menu3__wrapper">
            <?/*=Menu::widget([
                'options' => ['class' => 'menu3'],
                'items' => \common\models\Catalog::getMenu(),
                'linkTemplate' => '<a href="{url}" class="menu3__link"><span>{label}</span></a>'
            ]);*/?>
            <a href="<?=Url::to(['textpage/index', 'alias' => Akcia::findOne(1)->alias])?>" class="menu3__akcia">Акция</a>
        </div>-->
    </div>
</div>