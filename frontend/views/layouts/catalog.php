<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>

<?= $this->render('_head') ?>

<?php $this->beginBody() ?>
<div class="bgbg">
    <?= $this->render('_header', ['class' => 'mainHeader_header3']) ?>
    
    <div class="breadcrumbs">
        <div class="container">
            <ul class="breadcrumbs__inner">
                <li class="breadcrumbs__item"><a href="/">Главная</a></li>
                <?=(isset($this->params['parent']))? '
                    <li class="breadcrumbs__item">
                        <a href="'.Url::to(['catalog/index', 'alias' => $this->params['parent']['alias']]).'">'.$this->params['parent']['name'].'</a>
                    </li>' : ''?>
                <li class="breadcrumbs__item"><span><?=$this->params['pagetitle']?></span></li>
            </ul>
        </div>
    </div>
    <h1 class="header"><span><?=(isset($this->params['seoh1']) && !empty($this->params['seoh1']))? $this->params['seoh1'] : $this->params['pagetitle']?></span></h1>
    
    <?=$content?>
    
    <?php
    if (isset($this->params['categories']) && !empty($this->params['categories'])) {
        echo $this->render('_footer', ['categories' => $this->params['categories']]);
    } else {
        echo $this->render('_footer');
    }
    ?>
</div>
<?php $this->endBody() ?>
<?= $this->render('_jivosite') ?>
</body>
</html>
<?php $this->endPage() ?>
