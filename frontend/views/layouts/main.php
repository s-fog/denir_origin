<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>

<?= $this->render('_head') ?>

<?php $this->beginBody() ?>
<div class="bgbg">
    <?= $this->render('_header') ?>

    <?=$content?>

    <?= $this->render('_footer') ?>
</div>
<?php $this->endBody() ?>
<?= $this->render('_jivosite') ?>
</body>
</html>
<?php $this->endPage() ?>
