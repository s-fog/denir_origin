<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>

<?= $this->render('_head') ?>

<?php $this->beginBody() ?>
<div class="bgbg">
    <?= $this->render('_header') ?>

    <div class="breadcrumbs">
        <div class="container">
            <ul class="breadcrumbs__inner">
                <li class="breadcrumbs__item"><a href="/">Главная</a></li>
                <li class="breadcrumbs__item"><span><?=$this->params['pagetitle']?></span></li>
            </ul>
        </div>
    </div>
    <h1 style="display: none;"><span><?=(isset($this->params['seoh1']) && !empty($this->params['seoh1']))? $this->params['seoh1'] : $this->params['pagetitle']?></span></h1>

    <?=$content?>

    <?= $this->render('_footer') ?>
</div>
<?php $this->endBody() ?>
<?= $this->render('_jivosite') ?>
</body>
</html>
<?php $this->endPage() ?>
