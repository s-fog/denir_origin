<div data-fancybox="works" 
     data-id="<?=$portfolio->id?>" 
     data-src="#work" 
     data-type="inline" 
     data-group="group1" 
     class="works__item worksOpen">
    <div class="works__itemImage" style="background-image: url(<?=$portfolio->previewimage?>)">
        <span></span>
    </div>
    <div class="works__itemInfo">
        <table>
            <tr>
                <td>Заказ №<?=$portfolio->id?></td>
                <td class="black">Материал: <?=$portfolio->material?></td>
            </tr>
            <tr>
                <td>Дата: <?=date('d.m.y', $portfolio->creationdate)?></td>
                <td class="black">Размер: <?=$portfolio->size?></td>
            </tr>
        </table>
    </div>
</div>