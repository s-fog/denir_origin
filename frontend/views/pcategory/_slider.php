<?php if(empty($portfolioslider)) { ?>
    <div class="slider owl-carousel">
        <?php foreach($model->pslider as $slide) { ?>
            <?=$this->render('@frontend/views/sliders/_pslider', ['slide' => $slide])?>
        <?php } ?>
    </div>
<?php } else { ?>
    <div class="slider slider_additional owl-carousel">
        <?php foreach($portfolioslider as $slide) { ?>
            <?=$this->render('@frontend/views/sliders/_porfolioSlider.php', ['slide' => $slide])?>
        <?php } ?>
    </div>
<?php } ?>