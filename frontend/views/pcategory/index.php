<?php
use common\models\Pcategory;
use yii\helpers\Url;
$this->params['pagetitle'] = $model->name;
$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
?>

<?= $this->render('_slider', ['model' => $model, 'portfolioslider' => $portfolioslider]) ?>

<div class="categories">
    <div class="container">
        <div class="categories__inner">
            <?php foreach(Pcategory::getCategories($model->id) as $category) { ?>
                <div class="categories__item categories__item_inner">
                    <a href="<?=Url::to(['pcategory/index', 'alias' => $category->alias]);?>" class="categories__image"
                    style="background-image: url(<?=$category->image?>);"></a>
                    <div class="categories__itemInfo">
                        <a href="<?=Url::to(['pcategory/index', 'alias' => $category->alias]);?>" class="categories__header"><span><?=$category->name?></span></a>
                        <div class="categories__text"><?=$category->description?></div>
                    </div>
                </div>
            <?php }?>
        </div>
    </div>
</div>

<?php
    if ($model->id == 8) {
        echo $this->render('@frontend/views/blocks/schema');
    }
?>

<?= $this->render('@frontend/views/blocks/requestForm') ?>

<?= $this->render('@frontend/views/blocks/seoBlock', ['model' => $model]) ?>

    <br>

<?= $this->render('@frontend/views/blocks/lastWorks', ['model' => $model]) ?>

<?= $this->render('@frontend/views/blocks/advantages', ['class' => '']) ?>

<?= $this->render('@frontend/views/blocks/reviews', ['pcategory_id' => $model->id]) ?>

<?= $this->render('@frontend/views/blocks/requestForm', ['id' => 'requestForm2']) ?>