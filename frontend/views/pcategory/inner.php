<?php
use common\models\Pcategory;
use yii\helpers\Url;
$this->params['pagetitle'] = $model->name;
$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
$this->params['parent'] = Pcategory::find()->where(['id' => $model->parent_id])->asArray()->one();
?>

<?= $this->render('_slider', ['model' => $model, 'portfolioslider' => $portfolioslider]) ?>

<?= $this->render('@frontend/views/blocks/seoBlock', ['model' => $model]) ?>

<br>

<?= $this->render('@frontend/views/blocks/lastWorks', ['model' => $model]) ?>

<?php
    if ($model->id == 8) {
        echo $this->render('@frontend/views/blocks/schema');
    }
?>

<?= $this->render('@frontend/views/blocks/requestForm') ?>

<?= $this->render('@frontend/views/blocks/reviews', ['pcategory_id' => $model->id]) ?>

<?= $this->render('@frontend/views/blocks/advantages', ['class' => 'advantages_additional advantages_bottom']) ?>

<?= $this->render('@frontend/views/blocks/requestForm') ?>