<?php
use common\models\Pcategory;
use yii\helpers\Url;
$this->params['pagetitle'] = $model->name;
$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
$this->params['parent'] = Pcategory::find()->where(['id' => $model->parent_id])->asArray()->one();
$forHim = Pcategory::findOne(11);
$forHer = Pcategory::findOne(12);
$forHolidays = Pcategory::findOne(13);
?>

    <div class="present"
         style="background-image: url(<?=($model->id == 11)? '/resources/img/prBg.png' : ''?><?=($model->id == 12)? '/resources/img/gbg.jpg' : ''?><?=($model->id == 13)? '/resources/img/prBg.png' : ''?>);">
        <div class="container">
            <div class="present__inner">
                <div class="present__left">
                    <div class="present__header"><span>Картина в подарок</span></div>
                    <div class="present__text"><?=$model->description?></div>
                    <div class="present__buttons">
                        <a
                            href="<?=Url::to(['pcategory/index', 'alias' => $forHim->alias])?>"
                            class="button2 button2_male<?=($forHim->alias == $model->alias)? ' button2_active' : ''?>">
                            <?=$forHim->name?>
                        </a>
                        <a
                            href="<?=Url::to(['pcategory/index', 'alias' => $forHer->alias])?>"
                           class="button2 button2_female<?=($forHer->alias == $model->alias)? ' button2_active' : ''?>">
                            <?=$forHer->name?>
                        </a>
                        <a
                            href="<?=Url::to(['pcategory/index', 'alias' => $forHolidays->alias])?>"
                            class="button2 button2_holiday<?=($forHolidays->alias == $model->alias)? ' button2_active' : ''?>" style="margin-top: 10px;">
                            <?=$forHolidays->name?>
                        </a>
                    </div>
                </div>
                <div class="present__middle<?=($model->id == 13)? ' therecolumn' : ''?>">
                    <ul class="present__list">
                        <?php foreach($model->ptags as $ptag) { ?>
                            <li class="present__listItem"><a href="?ptag_id=<?=$ptag->id?>" class="present__listLink"><?=$ptag->name?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <?php if ($model->id != 13) { ?>
                    <div class="present__right">
                        <div class="present__slider owl-carousel">
                            <?php foreach($model->getBehavior('galleryBehavior')->getImages() as $image) {
                                $imgSmall = str_replace(array(
                                    'D:\htdocs\denir-origin/www',
                                    '/home/anton438/denir.com.ua/www',
                                    '/home/u474194/denir.com.ua/www/www'
                                ), array('',''), $image->getUrl('small'));
                                $imgOriginal = str_replace(array(
                                    'D:\htdocs\denir-origin/www',
                                    '/home/anton438/denir.com.ua/www',
                                    '/home/u474194/denir.com.ua/www/www'
                                ), array('',''), $image->getUrl('original'));
                                ?>
                                <a href="<?=$imgOriginal?>" class="present__sliderItem" data-fancybox="present__slider">
                                    <img src="<?=$imgSmall?>" alt="">
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

<?= $this->render('@frontend/views/blocks/lastWorks', ['model' => $model]) ?>

<?= $this->render('@frontend/views/blocks/advantages', ['class' => '']) ?>

<?= $this->render('@frontend/views/blocks/reviews', ['pcategory_id' => $model->id]) ?>

<?= $this->render('@frontend/views/blocks/requestForm') ?>