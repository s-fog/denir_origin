<?php use common\models\Akcia;
use common\models\Personal;
use common\models\Textpage;
use yii\helpers\Url;

$inter = ' style="display: none;"';
if ((!empty($portfolio->interierimage))) {
    $inter = '';
}
if ((!empty($portfolio->interierimage2))) {
    $inter = '';
}
if ((!empty($portfolio->interierimage3))) {
    $inter = '';
}
if ((!empty($portfolio->interierimage4))) {
    $inter = '';
}
if ((!empty($portfolio->interierimage5))) {
    $inter = '';
}
if ((!empty($portfolio->interierimage6))) {
    $inter = '';
}

if (isset($review) && !empty($review)) { ?>
    <div class="work__inner" data-id="<?=$portfolio->id?>">
        <div class="work__buttonLeft"></div>
        <div class="work__buttonRight"></div>
        <div class="work__left work__left_main"
             style="background-image: url(<?=$portfolio->borderbg?>);<?=(empty($portfolio->interierimage))? ' padding-top: 82px' : '' ?>">
            <div class="work__toInterier"<?=$inter?>>
                <span>Картина в интерьере</span>
            </div>
            <a href="<?=$portfolio->bigimage?>"
               class="work__image
               <?=(empty($portfolio->interierimage))? ' center' : '' ?>
               <?=($portfolio->type == 'Модульная' || $portfolio->type == 'Роспись стен') ? ' work__image_noshadow' : ''?>
               "
               data-fancybox="workImage">
                <div class="work__imageInner">
                    <img src="<?=(empty($portfolio->borderimage))? $portfolio->bigimage : $portfolio->borderimage?>"
                         alt="">
                    <div class="work__imageLoop"></div>
                </div>
            </a>
        </div>
        <div class="work__left work__left_interier">
            <div class="work__leftInterierSlider owl-carousel">
                <?php if (!empty($portfolio->interierimage)) { ?>
                    <div class="work__leftInterierSliderItem" style="background-image: url(<?=$portfolio->interierimage?>);"></div>
                <?php } ?>
                <?php if (!empty($portfolio->interierimage2)) { ?>
                    <div class="work__leftInterierSliderItem" style="background-image: url(<?=$portfolio->interierimage2?>);"></div>
                <?php } ?>
                <?php if (!empty($portfolio->interierimage3)) { ?>
                    <div class="work__leftInterierSliderItem" style="background-image: url(<?=$portfolio->interierimage3?>);"></div>
                <?php } ?>
                <?php if (!empty($portfolio->interierimage4)) { ?>
                    <div class="work__leftInterierSliderItem" style="background-image: url(<?=$portfolio->interierimage4?>);"></div>
                <?php } ?>
                <?php if (!empty($portfolio->interierimage5)) { ?>
                    <div class="work__leftInterierSliderItem" style="background-image: url(<?=$portfolio->interierimage5?>);"></div>
                <?php } ?>
                <?php if (!empty($portfolio->interierimage6)) { ?>
                    <div class="work__leftInterierSliderItem" style="background-image: url(<?=$portfolio->interierimage6?>);"></div>
                <?php } ?>
            </div>
        </div>
        <div class="work__left work__left_review">
            <div class="work__reviewSlider">
                <?php foreach($review->getBehavior('galleryBehavior')->getImages() as $image):
                    $imgOriginal = str_replace(array(
                        'D:\htdocs\denir-origin/www',
                        '/home/anton438/denir.com.ua/www',
                        '/home/u474194/denir.com.ua/www/www'
                    ), array('',''), $image->getUrl('original')); ?>
                    <a href="<?=$imgOriginal?>"
                       class="work__sliderItem"
                       data-fancybox="workSliderItem"><img src="<?=$imgOriginal?>" alt=""></a>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="work__right">
            <div class="work__faces">
                <?php foreach($portfolio->getBehavior('galleryBehavior')->getImages() as $image):
                    $imgSmall = str_replace(array(
                        'D:\htdocs\denir-origin/www',
                        '/home/anton438/denir.com.ua/www',
                        '/home/u474194/denir.com.ua/www/www'
                    ), array('',''), $image->getUrl('small'));
                    $imgOriginal = str_replace(array(
                        'D:\htdocs\denir-origin/www',
                        '/home/anton438/denir.com.ua/www',
                        '/home/u474194/denir.com.ua/www/www'
                    ), array('',''), $image->getUrl('original')); ?>
                    <a href="<?=$imgOriginal?>"
                       data-fancybox="faces"
                       class="work__face"><img src="<?=$imgSmall?>" alt=""></a>
                <?php endforeach; ?>
            </div>
            <p>Заказ №<?=$portfolio->id?><br>Дата: <?=date('d.m.y', $portfolio->creationdate)?></p>
            <?=(!empty($portfolio->personal_id))? '
                <p>
                    Художник-дизайнер:<br>
                    <span style="text-decoration: underline;">'.Personal::findOne($portfolio->personal_id)->name.'</span>
                </p>
            ' : ''?>
            <p class="work__rightBigText">
                <?=($portfolio->material)? 'Материал: '.$portfolio->material.'' : ''?>
                <br>
                <?=($portfolio->size)? 'Размер: '.$portfolio->size.'' : ''?>
            </p>
            <p><?=$portfolio->dop?></p>
            <p class="work__rightBigText">Срок создания: <?=$portfolio->creationperiod?></p>
        </div>
    </div>
    <div class="work__showReview">отзыв клиента</div>
    <div class="work__bottom">
        <div class="work__bottomLeft">
            <div class="work__bottomLeftInner">
                <div class="work__bottomRightText"><?=$review->text?></div>
                <hr>
                <div class="work__reviewWho">
                    <div class="work__reviewWhoName"><?=$review->name?></div>
                    <div class="work__reviewWhoText">
                        <?=$review->city?>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<?=$review->type?>
                    </div>
                </div>
            </div>
        </div>
        <div class="work__bottomRight">
            <div class="work__bottomRightHeader">Хотите так же?</div>
            <div class="work__bottomRightText">Отправляйте фото и уже через
                4 часа вы получите возможные варианты будущей работы</div>
            <br>
            <br>
            <br>
            <img src="/resources/img/arr.png" alt="">
            <a href="#photo" data-fancybox="forms" class="button2" style="margin-bottom: 10px;">Загрузить фото</a>
            <a href="<?=Url::to(['textpage/index', 'alias' => Akcia::findOne(1)->alias])?>" class="button1 button1_padding">Сделать расчет</a>
            <br>
            <br>
            <a href="<?=Url::to(['textpage/index', 'alias' => Textpage::findOne(2)->alias])?>" class="work__bottomRightLink">Доставка и оплата</a>
        </div>
    </div>
<?php } else { ?>
    <div class="work__inner" data-id="<?=$portfolio->id?>">
        <div class="work__buttonLeft"></div>
        <div class="work__buttonRight"></div>
        <div class="work__left work__left_main"
             style="background-image: url(<?=$portfolio->borderbg?>);<?=(empty($portfolio->interierimage))? ' padding-top: 82px' : '' ?>">
            <div class="work__toInterier"<?=$inter?>>
                <span>Картина в интерьере</span>
            </div>
            <a href="<?=$portfolio->bigimage?>"
               class="work__image
               <?=(empty($portfolio->interierimage))? ' center' : '' ?>
               <?=($portfolio->type == 'Модульная' || $portfolio->type == 'Роспись стен') ? ' work__image_noshadow' : ''?>
               "
               data-fancybox="workImage">
                <div class="work__imageInner">
                    <img src="<?=(empty($portfolio->borderimage))? $portfolio->bigimage : $portfolio->borderimage?>"
                         alt="">
                    <div class="work__imageLoop"></div>
                </div>
            </a>
        </div>
        <div class="work__left work__left_interier">
            <div class="work__leftInterierSlider owl-carousel">
                <?php if (!empty($portfolio->interierimage)) { ?>
                    <div class="work__leftInterierSliderItem" style="background-image: url(<?=$portfolio->interierimage?>);"></div>
                <?php } ?>
                <?php if (!empty($portfolio->interierimage2)) { ?>
                    <div class="work__leftInterierSliderItem" style="background-image: url(<?=$portfolio->interierimage2?>);"></div>
                <?php } ?>
                <?php if (!empty($portfolio->interierimage3)) { ?>
                    <div class="work__leftInterierSliderItem" style="background-image: url(<?=$portfolio->interierimage3?>);"></div>
                <?php } ?>
                <?php if (!empty($portfolio->interierimage4)) { ?>
                    <div class="work__leftInterierSliderItem" style="background-image: url(<?=$portfolio->interierimage4?>);"></div>
                <?php } ?>
                <?php if (!empty($portfolio->interierimage5)) { ?>
                    <div class="work__leftInterierSliderItem" style="background-image: url(<?=$portfolio->interierimage5?>);"></div>
                <?php } ?>
                <?php if (!empty($portfolio->interierimage6)) { ?>
                    <div class="work__leftInterierSliderItem" style="background-image: url(<?=$portfolio->interierimage6?>);"></div>
                <?php } ?>
            </div>
        </div>
        <div class="work__right">
            <div class="work__faces">
                <?php foreach($portfolio->getBehavior('galleryBehavior')->getImages() as $image):
                    $imgSmall = str_replace(array(
                        'D:\htdocs\denir-origin/www',
                        '/home/anton438/denir.com.ua/www',
                        '/home/u474194/denir.com.ua/www/www'
                    ), array('',''), $image->getUrl('small'));
                    $imgOriginal = str_replace(array(
                        'D:\htdocs\denir-origin/www',
                        '/home/anton438/denir.com.ua/www',
                        '/home/u474194/denir.com.ua/www/www'
                    ), array('',''), $image->getUrl('original')); ?>
                    <a href="<?=$imgOriginal?>"
                       data-fancybox="faces"
                       class="work__face"><img src="<?=$imgSmall?>" alt=""></a>
                <?php endforeach; ?>
            </div>
            <p>Заказ №<?=$portfolio->id?><br>Дата: <?=date('d.m.y', $portfolio->creationdate)?></p>
            <?=(!empty($portfolio->personal_id))? '
                <p>
                    Художник-дизайнер:<br>
                    <span style="text-decoration: underline;">'.Personal::findOne($portfolio->personal_id)->name.'</span>
                </p>
            ' : ''?>
            <p class="work__rightBigText">
                <?=($portfolio->material)? 'Материал: '.$portfolio->material.'' : ''?>
                <br>
                <?=($portfolio->size)? 'Размер: '.$portfolio->size.'' : ''?>
            </p>
            <p><?=$portfolio->dop?></p>
            <p class="work__rightBigText">Срок создания: <?=$portfolio->creationperiod?></p>
        </div>
    </div>
    <div class="work__showReview" style="display: none;"></div>
    <div class="work__bottom">
        <div class="work__bottomLeft"></div>
        <div class="work__bottomRight">
            <div class="work__bottomRightHeader">Хотите так же?</div>
            <div class="work__bottomRightText">Отправляйте фото и уже через
                4 часа вы получите возможные варианты будущей работы</div>
            <br>
            <br>
            <br>
            <img src="/resources/img/arr.png" alt="">
            <a href="#photo" data-fancybox="forms" class="button2" style="margin-bottom: 10px;">Загрузить фото</a>
            <a href="<?=Url::to(['textpage/index', 'alias' => Akcia::findOne(1)->alias])?>" class="button1 button1_padding">Сделать расчет</a>
            <br>
            <br>
            <a href="<?=Url::to(['textpage/index', 'alias' => Textpage::findOne(2)->alias])?>" class="work__bottomRightLink">Доставка и оплата</a>
        </div>
    </div>
<?php } ?>
<button data-fancybox-close="" class="fancybox-close-small"></button>