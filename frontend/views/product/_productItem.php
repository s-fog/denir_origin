<?php
use yii\helpers\Url;
?>
<div class="catalog__item">
    <div class="catalog__itemImage"
         style="background-image: url(<?=(empty($product->previewimage))? $product->image : $product->previewimage?>);">
        <a href="<?=$product->image?>" data-fancybox="catalogItems" data-src="#product" data-type="inline" class="catalog__itemShow">Увеличить</a>
        <a href="<?=Url::to(['catalog/view', 'alias' => $product->alias]);?>" class="catalog__itemGetPrice">Узнать цену</a>
    </div>
    <div class="catalog__itemNumber">№<?=$product->artikul?></div>
    <div class="catalog__itemCat"><?=$product->name?></div>
</div>