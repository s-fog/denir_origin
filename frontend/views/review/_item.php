<?php

use yii\helpers\Html;

?>
<div class="reviews__item">
    <div class="reviews__itemImage">
        <div class="reviews__itemSlider owl-carousel">
            <?php foreach($review->getBehavior('galleryBehavior')->getImages() as $image):
                $imgMedium = str_replace(array(
                    'D:\htdocs\denir-origin/www',
                    '/home/anton438/denir.com.ua/www',
                    '/home/u474194/denir.com.ua/www/www'
                ), array('',''), $image->getUrl('medium'));
                $imgOriginal = str_replace(array(
                    'D:\htdocs\denir-origin/www',
                    '/home/anton438/denir.com.ua/www',
                    '/home/u474194/denir.com.ua/www/www'
                ), array('',''), $image->getUrl('original')); ?>
                <a href="<?=$imgOriginal?>"
                   data-fancybox="review<?=$review->id?>" style="background-image: url(<?=$imgMedium?>);"></a>
            <?php endforeach; ?>
        </div>
        <?php if ($review->portfolio_id != 0) { ?>
            <span data-fancybox="reviews"
                  data-id="<?=$review->portfolio_id?>"
                  data-src="#work"
                  data-type="inline"
                  class="worksOpen">Посмотреть работу</span>
        <?php } ?>
    </div>
    <div class="work__bottomLeftInner" style="display: block;">
        <div class="work__bottomRightText"><?=$review->introtext?></div>
        <hr>
        <div class="work__reviewWho">
            <div class="work__reviewWhoName"><?=$review->name?></div>
            <div class="work__reviewWhoText"><?=$review->city?>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<?=$review->type?></div>
        </div>
    </div>
</div>