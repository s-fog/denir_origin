<?php

/* @var $this yii\web\View */

use common\models\Akcia;
use common\models\Pcategory;
use yii\helpers\Url;

$this->params['pagetitle'] = $model->name;
$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
?>

<div class="slider owl-carousel">
    <?php foreach($model->pslider as $slide) { ?>
        <?=$this->render('@frontend/views/sliders/_pslider', ['slide' => $slide])?>
    <?php } ?>
</div>

<div class="categories">
    <div class="container">
        <div class="categories__inner">
            <?php foreach($mainCategories as $category) { ?>
                <div class="categories__item">
                    <a href="<?=Url::to(['pcategory/index', 'alias' => $category->alias]);?>" class="categories__image"
                       style="background-image: url(<?=$category->image?>);"></a>
                    <a href="<?=Url::to(['pcategory/index', 'alias' => $category->alias]);?>" class="categories__header"><span><?=$category->name?></span></a>
                    <div class="categories__text"><?=$category->description?></div>
                    <div class="slider__itemFeatures">
                        <div class="slider__itemFeaturesItem" style="background-image: url(resources/img/priceIcon2.png);">от <?=$category->price?></div>
                        <div class="slider__itemFeaturesItem" style="background-image: url(resources/img/timeIcon2.png);">от <?=$category->period?></div>
                    </div>
                    <?php if ($category->id == 6) {
                        $forHim = Pcategory::findOne(11);
                        $forHer = Pcategory::findOne(12);
                        $forHolidays = Pcategory::findOne(13);
                        ?>
                        <a href="<?=Url::to(['pcategory/index', 'alias' => $forHim->alias]);?>"
                           class="button2 button2_male"
                           style="margin-bottom: 10px;"><?=$forHim->name?></a>
                        <a href="<?=Url::to(['pcategory/index', 'alias' => $forHer->alias]);?>"
                           class="button2 button2_female"
                           style="margin-bottom: 10px;"><?=$forHer->name?></a>
                        <a href="<?=Url::to(['pcategory/index', 'alias' => $forHolidays->alias]);?>"
                           class="button2 button2_holiday"><?=$forHolidays->name?></a>
                    <?php } else { ?>
                        <a href="#photo" data-fancybox="forms" class="button2" style="margin-bottom: 10px;">
                            <?=($category->id == 5 || $category->id == 8)? 'Загрузить фото интерьера' : 'Загрузить фото'?>
                        </a>
                        <a href="<?=Url::to(['textpage/index', 'alias' => Akcia::findOne(1)->alias])?>" class="button1 button1_padding">Сделать расчет</a>
                    <?php } ?>
                </div>
            <?php }?>
        </div>
    </div>
</div>

<div class="slider slider_additional owl-carousel">
    <?php foreach($portfolioslider as $slide) { ?>
        <?=$this->render('@frontend/views/sliders/_porfolioSlider.php', ['slide' => $slide])?>
    <?php } ?>
</div>


<?= $this->render('@frontend/views/blocks/lastWorks', ['model' => 0]) ?>

<?= $this->render('@frontend/views/blocks/requestForm') ?>

<?= $this->render('@frontend/views/blocks/seoBlock', ['model' => $model]) ?>

<?= $this->render('@frontend/views/blocks/reviews') ?>

<?= $this->render('@frontend/views/blocks/advantages', ['class' => '']) ?>

    <!--<h1 class="header"><span>Каталог образов</span></h1>
    <div class="catalog">
        <div class="container">
            <div class="catalogCategories">
                <ul class="catalogCategories__inner">
                    <?php foreach($categories as $category) {
                        echo $this->render('@frontend/views/catalog/_item', ['category' => $category]);
                    }

                    if (count($categories) % 4 == 3) {
                        echo '<li class="hide"></li>';
                    } else if(count($categories) % 4 == 2) {
                        echo '<li class="hide"></li>';
                        echo '<li class="hide"></li>';
                    } else if(count($categories) % 4 == 1) {
                        echo '<li class="hide"></li>';
                        echo '<li class="hide"></li>';
                        echo '<li class="hide"></li>';
                    }
                    ?>
                </ul>
            </div>
            <div class="catalog__inner">
                <?php
                foreach($products as $product) { ?>
                    <?= $this->render('@frontend/views/product/_productItem', ['product' => $product]) ?>
                <?php }
                if (count($products) % 5 == 2) {
                    echo '<div class="catalog__item"></div>';
                    echo '<div class="catalog__item"></div>';
                    echo '<div class="catalog__item"></div>';
                } else if (count($products) % 5 == 3) {
                    echo '<div class="catalog__item"></div>';
                    echo '<div class="catalog__item"></div>';
                } else if (count($products) % 5 == 4) {
                    echo '<div class="catalog__item"></div>';
                }
                ?>
            </div>
        </div>
    </div>-->

<?= $this->render('@frontend/views/blocks/requestForm', ['id' => 'requestForm2']) ?>