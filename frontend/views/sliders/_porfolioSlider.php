<?php
use common\models\Akcia;
use common\models\Textpage;
use yii\helpers\Url;

$review = \common\models\Review::find()->where(['portfolio_id' => $slide->id])->one();
?>
<div class="slider__item" data-id="<?=$slide->id?>">
    <div class="slider__itemLeft"
         style="background-image: url(<?=(empty($slide->bordersliderbg))? $slide->borderbg : $slide->bordersliderbg?>);text-align: right;">
        <div class="slider__itemLeftInner">
            <a class="
            work__image
            <?=(empty($review->id))? ' withoutReview' : '' ?>
            <?=($slide->type == 'Модульная' || $slide->type == 'Роспись стен') ? ' work__image_noshadow' : ''?>
            "
               href="<?=$slide->bigimage?>"
               data-fancybox>
                <div class="work__imageInner">
                    <img src="<?=(empty($slide->borderimage))? $slide->bigimage : $slide->borderimage?>"
                         alt="">
                </div>
            </a>
            <?php if (!empty($review->id)) { ?>
                <div class="work__showReview worksOpen"
                     data-fancybox="works-slider"
                     data-id="<?=$slide->id?>"
                     data-src="#work"
                     data-type="inline">отзыв клиента</div>
            <?php }?>
        </div>
    </div>
    <div class="slider__itemRight" style="background-image: url(/resources/img/sll2.png);">
        <div class="slider__itemRightInner">
            <div class="slider__itemRightInnerLeft">
                <div class="work__faces">
                    <?php foreach($slide->getBehavior('galleryBehavior')->getImages() as $image):
                        $imgSmall = str_replace(array(
                            'D:\htdocs\denir-origin/www',
                            '/home/anton438/denir.com.ua/www',
                            '/home/u474194/denir.com.ua/www/www'
                        ), array('',''), $image->getUrl('small'));
                        $imgOriginal = str_replace(array(
                            'D:\htdocs\denir-origin/www',
                            '/home/anton438/denir.com.ua/www',
                            '/home/u474194/denir.com.ua/www/www'
                        ), array('',''), $image->getUrl('original')); ?>
                        <a href="<?=$imgOriginal?>" data-fancybox="faces" class="work__face"><img src="<?=$imgSmall?>" alt=""></a>
                    <?php endforeach; ?>
                </div>
                <p>Заказ №<?=$slide->id?><br>Дата: <?=date('d.m.y', $slide->creationdate)?></p>
                <p>Художник-дизайнер:<br><?=\common\models\Personal::find()->where(['id' => $slide->personal_id])->one()->name?></p>
                <p class="work__rightBigText">
                    <?=($slide->material)? 'Материал: '.$slide->material.'' : ''?>
                    <br>
                    <?=($slide->size)? 'Размер: '.$slide->size.'' : ''?>
                </p>
                <p><?=$slide->dop?></p>
                <p class="work__rightBigText">Срок создания: <?=$slide->creationperiod?></p>
            </div>
            <div class="slider__itemRightInnerRight">
                <div class="work__bottomRightHeader">Хотите так же?</div>
                <div class="work__bottomRightText">Отправляйте фото и уже через
                    4 часа вы получите возможные варианты будущей работы</div>
                <br>
                <br>
                <br>
                <img src="/resources/img/arr.png" alt="">
                <a href="#photo" data-fancybox="forms" class="button2" style="margin-bottom: 10px;">Загрузить фото</a>
                <a href="<?=Url::to(['textpage/index', 'alias' => Akcia::findOne(1)->alias])?>" class="button1 button1_padding">Сделать расчет</a>
                <br>
                <br>
                <a href="<?=Url::to(['textpage/index', 'alias' => Textpage::findOne(2)->alias])?>" class="work__bottomRightLink">Доставка и оплата</a>
            </div>
        </div>
    </div>
</div>