<?php
use common\models\Akcia;
use yii\helpers\Url;

?>
<div class="slider__item" data-id="<?=$slide->id?>">
    <div class="slider__itemLeft" style="background-image: url(<?=$slide->image?>);"></div>
    <div class="slider__itemRight" style="background-image: url(<?=$slide->background?>);">
        <div class="slider__itemHeader"><?=$slide->header?></div>
        <div class="slider__itemText"><?=$slide->text1?></div>
        <div class="slider__itemFeatures">
            <div class="slider__itemFeaturesItem" style="background-image: url(/resources/img/priceIcon.png);"><?=$slide->price?></div>
            <div class="slider__itemFeaturesItem" style="background-image: url(/resources/img/timeIcon.png);"><?=$slide->time?></div>
        </div>
        <div class="slider__itemBigText"><?=$slide->text2?></div>
        <div class="slider__itemFeatures">
            <a href="#photo" data-fancybox="forms" class="button2">Загрузить фото</a>
            &nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;
            <a href="<?=Url::to(['textpage/index', 'alias' => Akcia::findOne(1)->alias])?>" class="button1 button1_padding">Сделать расчет</a>
        </div>
    </div>
</div>