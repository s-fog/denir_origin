<?php
use common\models\Akcia;
use frontend\models\RaschetForm;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->params['pagetitle'] = $model->pagetitle;
$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
?>

<div class="header"><span><?=$model->pagetitle?></span></div>

<?php
    $products = [];
    $requestForm = new RaschetForm();

    foreach(explode(',', $model->products) as $pr) {
        $products[$pr] = $pr;
    }

    $form = ActiveForm::begin([
    'options' => [
    'class' => 'raschetForm sendForm',
    'id' => 'raschetForm'
    ],
    ]);?>

    <div class="container">
        <div class="raschetForm__steps">
            <div class="raschetForm__step">
                <div class="raschetForm__stepInner">
                    <div class="raschetForm__digit">01</div>
                    <div class="raschetForm__name">Выберите продукт:</div>
                    <div class="raschetForm__radioList">
                        <?=$form->field($requestForm, 'product')
                            ->radioList(
                                $products,
                                [
                                    'item' => function($index, $label, $name, $checked, $value) {
                                        $checked = '';

                                        if ($index == 0) {
                                            $checked = ' checked';
                                        }

                                        $return = '<label class="raschetForm__radio">';
                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '"'.$checked.'>';
                                        $return .= '<span>' . ucwords($label) . '</span>';
                                        $return .= '</label>';
                                        return $return;
                                    }
                                ]
                            )
                            ->label(false);
                        ?>
                    </div>
                </div>
            </div>
            <div class="raschetForm__step">
                <div class="raschetForm__stepInner">
                    <div class="raschetForm__digit">02</div>
                    <div class="raschetForm__name">Укажите размеры и пожелания:</div>
                    <?=$form->field($requestForm, 'wishes')
                        ->textarea([
                            'class' => 'raschetForm__input raschetForm__textarea'
                        ])->label(false)?>
                    <div class="raschetForm__name raschetForm__photoHeader">Прикрепите фото:</div>
                    <?=$form->field($requestForm, 'file[]', [
                        'template' => "<label class='raschetForm__file'><span>Загрузить фотографии (до 30 Мб.)</span>{input}</label>"
                    ])->fileInput([
                        'multiple' => true,
                        'accept' => 'image/*'
                    ])?>
                </div>
            </div>
            <div class="raschetForm__step">
                <div class="raschetForm__stepInner">
                    <div class="raschetForm__digit">03</div>
                    <div class="raschetForm__name">Введите контактные данные:</div>
                    <div class="raschetForm__fieldset">
                        <?=$form->field($requestForm, 'name')
                            ->textInput([
                                'class' => 'raschetForm__input',
                                'placeholder' => 'Имя'
                            ])->label(false)?>
                        <?=$form->field($requestForm, 'phone')
                            ->textInput([
                                'class' => 'raschetForm__input',
                                'placeholder' => 'Телефон'
                            ])->label(false)?>
                        <?=$form->field($requestForm, 'email')
                            ->textInput([
                                'class' => 'raschetForm__input',
                                'placeholder' => 'E-mail'
                            ])->label(false)?>
                    </div>
                    <button type="submit" class="raschetForm__submit">Отправить</button>
                    <div class="loading"></div>
                </div>
            </div>
        </div>
    </div>

        <?=$form->field($requestForm, 'type')
            ->hiddenInput([
                'value' => 'Оформление расчета с сайта denir'
            ])->label(false)?>

        <?=$form->field($requestForm, 'url')
            ->hiddenInput([
                'value' => $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']
            ])->label(false)?>

        <?=$form->field($requestForm, 'BC')
            ->hiddenInput([
                'value' => ''
            ])->label(false)?>

<?php ActiveForm::end();?>

<div class="text1">
    <div class="container">
        <div class="text1__inner"><?=$model->text1?></div>
    </div>
</div>

<div class="akcia">
    <div class="container">
        <div class="akcia__header">Акция</div>
        <div class="akcia__text"><?=$model->text2?></div>
    </div>
</div>

<?php
$lastDayOfThisMonth = strtotime('last day of this month');
$p = strtotime(date('Y:m:d', $lastDayOfThisMonth)." 23:59:59", $lastDayOfThisMonth);
$time = $p - time();
?>

<div class="akciaImage">
    <img src="<?=$model->image?>" alt="" class="akciaImage__image">
    <div class="akciaImage__header">До конца акции осталось: </div>
    <div class="akciaImage__counter" data-time="<?=$time?>"></div>
</div>


<?= $this->render('@frontend/views/blocks/reviews') ?>

<?= $this->render('@frontend/views/blocks/advantages', ['class' => 'advantages_additional advantages_bottom']) ?>

<?= $this->render('@frontend/views/blocks/requestForm') ?>