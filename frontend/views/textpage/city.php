<?php
use yii\helpers\Url;
$this->params['pagetitle'] = $model->name;
$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
?>
<div class="header"><span><?=$model->name?></span></div>
<div class="content">
    <div class="container text-center">
        <p class="shadow" style="font-size: 22px;"><?=$model->header?></p>
    </div>
</div>
<div class="cities content">
    <div class="container">
        <div class="content_small">
            <br>
            <?=$model->html?>
        </div>
    </div>
</div>


<?= $this->render('@frontend/views/blocks/requestForm') ?>

<?= $this->render('@frontend/views/blocks/advantages', ['class' => 'advantages_additional advantages_bottom']) ?>

<?= $this->render('@frontend/views/blocks/reviews', ['class' => 'reviews_beforeFooter']) ?>