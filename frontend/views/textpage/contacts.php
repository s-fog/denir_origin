<?php
use yii\widgets\LinkPager;

$this->params['pagetitle'] = $model->pagetitle;
$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
?>

    <div class="header"><span><?=$model->pagetitle?></span></div>
    <div class="contactPage">
        <div class="container">
            <div class="contactPage__inner">
                <div class="contactPage__left">
                    <div class="content">
                        <?=$model->text1?>
                    </div>
                </div>
                <div class="contactPage__middle">
                    <div class="contactPage__header"><span>Мастерская</span></div>
                    <div class="contactPage__slider owl-carousel">
                        <?php foreach($slider as $slide): ?>
                            <a href="<?=$slide->image?>" class="contactPage__sliderItem" data-fancybox="contactPage">
                                <img src="<?=$slide->image?>" alt="">
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="contactPage__right">
                    <div class="contactPage__header"><span>Художники</span></div>
                    <div class="contactPage__personalSlider owl-carousel">
                        <?php foreach($artists as $artist): ?>
                            <div class="personal__item">
                                <div class="personal__image">
                                    <img src="<?=$artist->image?>" alt="">
                                </div>
                                <div class="personal__name"><?=$artist->name?></div>
                                <div class="personal__function"><?=$artist->function?></div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAT7dgpnaHAF7mY4BMq3kAqBFJQds7K_FQ&callback=initMap"
            async defer></script>
    <div id="map"></div>
<script>
    var map;

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 50.492780, lng: 30.473495},
            zoom: 13
        });

        var beachMarker = new google.maps.Marker({
            position: {lat: 50.492780, lng: 30.473495},
            map: map,
            icon: '/resources/img/baloon.png'
        });
    }
</script>

<?= $this->render('@frontend/views/blocks/advantages', ['class' => 'advantages_additional advantages_bottom']) ?>

<?= $this->render('@frontend/views/blocks/requestForm') ?>

<?= $this->render('@frontend/views/blocks/reviews', ['class' => 'reviews_beforeFooter']) ?>