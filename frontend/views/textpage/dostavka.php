<?php
use yii\helpers\Url;
$this->params['pagetitle'] = $model->pagetitle;
$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
?>

<div class="header"><span><?=$model->pagetitle?></span></div>

<p class="text-center" style="font-size: 16px;">Доставка в города Украины осуществляется в течении суток службой доставок «Нова Пошта»,<br>
    по указанному Вами адресу или на склад.</p>
<p class="text-center" style="font-size: 16px;">Для получения посылки Вам нужно при себе иметь документ удостоверяющий личность<br>
    и номер декларации, который мы Вам заранее сообщим.</p>
<p class="shadow text-center" style="font-size: 22px;text-transform: uppercase;">Чтобы узнать способы оплаты и точное время доставки,<br>
    выберите свой населенный пункт:</p>

    <div class="cities">
        <div class="container">
            <div class="cities__inner">
                <?php
                foreach($cities as $letter => $ccitiess) {
                    echo '<div class="cities__item">';
                    echo '<div class="cities__letter">'.$letter.'</div>';
                    foreach($ccitiess as $city) {
                        echo '<div class="cities__city">
                            <a href="'.Url::to(['city/index', 'alias' => $city->alias]).'" class="cities__name">'.$city->name.'</a>
                            <div class="cities__price">от '.$city->period.' | от '.$city->price.'</div>
                        </div>';
                    }
                    echo '</div>';
                }
                ?>
            </div>
        </div>
    </div>

<?= $this->render('@frontend/views/blocks/requestForm') ?>

<?= $this->render('@frontend/views/blocks/advantages', ['class' => 'advantages_additional advantages_bottom']) ?>

<?= $this->render('@frontend/views/blocks/reviews', ['class' => 'reviews_beforeFooter']) ?>