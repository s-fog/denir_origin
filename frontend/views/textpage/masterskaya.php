<?php
    $this->params['pagetitle'] = $model->pagetitle;
$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
?>
<div class="slider slider_topLine owl-carousel">
    <?php foreach($slider as $slide): ?>
        <div class="slider__item">
            <div class="slider__itemLeft" style="background-image: url(<?=$slide->image?>);"></div>
            <div class="slider__itemRight" style="background-image: url(resources/img/der.png);">
                <div class="slider__header"><?=$slide->header?></div>
                <div class="slider__text"><?=$slide->text?></div>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<?= $this->render('@frontend/views/blocks/personal') ?>

<?= $this->render('@frontend/views/blocks/works') ?>

<?= $this->render('@frontend/views/blocks/advantages', ['class' => '']) ?>

<?= $this->render('@frontend/views/blocks/reviews') ?>

<?= $this->render('@frontend/views/blocks/requestForm') ?>