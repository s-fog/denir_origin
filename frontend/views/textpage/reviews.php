<?php
use yii\widgets\LinkPager;

$this->params['pagetitle'] = $model->pagetitle;
$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
?>

    <div class="reviews">
        <div class="container">
            <div class="reviews__header"><?=$model->pagetitle?></div>
            <div class="reviews__inner">
                <?php foreach ($reviews as $review) {
                    echo $this->render('@frontend/views/review/_item', ['review' => $review]);
                }  ?>
            </div>
            <?php
                echo LinkPager::widget([
                    'pagination' => $pages,
                ]);
            ?>
        </div>
    </div>

<?= $this->render('@frontend/views/blocks/requestForm') ?>

<?= $this->render('@frontend/views/blocks/advantages', ['class' => 'advantages_additional advantages_bottom']) ?>