<?php
    $this->params['pagetitle'] = $model->pagetitle;
$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
?>

    <div class="serts">
        <div class="container">
            <div class="header"><span><?=$model->pagetitle?></span></div>
            <div class="serts__inner">
                <?php foreach($model->getBehavior('galleryBehavior')->getImages() as $image):
                    $imgMedium = str_replace(array(
                        'D:\htdocs\denir-origin/www',
                        '/home/anton438/denir.com.ua/www',
                        '/home/u474194/denir.com.ua/www/www'
                    ), array('',''), $image->getUrl('medium'));
                    $imgOriginal = str_replace(array(
                        'D:\htdocs\denir-origin/www',
                        '/home/anton438/denir.com.ua/www',
                        '/home/u474194/denir.com.ua/www/www'
                    ), array('',''), $image->getUrl('original')); ?>
                    <a href="<?=$imgOriginal?>" data-fancybox="serts" class="serts__item">
                        <img src="<?=$imgMedium?>" alt="">
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="content content_bg">
        <div class="container" style="width: 830px;">
            <div class="content_column2 light" style="font-size: 16px;">
                <?=$model->text1?>
            </div>
            <hr>
            <p class="light text-center" style="font-size: 18px;">При активации сертификата, если стоимость закакза ниже цены указанной<br>
                на сертификате, разница не копменсируется.</p>
        </div>
    </div>


<?= $this->render('@frontend/views/blocks/requestForm') ?>

<?= $this->render('@frontend/views/blocks/advantages', ['class' => 'advantages_additional advantages_bottom']) ?>

<?= $this->render('@frontend/views/blocks/reviews', ['class' => 'reviews_beforeFooter']) ?>