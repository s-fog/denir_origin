<?php
use yii\helpers\Url;

$this->params['pagetitle'] = $model->pagetitle;
$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
?>
    <div class="video">
        <div class="container">
            <div class="header"><span><?=$model->pagetitle?></span></div>
            <div class="video__inner">
                <?php foreach($videos as $video) { ?>
                    <div class="video__item">
                        <div class="video__itemImage">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?=$video->video?>?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <div class="video__itemInfo">
                            <a href="<?=Url::to(['/video/index', 'alias' => $video->alias])?>" class="video__itemName"><?=$video->name?></a>
                            <div class="video__itemDescr"><?=$video->undername?></div>
                            <hr>
                            <div class="video__itemText"><?=$video->introtext?></div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

<?= $this->render('@frontend/views/blocks/requestForm') ?>

<?= $this->render('@frontend/views/blocks/advantages', ['class' => 'advantages_additional advantages_bottom']) ?>

<?= $this->render('@frontend/views/blocks/reviews', ['class' => 'reviews_beforeFooter']) ?>