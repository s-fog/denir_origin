<?php
use yii\helpers\Url;

$this->params['pagetitle'] = $model->name;
$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
?>
    <div class="videoPage">
        <a href="#" class="video__itemImage">
            <iframe width="870" height="400" src="https://www.youtube.com/embed/<?=$model->video?>?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
        </a>
        <div class="videoPage__info content_bg">
            <div class="videoPage__infoInner">
                <div class="videoPage__name"><?=$model->name?></div>
                <div class="videoPage__descr"><?=$model->undername?></div>
                <hr>
                <div class="content content_column2"><?=$model->text?></div>
            </div>
        </div>
    </div>


<?= $this->render('@frontend/views/blocks/requestForm') ?>

<?= $this->render('@frontend/views/blocks/advantages', ['class' => 'advantages_additional advantages_bottom']) ?>