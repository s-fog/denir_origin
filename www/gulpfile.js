var gulp = require('gulp');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var less = require('gulp-less');
var minifyCSS = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var watch = require('gulp-watch');
var browserSync = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
var plumber = require('gulp-plumber');
var autoprefixerOptions = {
    browsers: ['> 1%'],
    cascade: false
};
var cssDest = 'assets/css';

gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: 'www'
        }
    });
});

gulp.task('scripts', function () {
    return gulp.src('resources/js/script.es6')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(plumber())
        .pipe(concat('script.js'))
        .pipe(uglify())
        .pipe(gulp.dest('resources/js'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('scripts.vendor', function () {
    return gulp.src([
        'resources/js/vendor/jquery-ui/jquery-ui.min.js',
        'resources/js/vendor/fancybox/dist/jquery.fancybox.js',
        'resources/js/vendor/owl-carousel/owl-carousel/owl.carousel.js'
        ])
        .pipe(concat('vendor.js'))
        .pipe(plumber())
        .pipe(uglify())
        .pipe(gulp.dest('resources/js'));
});

gulp.task('styles', function () {
    return gulp.src('resources/css/styles.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(gulp.dest('resources/css'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('build', ['scripts', 'scripts.vendor', 'styles', 'images']);

gulp.task('watch', ['browser-sync', 'scripts', 'styles'], function () {
    gulp.watch('resources/js/script.es6', ['scripts']);
    gulp.watch('resources/css/**/*.scss', ['styles']);
    gulp.watch('*.php', browserSync.reload);
    gulp.watch('*.html', browserSync.reload);
});
