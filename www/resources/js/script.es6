class Works {
    constructor(root) {
        this.root = root;

        this.workContainerWidth = 0;
        this.reviewShow = true;

        this.workLeftClass = 'work__left';
        this.workLeftInterierClass = 'work__left_interier';
        this.workLeftMainClass = 'work__left_main';
        this.workLeftReviewClass = 'work__left_review';
        this.buttonClass = 'work__showReview';
        this.toInterierClass = 'work__toInterier';
        this.reviewContainerClass = 'work__bottomLeftInner';

        this._cacheNodes();
        this._bindEvents();
        this._ready();
    }

    _cacheNodes() {
        this.nodes = {
            workContainer: $('#work'),
            fancyboxButtonLeft: $(),
            fancyboxButtonRight: $(),
            workLeft: $('.'+this.workLeftClass+''),
            workLeftInterier: $('.'+this.workLeftInterierClass+''),
            workLeftMain: $('.'+this.workLeftMainClass+''),
            workLeftReview: $('.'+this.workLeftReviewClass+''),
            button: $('#work .'+this.buttonClass+''),
            toInterier: $('.'+this.toInterierClass+''),
            reviewContainer: $('.'+this.reviewContainerClass+'')
        }
    }

    _bindEvents() {
        $(window).resize(() => {
            this.controlButtonsToMiddle()
        });

        this.nodes.workContainer.on('click', '.'+this.toInterierClass+'', () => {
            this.nodes.workLeft.hide();
            this.nodes.workLeftInterier.show();
            this.nodes.button.show();
            this.nodes.button.text('Вернуться к работе');
            this.nodes.button.addClass('open');
        });

        this.nodes.workContainer.on('click', '.work__buttonLeft', () => {
            $('.fancybox-button--left').click();
        });

        this.nodes.workContainer.on('click', '.work__buttonRight', () => {
            $('.fancybox-button--right').click();
        });

        this.nodes.workContainer.on('click', '.'+this.buttonClass+'', (event) => {
            if($(event.currentTarget).hasClass('open')) {
                this.nodes.workLeft.hide();
                this.nodes.workLeftMain.show();
                this.reviewShowHide('hide');
                this.changeButton(false);

                if ($('.work__bottomLeft').html() == '') {
                    this.nodes.button.hide();
                }
            } else {
                this.reviewShowHide('show');
                this.changeButton(true);
            }
        });

        $('.reviews__more').click((event) => {
            let already = $('.reviews__item').length;
            let data = 'already=' + already;

            let pcategoryId;
            pcategoryId = $('.reviews').data('pcategoryid');
            if (!pcategoryId) pcategoryId = 0;

            data += '&pcategory_id=' + pcategoryId;
            $(event.target).addClass('loading');

            $.post('/review/loadmore', data, (response) => {
                let res = $.parseJSON(response);
                $(event.target).removeClass('loading');
                console.log(res);
                console.log(res.availableReviewsCount);
                console.log(res.allReviewsCount);

                if (parseInt(res.availableReviewsCount) >= parseInt(res.allReviewsCount)) {
                    $('.reviews__more').hide();
                }

                $('.reviews__inner').append(res.items);

                for (let i = already; i < $('.reviews__item').length; i++){
                    $('.reviews__itemSlider').eq(i).owlCarousel({
                        items: 1,
                        navigation: true,
                        navigationText: false,
                        pagination: false,
                        responsive: false
                    });
                }

                this.fancyboxWorks();
            });
        });

        $('.works__more').click((event) => {
            let already = $('.works__item:not(.hide)').length;
            let data = 'already=' + already + '&id=' + $(event.currentTarget).parents('.works').data('id');
            $(event.target).addClass('loading');

            $.post('/pcategory/loadmore', data, (response) => {
                let res = $.parseJSON(response);
                //console.log(data);
                //console.log(response);
                $(event.target).removeClass('loading');

                if (parseInt(res.availablePortfoliosCount) >= parseInt(res.allPortfoliosCount)) {
                    $('.works__more').hide();
                }

                $('.works__item.hide').remove();
                var count = (res.items.split('worksOpen').length - 1) + already;
                //console.log('count=' + count );
                //console.log('ost=' + count % 4 );

                $('.works__inner').append(res.items);

                if (count % 4 == 1) {
                    $('.works__inner').append('<div class="works__item hide"></div>' +
                        '<div class="works__item hide"></div>' +
                        '<div class="works__item hide"></div>');
                } else if (count % 4 == 2) {
                    $('.works__inner').append('<div class="works__item hide"></div>' +
                        '<div class="works__item hide"></div>');
                } else if (count % 4 == 3) {
                    $('.works__inner').append('<div class="works__item hide"></div>');
                }

                this.fancyboxWorks();
            });
        });
    }

    _ready() {
        this.workContainerWidth = this.nodes.workContainer.width();

        this.fancyboxWorks();

        $('[data-fancybox="faces"], [data-fancybox="workImage"], [data-fancybox="workSliderItem"]').fancybox({
            touch : false,
            baseTpl	: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
            '<div class="fancybox-bg"></div>' +
            '<div class="fancybox-slider-wrap">' +
            '<div class="fancybox-slider"></div>' +
            '</div>' +
            '<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' +
            '</div>'
        });

        this.initSlider();
    }

    fancyboxWorks() {
        $('.worksOpen').fancybox({
            scrolling: false,
            touch : false,
            baseTpl	: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
            '<div class="fancybox-bg"></div>' +
            '<button data-fancybox-previous class="fancybox-button fancybox-button--left" title="Previous" style="display: none;"></button>' +
            '<button data-fancybox-next class="fancybox-button fancybox-button--right" title="Next" style="display: none;"></button>' +
            '<div class="fancybox-controls">' +
            '<div class="fancybox-buttons">' +
            '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="Close (Esc)"></button>' +
            '</div>' +
            '</div>' +
            '<div class="fancybox-slider-wrap">' +
            '<div class="fancybox-slider"></div>' +
            '</div>' +
            '<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' +
            '</div>',
            onInit: () => {
                this.nodes.fancyboxButtonLeft = $('.fancybox-button--left');
                this.nodes.fancyboxButtonRight = $('.fancybox-button--right');
                this.reviewShow = true;
                $('[name="url"]').each(function(index, element) {
                    element.value = location.href
                });
            },
            beforeLoad: (instance, slide) => {
                let workId = $(slide.opts.$orig).data('id');
                this.loadAjaxContent(workId, $(slide.opts.$orig));
                this.controlButtonsToMiddle();

                let workWidth = 1024;
                let windowWidth = $(window).width();
                let left = parseInt((windowWidth - workWidth) / 2 * 0.76);
                let right = parseInt((windowWidth - workWidth) / 2 * 0.81);
                this.nodes.fancyboxButtonLeft.css('transform', 'translate('+left+'px)');
                this.nodes.fancyboxButtonRight.css('transform', 'translate(-'+right+'px)');
            },
            beforeMove: () => {
                this.loading('show');
            },
            afterMove: () => {
                $('[name="url"]').each(function(index, element) {
                    element.value = location.href
                });
            }
        });
    }

    initSlider() {
        $('.work__reviewSlider').owlCarousel({
            items: 1,
            navigation: true,
            navigationText: false,
            responsive: false
        });

        $('.work__leftInterierSlider').owlCarousel({
            items: 1,
            pagination: false,
            navigation: true,
            navigationText: false,
            responsive: false
        });

        $('[data-fancybox="forms"]').fancybox({
            touch : false,
            beforeLoad: (instance, slide) => {
                let parent;
                let id;
                parent = $(slide.opts.$orig).parents('.slider__item');
                id = parent.data('id');

                if (!id) {
                    parent = $(slide.opts.$orig).parents('.work').find('.work__inner');
                    id = parent.data('id');
                }

                console.log(id);
                if (id) {
                    $('[name="PhotoForm[zakazN]"]').val(id);
                } else {
                    $('[name="PhotoForm[zakazN]"]').val('');
                }
            }
        });
    };

    controlButtonsToMiddle() {
        let height = this.nodes.fancyboxButtonLeft.height();
        let windowHeight = $(window).height();
        let middleOffset = (windowHeight - height) / 2;
        let windowWidth = $(window).width();
        let sideOffset = (windowWidth - this.workContainerWidth) / 4;

        this.nodes.fancyboxButtonLeft.css({"top": middleOffset + "px", "left": sideOffset + "px"});
        this.nodes.fancyboxButtonRight.css({"top": middleOffset + "px", "right": sideOffset + "px"});
    };

    changeButton(back) {
        if(back) {
            this.nodes.button.text('Вернуться к работе');
            this.nodes.button.addClass('open');
            this.nodes.workLeft.hide();
            this.nodes.workLeftReview.show();
        } else {
            this.nodes.button.text('Отзыв клиента');
            this.nodes.button.removeClass('open');
            this.nodes.workLeft.hide();
            this.nodes.workLeftMain.show();
        }
    };

    reviewShowHide(mode) {
        if(mode == 'show') {
            this.root.find('.'+this.reviewContainerClass).show();
        } else {
            this.root.find('.'+this.reviewContainerClass).hide();
        }
    };

    loadAjaxContent(workId, clickedElement) {
        let data = 'id=' + workId;

        $.post('/portfolio/getportfolio', data, (response) => {
            this.nodes.workContainer.html(response);
            this.loading('hide');

            this._cacheNodes();
            this.initSlider();

            if ($(clickedElement).parents('.slider__item').get(0) && this.reviewShow == true) {
                this.reviewShowHide('show');
                this.changeButton(true);
                this.reviewShow = false;
            }
        });
    };

    loading(mode) {
        if (mode == 'show') {
            this.nodes.workContainer.addClass('work_loading');
        } else {
            this.nodes.workContainer.removeClass('work_loading');
        }
    };
}

class Sliders {
    constructor(root) {
        this.root = root;

        this._cacheNodes();
        this._bindEvents();
        this._ready();
    }

    _cacheNodes() {
        this.nodes = {
            handle: $(),
            widthFrom: $('[name="widthFrom"]'),
            widthTo: $('[name="widthTo"]'),
            priceFrom: $('[name="priceFrom"]'),
            priceTo: $('[name="priceTo"]'),
        }
    }

    _bindEvents() {

    }

    _ready() {
        let timeout;

        $('.filterWidth').slider({
            range: true,
            min: this.nodes.widthFrom.data('min'),
            max: this.nodes.widthTo.data('max'),
            values: [ this.nodes.widthFrom.val(), this.nodes.widthTo.val() ],
            create: (event, ui) => {
                this.nodes.handle = $('.filterWidth .ui-slider-handle');
                this.nodes.handle.append('<span class="ui-slider-handle-text">');
                this.nodes.handle.eq(0).find('.ui-slider-handle-text').text(this.nodes.widthFrom.val());
                this.nodes.handle.eq(1).find('.ui-slider-handle-text').text(this.nodes.widthTo.val());
            },
            slide: ( event, ui ) => {
                clearTimeout(timeout);
                this.nodes.handle = $('.filterWidth .ui-slider-handle');
                this.nodes.widthFrom.val(ui.values[0]);
                this.nodes.widthTo.val(ui.values[1]);
                this.nodes.handle.eq(0).find('.ui-slider-handle-text').text(ui.values[0]);
                this.nodes.handle.eq(1).find('.ui-slider-handle-text').text(ui.values[1]);
                timeout = setTimeout(() => {
                    $('.catalog__filter').submit();
                }, 900);
            }
        });

        $('.filterPrice').slider({
            range: true,
            min: this.nodes.priceFrom.data('min'),
            max: this.nodes.priceTo.data('max'),
            values: [ this.nodes.priceFrom.val(), this.nodes.priceTo.val() ],
            create: (event, ui) => {
                this.nodes.handle = $('.filterPrice .ui-slider-handle');
                this.nodes.handle.append('<span class="ui-slider-handle-text">');
                this.nodes.handle.eq(0).find('.ui-slider-handle-text').text(this.nodes.priceFrom.val());
                this.nodes.handle.eq(1).find('.ui-slider-handle-text').text(this.nodes.priceTo.val());
            },
            slide: ( event, ui ) => {
                clearTimeout(timeout);
                this.nodes.handle = $('.filterPrice .ui-slider-handle');
                this.nodes.priceFrom.val(ui.values[0]);
                this.nodes.priceTo.val(ui.values[1]);
                this.nodes.handle.eq(0).find('.ui-slider-handle-text').text(ui.values[0]);
                this.nodes.handle.eq(1).find('.ui-slider-handle-text').text(ui.values[1]);
                timeout = setTimeout(() => {
                    $('.catalog__filter').submit();
                }, 900);
            }
        });
    }
}

class Application {
    constructor() {
        this._mainScripts();
        this._initClasses();
    }

    _mainScripts() {
        /*$('input[name="phone"], input[type="tel"]').inputmask('+7 (999) 999-99-99');*/

        $('.catalog__filter, .search').submit(function() {
            let form = $(this);
            $('.sort__select').prop('disabled', false);


            form.find ('input, textearea, select').each(function(index, element) {
                if (element.value.length == 0) {
                    element.disabled = true;
                }
            });

            return true;
        });

        $('.slider').owlCarousel({
            items: 1,
            navigation: true,
            navigationText: false,
            responsive: false
        });

        $('[data-fancybox*="review"]').fancybox({
            touch : true,
        });

        $('[data-fancybox="forms"]').fancybox({
            touch : false,
            beforeLoad: (instance, slide) => {
                let parent;
                let id;
                parent = $(slide.opts.$orig).parents('.slider__item');
                id = parent.data('id');

                if (!id) {
                    parent = $(slide.opts.$orig).parents('.work').find('.work__inner');
                    id = parent.data('id');
                }

                console.log(id);
                if (id) {
                    $('[name="PhotoForm[zakazN]"]').val(id);
                } else {
                    $('[name="PhotoForm[zakazN]"]').val('');
                }
            }
        });
        
        $('[data-fancybox="catalogItems"]').fancybox({
            touch : false,
            beforeLoad: (instance, slide) => {
                let parent = $(slide.opts.$orig).parents('.catalog__item');
                let img = parent.find('.catalog__itemShow').attr('href');
                let link = parent.find('.catalog__itemGetPrice').attr('href');
                $('.productPopup__image').prop('src', img);
                $('.productPopup__link').attr('href', link);
            }
        });

        $('.present__slider').owlCarousel({
            items: 1,
            navigation: true,
            navigationText: false,
            pagination: true,
            responsive: false
        });

        var slidersOption = {
            items: 1,
            navigation: true,
            navigationText: false,
            pagination: false,
            responsive: false
        };

        $('.contactPage__slider, .contactPage__personalSlider, .reviews__itemSlider').owlCarousel(slidersOption);

        $('.field-photoform-file [type="file"]').change(function(event) {
            var file_name = this.value.replace(/\\/g, '/').replace(/.*\//, '');
            $(event.currentTarget).parent().find('[for="photoform-file"]').text(file_name);
        });

        $('.field-proschetform-file [type="file"]').change(function(event) {
            var file_name = this.value.replace(/\\/g, '/').replace(/.*\//, '');
            $(event.currentTarget).parent().find('[for="proschetform-file"]').text(file_name);
        });

        $('.field-requestform-file [type="file"]').change(function(event) {
            var file_name = this.value.replace(/\\/g, '/').replace(/.*\//, '');
            $(this).parent().find('[for="requestform-file"]').text(file_name);
        });

        $('.field-requestform2-file [type="file"]').change(function(event) {
            var file_name = this.value.replace(/\\/g, '/').replace(/.*\//, '');
            $(this).parent().find('[for="requestform2-file"]').text(file_name);
        });

        $(".sort__select").selectmenu({
            change: function( event, ui ) {
                $('.catalog__filter').submit();
            }
        }).selectmenu( "disable" );

        $('.sort').on('click', '.sort__select + .ui-selectmenu-button', function() {
            let selected = $('.sort__select option:selected');
            let neww = selected.siblings('option');
            selected.prop('selected', false);
            neww.prop('selected', true);
            $('.sort__select').prop('disabled', false);
            $('.catalog__filter').submit();
        });

        $('body').on('beforeSubmit', '.sendForm', (event) => {
            var form = $(event.currentTarget);
            var submitButton = form.find('[type="submit"]');
            console.log(form.attr('class'));
            var formData = new FormData(form.get(0));
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "/mail/index");
            xhr.send(formData);
            form.find('.loading').show();
            submitButton.prop('disabled', true);

            xhr.upload.onprogress = () => {

            };

            xhr.onreadystatechange = () => {
                if (xhr.readyState == 4){
                    if (xhr.status == 200){
                        var response = xhr.responseText;
                        form.find('.loading').hide();

                        console.log(response);

                        if (response == 'success') {
                            $.fancybox.close();
                            submitButton.prop('disabled', false);

                            yaCounter47442727.reachGoal('order');
                            ga('send', 'event', 'ls', 'order');
                            fbq('track', 'Lead');

                            setTimeout(() => {
                                $.fancybox.open('<div class="success__wrapper">'+$('#success').html()+'</div>');
                            }, 200);

                            setTimeout(() => {
                                $.fancybox.close();
                            }, 5000);
                        } else {
                            alert('Ошибка');
                        }
                    } else {
                        console.log('error status');
                    }
                }
            };
            return false;
        });

        $('[name="type"]').change(function() {
            $(this).parents('form').submit();
        });

        $('[data-fancybox="bagetPopup"]').fancybox({
            touch : false,
            beforeLoad: (instance, slide) => {
                let bagetPopup = $('#bagetPopup');
                let parent = $(slide.opts.$orig);

                let img = parent.find('.catalogBagets__itemImage').css('background-image');
                let html = parent.find('.catalogBagets__itemInfoInner').html();

                if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
                    img = img.substring(4, img.length-1);
                } else {
                    img = img.substring(5, img.length-2);
                }

                bagetPopup.find('.bagetPopup__rightInner').html(html);
                bagetPopup.find('.bagetPopup__left img').attr('src', img);
            }
        });

        $('.field-proschetform-file').change(function() {
            let button = $(this).siblings('[for="proschetform-file"]');

            if (this.value != undefined) {
                if (this.value.length > 10) {
                    button.addClass('padding-left');
                } else {
                    button.removeClass('padding-left').text('Загрузить фото');
                }
            } else {
                button.removeClass('padding-left').text('Загрузить фото');
            }
        });

        $('.field-requestform-file').change(function() {
            let button = $(this).siblings('[for="requestform-file"]');

            if (this.value != undefined) {
                if (this.value.length > 10) {
                    button.addClass('padding-left');
                } else {
                    button.removeClass('padding-left').text('Загрузить фото');
                }
            } else {
                button.removeClass('padding-left').text('Загрузить фото');
            }
        });

        $('.catalogItem__infoItem').click(function() {
            let next = $(this).next('.catalogItem__infoContent');
            $('.catalogItem__infoContent').slideUp('fast');

            if (next.css('display') == 'none') {
                next.slideToggle('fast');
            }
            return false;
        });

        $('[href="#photo"], [href="#raschet"], [href="#callback"]').click(function() {
            yaCounter47442727.reachGoal('order_open');
            ga('send', 'event', 'ls', 'order_open');
            fbq('track', 'Lead');
        });

        $('.phone, .phone_contact').click(function() {
            yaCounter47442727.reachGoal('phone_click');
            ga('send', 'event', 'ls', 'phone_click');
            fbq('track', 'Lead');
        });

        var clock = $('.akciaImage__counter').FlipClock(parseInt($('.akciaImage__counter').data('time')),{
            clockFace: 'DailyCounter',
            countdown: true,
            language: 'russian'
        });

        $('.raschetFooter').click(function() {
            if ($('#raschetForm').get(0)) {
                $("html, body").stop().animate({scrollTop:0}, 500, 'swing');

                return false;
            }
        });

        $('.personal__infoTrigger').hover(
            function(event) {
                $(event.currentTarget).next().addClass('active');
            }, function() {
                $('.personal__info').removeClass('active');
            }
        )
    }

    _initClasses() {
        new Works($('#work'));
        new Sliders();
    }
}

(function () {
    new Application();
})();